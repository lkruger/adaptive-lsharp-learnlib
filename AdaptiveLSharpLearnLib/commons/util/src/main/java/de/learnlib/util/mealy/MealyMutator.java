/* Copyright (C) 2013-2021 TU Dortmund
 * This file is part of LearnLib, http://www.learnlib.de/.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.util.mealy;

import net.automatalib.automata.transducers.impl.compact.CompactMealy;
import net.automatalib.util.automata.Automata;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import net.automatalib.words.impl.ListAlphabet;

import java.util.*;

/**
 * Class that allows to mutate a Mealy machine in several ways
 *
 * @param <I> input symbol type
 * @author Loes Kruger
 */
public class MealyMutator<I, O>  {

    Alphabet<I> alphabet;
    List<O> outputs;

    /**
     * Constructor.
     *
     * @param inputAlphabet  the learning alphabet
     */
    public MealyMutator(Alphabet<I> inputAlphabet, List<O> outputAlphabet) {
        this.alphabet= inputAlphabet;
        this.outputs= outputAlphabet;
    }

    // adds a state to a Mealy machine
    public CompactMealy<I,O> addState(CompactMealy<I,O> original, Random seed) {
        CompactMealy<I,O> automaton = new CompactMealy<>(original);
        int q = automaton.addState();

        // choose random state and random input to go to new state
        int p = automaton.getState(seed.nextInt(automaton.getStates().size() - 1));
        I i = this.alphabet.getSymbol(seed.nextInt(this.alphabet.size()));
        O o = original.getOutput(p, i);
        automaton.setTransition(p, i, q, o);

        for(I inp : this.alphabet) {
            // 20% choose a random output, 80% choose an already present output
            int prob = seed.nextInt(100);
            int pn = automaton.getState(seed.nextInt(automaton.getStates().size() - 1));
            O on = this.outputs.get(seed.nextInt(this.outputs.size()));
            if(prob > 20) {
                on = original.getOutput(pn, inp);
            }
            automaton.setTransition(q, inp, pn, on);
        }

        return automaton;
    }

    // removes a (non-initial) state of the Mealy machine
    public CompactMealy<I,O> removeState(CompactMealy<I,O> original, Random seed) {
        // choose random state
        int q = original.getState(seed.nextInt(original.getStates().size()));
        // cannot delete initial state
        while(original.getInitialState() == q) {
            q = original.getState(seed.nextInt(original.getStates().size()));
        }
        CompactMealy<I,O> automaton = new CompactMealy<>(this.alphabet, original.getStates().size() - 1);

        for(int p = 0; p < original.getStates().size(); p++) {
            if(p != q) {
                int pp = automaton.addState();
                for(I i : this.alphabet) {
                    // set newsucc to be same as original
                    int newsucc = original.getSuccessor(p, i);
                    // if in original was removed, use i successor of removed or self loop
                    if(newsucc == q) {
                        newsucc = original.getSuccessor(q, i);
                        if(newsucc == q) {
                            newsucc = pp;
                        } else if(newsucc > q) {
                            newsucc--;
                        }
                    } // do -1 to account for removed state
                    else if(newsucc > q) {
                        newsucc--;
                    }
                    O o = original.getOutput(p, i);
                    automaton.setTransition(pp, i, newsucc, o);
                }
            }
        }
        automaton.setInitialState(0);

        return automaton;
    }

    // changes the destination of a transition
    public CompactMealy<I,O> divertTransition(CompactMealy<I,O> original, Random seed) {
        int q = original.getState(seed.nextInt(original.getStates().size()));
        I i = this.alphabet.getSymbol(seed.nextInt(this.alphabet.size()));
        int p = original.getState(seed.nextInt(original.getStates().size()));
        CompactMealy<I,O> automaton = new CompactMealy<>(original);
        O o = original.getOutput(q, i);
        automaton.setTransition(q,i,p,o);

        // try again if the resulting automata is bisimulation equivalent to original Mealy machine
        Word<I> sep = Automata.findSeparatingWord(original, automaton, this.alphabet);
        while (sep == null) {
            q = original.getState(seed.nextInt(original.getStates().size()));
            i = this.alphabet.getSymbol(seed.nextInt(this.alphabet.size()));
            p = original.getState(seed.nextInt(original.getStates().size()));
            o = original.getOutput(q, i);
            automaton.setTransition(q,i,p,o);
            sep = Automata.findSeparatingWord(original, automaton, this.alphabet);
        }

        return automaton;
    }

    // changes the output of a transition
    public CompactMealy<I,O> changeTransitionOutput(CompactMealy<I,O> original, Random seed) {
        int q = original.getState(seed.nextInt(original.getStates().size()));
        I i = this.alphabet.getSymbol(seed.nextInt(this.alphabet.size()));
        O o = this.outputs.get(seed.nextInt(this.outputs.size()));
        int p = original.getSuccessor(q, i);
        CompactMealy<I,O> automaton = new CompactMealy<>(original);
        automaton.setTransition(q,i,p,o);

        // try again if the resulting automata is bisimulation equivalent to original Mealy machine
        Word<I> sep = Automata.findSeparatingWord(original, automaton, this.alphabet);
        while (sep == null) {
            q = original.getState(seed.nextInt(original.getStates().size()));
            i = this.alphabet.getSymbol(seed.nextInt(this.alphabet.size()));
            o = this.outputs.get(seed.nextInt(this.outputs.size()));
            p = original.getSuccessor(q, i);
            automaton.setTransition(q,i,p,o);
            sep = Automata.findSeparatingWord(original, automaton, this.alphabet);
        }

        return automaton;
    }

    // adds a state to a Mealy machine
    public CompactMealy<String,O> addDummyInitState(CompactMealy<String,O> original, Random seed, String freshSym) {
        List<String> lalp = new ArrayList<>();
        for(String sym : original.getInputAlphabet()) {
            lalp.add(sym);
        }
        lalp.add(freshSym);
        Alphabet<String> myalp = new ListAlphabet<>(lalp);
        CompactMealy<String, O> automaton = new CompactMealy<>(myalp, original.getStates().size());

        // retrieve old transitions
        for(Integer st : original.getStates()) {
            automaton.addState();
        }
        for(Integer st : automaton.getStates()) {
            for(String sym : original.getInputAlphabet()) {
                automaton.setTransition(st, sym, original.getSuccessor(st, sym), original.getOutput(st, sym));
            }
        }
        int dummy = automaton.addState();
        // self loop in dummy state
        for(String sym : myalp) {
            automaton.setTransition(dummy, sym, dummy, original.getOutput(0, myalp.getSymbol(0)));
        }
        // self loop with dummy in other states
        for(Integer st : automaton.getStates()) {
            automaton.setTransition(st, freshSym, st, original.getOutput(0, myalp.getSymbol(0)));
        }
        // transition from dummy to prev init state
        automaton.setTransition(dummy, freshSym, 0, original.getOutput(0, myalp.getSymbol(1)));
        automaton.setInitialState(dummy);

        return automaton;
    }

    public CompactMealy<I,O> changeInitState(CompactMealy<I,O> original, Random seed) {
        int newInit = seed.nextInt(original.getStates().size());
        original.setInitialState(newInit);
        return original;
    }

    public CompactMealy<I,O> removeSymbolFromAlphabet(CompactMealy<I,O> original, Random seed) {
        int torem = seed.nextInt(original.getInputAlphabet().size());
        I remSym = original.getInputAlphabet().getSymbol(torem);

        List<I> lalp = new ArrayList<>();
        for(I sym : original.getInputAlphabet()) {
            if(!sym.equals(remSym)) {
                lalp.add(sym);
            }
        }
        Alphabet<I> myalp = new ListAlphabet<>(lalp);
        CompactMealy<I, O> automaton = new CompactMealy<>(myalp, original.getStates().size());
        automaton.setInitialState(original.getInitialState());

        for(int st : original.getStates()){
            automaton.addState();
            for(I sym : automaton.getInputAlphabet()){
                automaton.setTransition(st, sym, original.getSuccessor(st, sym), original.getOutput(st, sym));
            }
        }

        return automaton;
    }

    public CompactMealy<I,O> generateConcat(CompactMealy<I,O> original, boolean front, int changeState, Random seed) {
        // generate other automaton
        CompactMealy<I, O> mutation = manyMutations(original, seed);
        if(front){
            return concatenateMealies(original, mutation, changeState, seed);
        } else {
            return concatenateMealies(mutation, original, changeState, seed);
        }
    }

    public CompactMealy<I,O> generateConcatGiven(CompactMealy<I,O> original, CompactMealy<I,O> original2, int changeState, Random seed) {
        return concatenateMealies(original, original2, changeState, seed);
    }

    // concatenates two Mealy machines
    public CompactMealy<I,O> concatenateMealies(CompactMealy<I,O> m1, CompactMealy<I,O> m2, int changeState, Random seed) {
        List<I> lalp = new ArrayList<>();
        for(I sym : m1.getInputAlphabet()) {
            lalp.add(sym);
        }
        for(I sym : m2.getInputAlphabet()) {
            if(!lalp.contains(sym)) {
                lalp.add(sym);
            }
        }
        Alphabet<I> myalp = new ListAlphabet<>(lalp);

        CompactMealy<I, O> m12 = new CompactMealy<>(myalp, m1.getStates().size()+m2.getStates().size());


        // retrieve old transitions
        for(Integer st = 0; st < m1.getStates().size() + m2.getStates().size(); st++) {
            m12.addState();
        }
        // last m1 state
        int lastm1 = m1.getStates().size();
        O def_out = m1.getOutput(0, m1.getInputAlphabet().getSymbol(0));
        // add m1 part
        for(Integer st : m1.getStates()) {
            for(I sym : myalp) {
                if(m1.getInputAlphabet().contains(sym)) {
                    m12.setTransition(st, sym, m1.getSuccessor(st, sym), m1.getOutput(st, sym));
                } else {
                    m12.setTransition(st, sym, st, def_out);
                }

            }
        }
        // transition to m2
        m12.setTransition(changeState, m1.getInputAlphabet().getSymbol(0), m1.getStates().size(), m1.getOutput(changeState, m1.getInputAlphabet().getSymbol(0)));
        for(Integer st = 0; st < m2.getStates().size(); st++) {
            for(I sym : myalp) {
                if(m2.getInputAlphabet().contains(sym)) {
                    m12.setTransition(st+lastm1, sym, m2.getSuccessor(st, sym)+lastm1, m2.getOutput(st, sym));
                } else {
                    m12.setTransition(st + lastm1, sym, st + lastm1, def_out);
                }
            }
        }

        m12.setInitialState(0);
        return m12;
    }

    public CompactMealy<I,O> manyMutations(CompactMealy<I,O> original, Random seed) {
        // generate other automaton
        CompactMealy<I, O> mutation = removeState(original, seed);
        mutation = addState(mutation, seed);
        mutation = divertTransition(mutation, seed);
        mutation = changeTransitionOutput(mutation, seed);
        mutation = removeState(mutation, seed);
        mutation = addState(mutation, seed);
        mutation = divertTransition(mutation, seed);
        mutation = changeTransitionOutput(mutation, seed);
        mutation = removeState(mutation, seed);
        mutation = addState(mutation, seed);
        mutation = divertTransition(mutation, seed);
        mutation = changeTransitionOutput(mutation, seed);

        return mutation;
    }

    public CompactMealy<String,O> unionMutationAB(CompactMealy<String,O> original, Random seed) {
        CompactMealy<String,O> originalcopy = new CompactMealy<>(original);
        CompactMealy<String,O> m1 = addDummyInitState(original, seed, "dummy");
        CompactMealy<String,O> originalcopy2 = (CompactMealy<String, O>) manyMutations((CompactMealy<I, O>) originalcopy, seed);
        CompactMealy<String,O> m2 = addDummyInitState(originalcopy2, seed, "dummy2");

        List<String> lalp = new ArrayList<>();
        for(String sym : original.getInputAlphabet()) {
            lalp.add(sym);
        }
        lalp.add("dummy");
        lalp.add("dummy2");
        Alphabet<String> myalp = new ListAlphabet<>(lalp);
        CompactMealy<String, O> m12 = new CompactMealy<>(myalp, 1+original.getStates().size()+original.getStates().size());

        // retrieve old transitions
        for(Integer st = 0; st < m1.getStates().size() + m2.getStates().size() - 1; st++) {
            m12.addState();
        }
        // last m1 state
        int lastm1 = m1.getStates().size();
        O def_out = m1.getOutput(0, m1.getInputAlphabet().getSymbol(0));
        // add m1 part
        for(Integer st : m1.getStates()) {
            for(String sym : myalp) {
                if(m1.getInputAlphabet().contains(sym)) {
                    m12.setTransition(st, sym, m1.getSuccessor(st, sym), m1.getOutput(st, sym));
                } else {
                    m12.setTransition(st, sym, st, def_out);
                }

            }
        }
        for(Integer st = 0; st < m2.getStates().size()-1; st++) {
            for(String sym : myalp) {
                if(m2.getInputAlphabet().contains(sym)) {
                    m12.setTransition(st+lastm1, sym, m2.getSuccessor(st, sym)+lastm1, m2.getOutput(st, sym));
                } else {
                    m12.setTransition(st + lastm1, sym, st + lastm1, def_out);
                }
            }
        }
        m12.setTransition(m1.getInitialState(), myalp.getSymbolIndex("dummy2"), lastm1, def_out);

        m12.setInitialState(m1.getInitialState());
        return m12;
    }

    public CompactMealy<String,O> unionMutationABGiven(CompactMealy<String,O> original, CompactMealy<String,O> original2, Random seed) {
        CompactMealy<String,O> originalcopy = new CompactMealy<>(original);
        CompactMealy<String,O> m1 = addDummyInitState(original, seed, "dummy");
        CompactMealy<String,O> originalcopy2 = new CompactMealy<>(original2);
        CompactMealy<String,O> m2 = addDummyInitState(originalcopy2, seed, "dummy2");

        List<String> lalp = new ArrayList<>();
        for(String sym : original.getInputAlphabet()) {
            lalp.add(sym);
        }
        lalp.add("dummy");
        lalp.add("dummy2");
        Alphabet<String> myalp = new ListAlphabet<>(lalp);
        CompactMealy<String, O> m12 = new CompactMealy<>(myalp, 1+original.getStates().size()+original.getStates().size());

        // retrieve old transitions
        for(Integer st = 0; st < m1.getStates().size() + m2.getStates().size() - 1; st++) {
            m12.addState();
        }
        // last m1 state
        int lastm1 = m1.getStates().size();
        O def_out = m1.getOutput(0, m1.getInputAlphabet().getSymbol(0));
        // add m1 part
        for(Integer st : m1.getStates()) {
            for(String sym : myalp) {
                if(m1.getInputAlphabet().contains(sym)) {
                    m12.setTransition(st, sym, m1.getSuccessor(st, sym), m1.getOutput(st, sym));
                } else {
                    m12.setTransition(st, sym, st, def_out);
                }

            }
        }
        for(Integer st = 0; st < m2.getStates().size()-1; st++) {
            for(String sym : myalp) {
                if(m2.getInputAlphabet().contains(sym)) {
                    m12.setTransition(st+lastm1, sym, m2.getSuccessor(st, sym)+lastm1, m2.getOutput(st, sym));
                } else {
                    m12.setTransition(st + lastm1, sym, st + lastm1, def_out);
                }
            }
        }
        m12.setTransition(m1.getInitialState(), myalp.getSymbolIndex("dummy2"), lastm1, def_out);

        m12.setInitialState(m1.getInitialState());
        return m12;
    }

}
