// This class has been generated by de.learnlib.buildtool.refinement.processor.RefinementProcessor.
// Do not edit this class, changes will be overridden.
package de.learnlib.oracle.property;

import de.learnlib.api.oracle.PropertyOracle;
import java.lang.Boolean;
import java.lang.SafeVarargs;
import java.util.Collection;
import net.automatalib.automata.fsa.DFA;

/**
 * This is an auto-generated refinement. See the {@link PropertyOracleChain original class}.
 */
public class DFAPropertyOracleChain<I, P> extends PropertyOracleChain<I, DFA<?, I>, P, Boolean> implements PropertyOracle.DFAPropertyOracle<I, P> {
  /**
   * This is an auto-generated constructor. See the {@link PropertyOracleChain#PropertyOracleChain(PropertyOracle[]) original constructor}.
   */
  @SafeVarargs
  public DFAPropertyOracleChain(PropertyOracle.DFAPropertyOracle<I, P>... oracles) {
    super(oracles);
  }

  /**
   * This is an auto-generated constructor. See the {@link PropertyOracleChain#PropertyOracleChain(Collection) original constructor}.
   */
  public DFAPropertyOracleChain(
      Collection<? extends PropertyOracle.DFAPropertyOracle<I, P>> oracles) {
    super(oracles);
  }
}
