// This class has been generated by de.learnlib.buildtool.refinement.processor.RefinementProcessor.
// Do not edit this class, changes will be overridden.
package de.learnlib.oracle.equivalence;

import de.learnlib.api.oracle.EquivalenceOracle;
import de.learnlib.api.oracle.MembershipOracle;
import java.util.Random;
import net.automatalib.automata.transducers.MealyMachine;
import net.automatalib.words.Word;

/**
 * This is an auto-generated refinement. See the {@link RandomWMethodEQOracle original class}.
 */
public class MealyRandomWMethodEQOracle<I, O> extends RandomWMethodEQOracle<MealyMachine<?, I, ?, O>, I, Word<O>> implements EquivalenceOracle.MealyEquivalenceOracle<I, O> {
  /**
   * This is an auto-generated constructor. See the {@link RandomWMethodEQOracle#RandomWMethodEQOracle(MembershipOracle, int, int) original constructor}.
   */
  public MealyRandomWMethodEQOracle(MembershipOracle.MealyMembershipOracle<I, O> sulOracle,
      int minimalSize, int rndLength) {
    super(sulOracle, minimalSize, rndLength);
  }

  /**
   * This is an auto-generated constructor. See the {@link RandomWMethodEQOracle#RandomWMethodEQOracle(MembershipOracle, int, int, int) original constructor}.
   */
  public MealyRandomWMethodEQOracle(MembershipOracle.MealyMembershipOracle<I, O> sulOracle,
      int minimalSize, int rndLength, int bound) {
    super(sulOracle, minimalSize, rndLength, bound);
  }

  /**
   * This is an auto-generated constructor. See the {@link RandomWMethodEQOracle#RandomWMethodEQOracle(MembershipOracle, int, int, int, int) original constructor}.
   */
  public MealyRandomWMethodEQOracle(MembershipOracle.MealyMembershipOracle<I, O> sulOracle,
      int minimalSize, int rndLength, int bound, int batchSize) {
    super(sulOracle, minimalSize, rndLength, bound, batchSize);
  }

  /**
   * This is an auto-generated constructor. See the {@link RandomWMethodEQOracle#RandomWMethodEQOracle(MembershipOracle, int, int, int, Random, int) original constructor}.
   */
  public MealyRandomWMethodEQOracle(MembershipOracle.MealyMembershipOracle<I, O> sulOracle,
      int minimalSize, int rndLength, int bound, Random random, int batchSize) {
    super(sulOracle, minimalSize, rndLength, bound, random, batchSize);
  }
}
