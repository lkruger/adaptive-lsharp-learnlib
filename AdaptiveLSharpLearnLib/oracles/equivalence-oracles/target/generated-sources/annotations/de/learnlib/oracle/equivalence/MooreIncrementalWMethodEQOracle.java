// This class has been generated by de.learnlib.buildtool.refinement.processor.RefinementProcessor.
// Do not edit this class, changes will be overridden.
package de.learnlib.oracle.equivalence;

import de.learnlib.api.oracle.EquivalenceOracle;
import de.learnlib.api.oracle.MembershipOracle;
import net.automatalib.automata.transducers.MooreMachine;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;

/**
 * This is an auto-generated refinement. See the {@link IncrementalWMethodEQOracle original class}.
 */
public class MooreIncrementalWMethodEQOracle<I, O> extends IncrementalWMethodEQOracle<MooreMachine<?, I, ?, O>, I, Word<O>> implements EquivalenceOracle.MooreEquivalenceOracle<I, O> {
  /**
   * This is an auto-generated constructor. See the {@link IncrementalWMethodEQOracle#IncrementalWMethodEQOracle(MembershipOracle, Alphabet) original constructor}.
   */
  public MooreIncrementalWMethodEQOracle(MembershipOracle.MooreMembershipOracle<I, O> oracle,
      Alphabet<I> alphabet) {
    super(oracle, alphabet);
  }

  /**
   * This is an auto-generated constructor. See the {@link IncrementalWMethodEQOracle#IncrementalWMethodEQOracle(MembershipOracle, Alphabet, int) original constructor}.
   */
  public MooreIncrementalWMethodEQOracle(MembershipOracle.MooreMembershipOracle<I, O> oracle,
      Alphabet<I> alphabet, int maxDepth) {
    super(oracle, alphabet, maxDepth);
  }

  /**
   * This is an auto-generated constructor. See the {@link IncrementalWMethodEQOracle#IncrementalWMethodEQOracle(MembershipOracle, Alphabet, int, int) original constructor}.
   */
  public MooreIncrementalWMethodEQOracle(MembershipOracle.MooreMembershipOracle<I, O> oracle,
      Alphabet<I> alphabet, int maxDepth, int batchSize) {
    super(oracle, alphabet, maxDepth, batchSize);
  }
}
