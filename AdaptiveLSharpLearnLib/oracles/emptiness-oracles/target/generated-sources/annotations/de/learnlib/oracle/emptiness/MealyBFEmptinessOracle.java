// This class has been generated by de.learnlib.buildtool.refinement.processor.RefinementProcessor.
// Do not edit this class, changes will be overridden.
package de.learnlib.oracle.emptiness;

import de.learnlib.api.oracle.AutomatonOracle;
import de.learnlib.api.oracle.EmptinessOracle;
import de.learnlib.api.oracle.MembershipOracle;
import net.automatalib.automata.transducers.MealyMachine;
import net.automatalib.words.Word;

/**
 * This is an auto-generated refinement. See the {@link AbstractBFEmptinessOracle original class}.
 */
public class MealyBFEmptinessOracle<I, O> extends AbstractBFEmptinessOracle<MealyMachine<?, I, ?, O>, I, Word<O>> implements EmptinessOracle.MealyEmptinessOracle<I, O>, AutomatonOracle.MealyOracle<I, O> {
  /**
   * This is an auto-generated constructor. See the {@link AbstractBFEmptinessOracle#AbstractBFEmptinessOracle(MembershipOracle, double) original constructor}.
   */
  public MealyBFEmptinessOracle(MembershipOracle.MealyMembershipOracle<I, O> membershipOracle,
      double multiplier) {
    super(membershipOracle, multiplier);
  }
}
