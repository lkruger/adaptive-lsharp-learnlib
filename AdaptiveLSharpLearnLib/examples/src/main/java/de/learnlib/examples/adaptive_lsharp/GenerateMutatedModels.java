/* Copyright (C) 2013-2022 TU Dortmund
 * This file is part of LearnLib, http://www.learnlib.de/.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.examples.adaptive_lsharp;

import net.automatalib.automata.transducers.impl.compact.CompactMealy;
import net.automatalib.commons.util.Pair;
import net.automatalib.serialization.InputModelDeserializer;
import net.automatalib.serialization.dot.DOTParsers;
import net.automatalib.serialization.dot.GraphDOT;
import net.automatalib.visualization.VisualizationHelper;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import net.automatalib.words.impl.ListAlphabet;
import org.apache.commons.cli.*;
import org.checkerframework.checker.nullness.qual.Nullable;
import de.learnlib.util.mealy.MealyMutator;

import java.io.*;
import java.util.*;
import java.util.function.Function;

/**
 * Example runner file to make mutated Mealy machines
 *
 * @author Loes Kruger
 */
@SuppressWarnings("PMD.SystemPrintln")
public final class GenerateMutatedModels {

    public static final String SUL = "sul";
    public static final String SUL2 = "sul2";
    public static final String OUT = "out";
    public static final String MUT = "mut";
    public static final String SEED = "seed";
    public static final String STATE = "state";

    private GenerateMutatedModels() {
        // prevent instantiation
    }

    public static final Function<Map<String, String>, Pair<@Nullable String, @Nullable Word<String>>>
            MEALY_EDGE_WORD_STR_PARSER = attr -> {
        final String label = attr.get(VisualizationHelper.EdgeAttrs.LABEL);
        if (label == null) {
            return Pair.of(null, null);
        }

        final String[] tokens = label.split("/");

        if (tokens.length != 2) {
            return Pair.of(null, null);
        }

        Word<String> token2 = Word.epsilon();
        token2 = token2.append(tokens[1]);
        return Pair.of(tokens[0], token2);
    };

    public static void main(String[] args) throws Exception {
        // create the command line parser
        CommandLineParser parser = new BasicParser();
        // create the Options
        Options options = createOptions();

        long tstamp = System.currentTimeMillis();
        // random seed
        Random rnd_seed = new Random(tstamp);

        try {
            // parse the command line arguments
            CommandLine line = parser.parse(options, args);

            if(!line.hasOption(SUL)){
                throw new IllegalArgumentException("must provide a SUL");
            }

            // set SUL path
            File sul = new File(line.getOptionValue(SUL));

            File out_dir = new File("mut-"+line.getOptionValue(SUL));
            if( line.hasOption(OUT)){
                out_dir = new File(line.getOptionValue(OUT));
                out_dir.delete();
                out_dir = new File(line.getOptionValue(OUT));
            }

            if( line.hasOption( SEED ) )  tstamp = Long.valueOf(line.getOptionValue(SEED));
            rnd_seed.setSeed(tstamp);

            int changeState = 0;
            if( line.hasOption( STATE ) )  changeState = Integer.valueOf(line.getOptionValue(STATE));

            InputModelDeserializer<String, CompactMealy<String, Word<String>>> mealy_parser = DOTParsers.mealy(MEALY_EDGE_WORD_STR_PARSER);
            CompactMealy<String, Word<String>> target = mealy_parser.readModel(sul).model;

            CompactMealy<String, Word<String>> target2 = new CompactMealy<>(target);
            if( line.hasOption( SUL2 ) ) {
                File sul2 = new File(line.getOptionValue(SUL2));
                target2 = mealy_parser.readModel(sul2).model;
            }

            Set<Word<String>> outputs = new HashSet<>();
            for(int q : target.getStates()) {
                for(String i : target.getInputAlphabet()) {
                    outputs.add(target.getOutput(q, i));
                }
            }
            MealyMutator<String, Word<String>> mutator = new MealyMutator<>(target.getInputAlphabet(), new ArrayList<>(outputs));

            int mutation = 0;
            if(line.hasOption(MUT)) {
                mutation = Integer.parseInt(line.getOptionValue(MUT).toLowerCase());
            } else {
                throw new Exception("No mutation method selected");
            }

            CompactMealy<String, Word<String>> result = null;

            switch(mutation) {
                case 1:
                    result = mutator.addDummyInitState(target, rnd_seed, "dummy");
                    break;
                case 2:
                    result = mutator.changeInitState(target, rnd_seed);
                    break;
                case 3:
                    result = mutator.addState(target, rnd_seed);
                    break;
                case 4:
                    result = mutator.removeState(target, rnd_seed);
                    break;
                case 5:
                    result = mutator.divertTransition(target, rnd_seed);
                    break;
                case 6:
                    result = mutator.changeTransitionOutput(target, rnd_seed);
                    break;
                case 7:
                    result = mutator.removeSymbolFromAlphabet(target, rnd_seed);
                    break;
                case 8:
                    result = mutator.generateConcat(target, true, changeState, rnd_seed);
                    break;
                case 9:
                    result = mutator.generateConcat(target, false, changeState, rnd_seed);
                    break;
                case 10:
                    result = mutator.addState(target, rnd_seed);
                    result = mutator.removeState(result, rnd_seed);
                    result = mutator.divertTransition(result, rnd_seed);
                    result = mutator.changeTransitionOutput(result, rnd_seed);
                    break;
                case 11:
                    result = mutator.changeInitState(target, rnd_seed);
                    result = mutator.addState(result, rnd_seed);
                    result = mutator.removeState(result, rnd_seed);
                    result = mutator.divertTransition(result, rnd_seed);
                    result = mutator.changeTransitionOutput(result, rnd_seed);
                    break;
                case 12:
                    result = mutator.divertTransition(target, rnd_seed);
                    result = mutator.changeTransitionOutput(result, rnd_seed);
                    result = mutator.divertTransition(result, rnd_seed);
                    result = mutator.changeTransitionOutput(result, rnd_seed);
                    result = mutator.divertTransition(result, rnd_seed);
                    result = mutator.changeTransitionOutput(result, rnd_seed);
                    break;
                case 13:
                    result = mutator.manyMutations(target, rnd_seed);
                    break;
                case 14:
                    result = mutator.unionMutationAB(target, rnd_seed);
                    break;
                case 15:
                    result = mutator.unionMutationABGiven(target, target2, rnd_seed);
                    break;
                case 16:
                    result = mutator.generateConcatGiven(target, target2, changeState, rnd_seed);
                    break;
                case 17:
                    result = mutator.generateConcatGiven(target2, target, changeState, rnd_seed);
                    break;
                default:
                    throw new Exception("Invalid mutation method selected: " + mutation);
            }

            FileWriter mFileWriter = new FileWriter(out_dir, true);

            GraphDOT.write(result, result.getInputAlphabet(), mFileWriter);

            mFileWriter.flush();
            mFileWriter.close();

            FileReader fr = new FileReader(out_dir);
            String s;
            try {
                BufferedReader br = new BufferedReader(fr);
                String totalStr = "";
                while ((s = br.readLine()) != null) {
                    totalStr = totalStr + s + "\n";
                }
                totalStr = totalStr.replaceAll(" / ", "/");

                FileWriter fw = new FileWriter(out_dir);
                fw.write(totalStr);
                fw.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        } catch (ParseException e) {
            throw new RuntimeException(e);
        } catch (IllegalArgumentException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static Options createOptions() {
        // create the Options
        Options options = new Options();
        options.addOption( SUL,  true, "System Under Learning (SUL)" );
        options.addOption( SUL2,  true, "System Under Learning (SUL) 2" );
        options.addOption( OUT,  true, "Set output directory" );
        options.addOption( MUT,  true, "Set mutation" );
        options.addOption( SEED,  true, "Set seed" );
        options.addOption(STATE, true, "Set state for concatenation mutations");
        return options;
    }

}
