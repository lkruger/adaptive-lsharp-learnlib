/* Copyright (C) 2013-2022 TU Dortmund
 * This file is part of LearnLib, http://www.learnlib.de/.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.examples.adaptive_lsharp;

import de.learnlib.algorithms.adaptivelsharp.AdaptiveLSharpMealy;
import de.learnlib.algorithms.adaptivelsharp.LSOracleMatching;
import de.learnlib.algorithms.lsharp.NormalObservationTree;
import de.learnlib.algorithms.lsharp.Rule2;
import de.learnlib.algorithms.lsharp.Rule3;
import de.learnlib.api.SUL;
import de.learnlib.api.oracle.EquivalenceOracle;
import de.learnlib.api.oracle.MembershipOracle;
import de.learnlib.api.query.DefaultQuery;
import de.learnlib.api.statistic.StatisticSUL;
import de.learnlib.driver.util.MealySimulatorSUL;
import de.learnlib.filter.cache.sul.SULCaches;
import de.learnlib.filter.statistic.sul.ResetCounterSUL;
import de.learnlib.filter.statistic.sul.SymbolCounterSUL;
import de.learnlib.oracle.equivalence.RandomWpMethodEQOracle;
import de.learnlib.oracle.membership.SULOracle;
import net.automatalib.automata.transducers.MealyMachine;
import net.automatalib.automata.transducers.impl.compact.CompactMealy;
import net.automatalib.commons.util.Pair;
import net.automatalib.serialization.InputModelDeserializer;
import net.automatalib.serialization.dot.DOTParsers;
import net.automatalib.util.automata.Automata;
import net.automatalib.visualization.VisualizationHelper;
import net.automatalib.words.Word;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;

/**
 * This example shows the usage of a adaptive lsharp
 * It follows the running example (Fig. 1) of the paper
 * "State Matching and Mulitple References in Adaptive Active Automata Learning"
 *
 * @author Loes Kruger
 */
@SuppressWarnings("PMD.SystemPrintln")
public final class AdaptiveLSharpRunningExample {

    private AdaptiveLSharpRunningExample() {
        // prevent instantiation
    }

    public static final Function<Map<String, String>, Pair<@Nullable String, @Nullable Word<String>>>
            MEALY_EDGE_WORD_STR_PARSER = attr -> {
        final String label = attr.get(VisualizationHelper.EdgeAttrs.LABEL);
        if (label == null) {
            return Pair.of(null, null);
        }

        final String[] tokens = label.split("/");

        if (tokens.length != 2) {
            return Pair.of(null, null);
        }

        Word<String> token2 = Word.epsilon();
        token2=token2.append(tokens[1]);
        return Pair.of(tokens[0], token2);
    };

    public static void main(String[] args) throws IOException {

        InputModelDeserializer<String, CompactMealy<String, Word<String>>> mealy_parser = DOTParsers.mealy(MEALY_EDGE_WORD_STR_PARSER);
        CompactMealy<String, Word<String>> target = mealy_parser.readModel(new File("Target.dot")).model;
        CompactMealy<String, Word<String>> ref1 = mealy_parser.readModel(new File("Ref1.dot")).model;
        CompactMealy<String, Word<String>> ref2 = mealy_parser.readModel(new File("Ref2.dot")).model;
        CompactMealy<String, Word<String>> ref3 = mealy_parser.readModel(new File("Ref3.dot")).model;

        // First, we show how the target can be learned using only rebuilding and reference 1
        System.out.println("Learning S with R1 and rebuilding");
        rebuild_one_ref(target, ref1);

        // Second, we show that using rebuilding and reference 2 does not lead to good results
        System.out.println("\n\nLearning S with R2 and rebuilding");
        rebuild_one_ref(target, ref2);

        // Third, we show that using state matching and reference 2 is better
        System.out.println("\n\nLearning S with R2 and state matching");
        statematch_one_ref(target, ref2, false);

        // Fourth, we show that using approximate state matching and reference 3 works well
        System.out.println("\n\nLearning S with R3 and approximate state matching");
        statematch_one_ref(target, ref3, true);

        // Finally, we show that using rebuilding and state matching with both reference 1 and 2 is the best
        System.out.println("\n\nLearning S with R1 & R2 and rebuilding + approximate state matching");
        ArrayList<CompactMealy<String,Word<String>>> refs = new ArrayList<>(Arrays.asList(ref1,ref2));
        multi_ref(target, refs);
    }

    private static void rebuild_one_ref(CompactMealy<String, Word<String>> target, CompactMealy<String, Word<String>> ref){

        SUL<String, Word<String>> sulSim = new MealySimulatorSUL<>(target, null);
        // Counters for MQs
        StatisticSUL<String, Word<String>>  mq_sym = new SymbolCounterSUL<>("MQ", sulSim);
        StatisticSUL<String, Word<String>>  mq_rst = new ResetCounterSUL <>("MQ", mq_sym);
        SUL<String, Word<String>> mq_sul = SULCaches.createDAGCache(target.getInputAlphabet(), mq_rst);
        MembershipOracle<String, Word<Word<String>>> mqOracle = new SULOracle<String, Word<String>>(mq_sul);

        Random rand = new Random(14);

        NormalObservationTree<String, Word<String>> obsTree = new NormalObservationTree<>(target.getInputAlphabet());
        LSOracleMatching<String, Word<String>> lsOracle = new LSOracleMatching<>(mqOracle, obsTree, Rule2.NOTHING, Rule3.SEPSEQ, null, null, rand);
        AdaptiveLSharpMealy<String, Word<String>> learner = new AdaptiveLSharpMealy<>(
                lsOracle,
                target.getInputAlphabet(),
                new ArrayList<>(Arrays.asList(ref)),
                true,
                false,
                false);
        learner.startLearning();

        // Counters for EQs
        StatisticSUL<String, Word<String>>  eq_sym = new SymbolCounterSUL<>("EQ", sulSim);
        StatisticSUL<String, Word<String>>  eq_rst = new ResetCounterSUL <>("EQ", eq_sym);
        SUL<String, Word<String>> eq_sul = SULCaches.createDAGCache(target.getInputAlphabet(), eq_rst);
        MembershipOracle<String, Word<Word<String>>> oracleForEQoracle = new SULOracle<>(eq_sul);
        EquivalenceOracle<MealyMachine<?, String, ?, Word<String>>, String, Word<Word<String>>> eqOracle =
                new RandomWpMethodEQOracle(oracleForEQoracle,
                        3,
                        5,
                        50000,
                        rand,
                        1);

        while (true) {
            MealyMachine<?, String, ?, Word<String>> hyp = learner.getHypothesisModel();

            Word<String> sep = Automata.findSeparatingWord(target, hyp, target.getInputAlphabet());
            if (sep == null) {
                break;
            }

            DefaultQuery<String, Word<Word<String>>> ce = eqOracle.findCounterExample(hyp, target.getInputAlphabet());

            if (ce == null) {
                break;
            }

            learner.refineHypothesis(ce);
        }

        MealyMachine<?, String, ?, Word<String>> result = learner.getHypothesisModel();

        System.out.println("MQ Resets: " + ((ResetCounterSUL)mq_rst).getStatisticalData().getCount());
        System.out.println("MQ Symbols: " + ((SymbolCounterSUL)mq_sym).getStatisticalData().getCount());
        System.out.println("EQ Resets: " + ((ResetCounterSUL)eq_rst).getStatisticalData().getCount());
        System.out.println("EQ Symbols: " + ((SymbolCounterSUL)eq_sym).getStatisticalData().getCount());

        System.out.println("States found with rebuilding " + learner.rebuildStates);
    }

    private static void statematch_one_ref(CompactMealy<String, Word<String>> target, CompactMealy<String, Word<String>> ref, boolean approximate){

        SUL<String, Word<String>> sulSim = new MealySimulatorSUL<>(target, null);
        // Counters for MQs
        StatisticSUL<String, Word<String>>  mq_sym = new SymbolCounterSUL<>("MQ", sulSim);
        StatisticSUL<String, Word<String>>  mq_rst = new ResetCounterSUL <>("MQ", mq_sym);
        SUL<String, Word<String>> mq_sul = SULCaches.createDAGCache(target.getInputAlphabet(), mq_rst);
        MembershipOracle<String, Word<Word<String>>> mqOracle = new SULOracle<String, Word<String>>(mq_sul);

        Random rand = new Random(14);

        NormalObservationTree<String, Word<String>> obsTree = new NormalObservationTree<>(target.getInputAlphabet());
        LSOracleMatching<String, Word<String>> lsOracle = new LSOracleMatching<>(mqOracle, obsTree, Rule2.NOTHING, Rule3.SEPSEQ, null, null, rand);
        AdaptiveLSharpMealy<String, Word<String>> learner = new AdaptiveLSharpMealy<>(
                lsOracle,
                target.getInputAlphabet(),
                new ArrayList<>(Arrays.asList(ref)),
                false,
                true,
                approximate);
        learner.startLearning();

        // Counters for EQs
        StatisticSUL<String, Word<String>>  eq_sym = new SymbolCounterSUL<>("EQ", sulSim);
        StatisticSUL<String, Word<String>>  eq_rst = new ResetCounterSUL <>("EQ", eq_sym);
        SUL<String, Word<String>> eq_sul = SULCaches.createDAGCache(target.getInputAlphabet(), eq_rst);
        MembershipOracle<String, Word<Word<String>>> oracleForEQoracle = new SULOracle<>(eq_sul);
        EquivalenceOracle<MealyMachine<?, String, ?, Word<String>>, String, Word<Word<String>>> eqOracle =
                new RandomWpMethodEQOracle(oracleForEQoracle,
                        3,
                        5,
                        50000,
                        rand,
                        1);

        while (true) {
            MealyMachine<?, String, ?, Word<String>> hyp = learner.getHypothesisModel();

            Word<String> sep = Automata.findSeparatingWord(target, hyp, target.getInputAlphabet());
            if (sep == null) {
                break;
            }

            DefaultQuery<String, Word<Word<String>>> ce = eqOracle.findCounterExample(hyp, target.getInputAlphabet());

            if (ce == null) {
                break;
            }

            learner.refineHypothesis(ce);
        }

        MealyMachine<?, String, ?, Word<String>> result = learner.getHypothesisModel();

        System.out.println("MQ Resets: " + ((ResetCounterSUL)mq_rst).getStatisticalData().getCount());
        System.out.println("MQ Symbols: " + ((SymbolCounterSUL)mq_sym).getStatisticalData().getCount());
        System.out.println("EQ Resets: " + ((ResetCounterSUL)eq_rst).getStatisticalData().getCount());
        System.out.println("EQ Symbols: " + ((SymbolCounterSUL)eq_sym).getStatisticalData().getCount());

        System.out.println("States found with match refinement " + learner.matchRefinementStates);
        System.out.println("States found with match separation " + learner.matchSeparationStates);
    }

    private static void multi_ref(CompactMealy<String, Word<String>> target, ArrayList<CompactMealy<String, Word<String>>> refs){

        SUL<String, Word<String>> sulSim = new MealySimulatorSUL<>(target, null);
        // Counters for MQs
        StatisticSUL<String, Word<String>>  mq_sym = new SymbolCounterSUL<>("MQ", sulSim);
        StatisticSUL<String, Word<String>>  mq_rst = new ResetCounterSUL <>("MQ", mq_sym);
        SUL<String, Word<String>> mq_sul = SULCaches.createDAGCache(target.getInputAlphabet(), mq_rst);
        MembershipOracle<String, Word<Word<String>>> mqOracle = new SULOracle<String, Word<String>>(mq_sul);

        Random rand = new Random(14);

        NormalObservationTree<String, Word<String>> obsTree = new NormalObservationTree<>(target.getInputAlphabet());
        LSOracleMatching<String, Word<String>> lsOracle = new LSOracleMatching<>(mqOracle, obsTree, Rule2.NOTHING, Rule3.SEPSEQ, null, null, rand);
        AdaptiveLSharpMealy<String, Word<String>> learner = new AdaptiveLSharpMealy<>(
                lsOracle,
                target.getInputAlphabet(),
                refs,
                true,
                true,
                true);
        learner.startLearning();

        // Counters for EQs
        StatisticSUL<String, Word<String>>  eq_sym = new SymbolCounterSUL<>("EQ", sulSim);
        StatisticSUL<String, Word<String>>  eq_rst = new ResetCounterSUL <>("EQ", eq_sym);
        SUL<String, Word<String>> eq_sul = SULCaches.createDAGCache(target.getInputAlphabet(), eq_rst);
        MembershipOracle<String, Word<Word<String>>> oracleForEQoracle = new SULOracle<>(eq_sul);
        EquivalenceOracle<MealyMachine<?, String, ?, Word<String>>, String, Word<Word<String>>> eqOracle =
                new RandomWpMethodEQOracle(oracleForEQoracle,
                        3,
                        5,
                        50000,
                        rand,
                        1);

        while (true) {
            MealyMachine<?, String, ?, Word<String>> hyp = learner.getHypothesisModel();

            Word<String> sep = Automata.findSeparatingWord(target, hyp, target.getInputAlphabet());
            if (sep == null) {
                break;
            }

            DefaultQuery<String, Word<Word<String>>> ce = eqOracle.findCounterExample(hyp, target.getInputAlphabet());

            if (ce == null) {
                break;
            }

            learner.refineHypothesis(ce);
        }

        MealyMachine<?, String, ?, Word<String>> result = learner.getHypothesisModel();

        System.out.println("MQ Resets: " + ((ResetCounterSUL)mq_rst).getStatisticalData().getCount());
        System.out.println("MQ Symbols: " + ((SymbolCounterSUL)mq_sym).getStatisticalData().getCount());
        System.out.println("EQ Resets: " + ((ResetCounterSUL)eq_rst).getStatisticalData().getCount());
        System.out.println("EQ Symbols: " + ((SymbolCounterSUL)eq_sym).getStatisticalData().getCount());

        System.out.println("States found with rebuilding " + learner.rebuildStates);
        System.out.println("States found with match refinement " + learner.matchRefinementStates);
        System.out.println("States found with match separation " + learner.matchSeparationStates);
    }

}
