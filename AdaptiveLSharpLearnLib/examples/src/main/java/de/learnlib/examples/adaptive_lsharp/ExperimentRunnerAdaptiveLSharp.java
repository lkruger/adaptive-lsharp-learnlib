/* Copyright (C) 2013-2022 TU Dortmund
 * This file is part of LearnLib, http://www.learnlib.de/.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.examples.adaptive_lsharp;

import de.learnlib.acex.analyzers.AcexAnalyzers;
import de.learnlib.algorithms.adaptivelsharp.*;
import de.learnlib.algorithms.adaptivelsharp.LSOracleMatching;
import de.learnlib.algorithms.lsharp.LSOracle;
import de.learnlib.algorithms.dlstar.mealy.ExtensibleDLStarMealy;
import de.learnlib.algorithms.incremental.dfa.IKearnsVaziraniMealy;
import de.learnlib.algorithms.incremental.dfa.IKearnsVaziraniMealyBuilder;
import de.learnlib.algorithms.kv.mealy.KearnsVaziraniMealy;
import de.learnlib.algorithms.kv.mealy.KearnsVaziraniMealyBuilder;
import de.learnlib.algorithms.kv.mealy.KearnsVaziraniMealyState;
import de.learnlib.algorithms.lsharp.*;
import de.learnlib.algorithms.lstar.ce.ObservationTableCEXHandler;
import de.learnlib.algorithms.lstar.ce.ObservationTableCEXHandlers;
import de.learnlib.algorithms.lstar.closing.ClosingStrategies;
import de.learnlib.algorithms.lstar.closing.ClosingStrategy;
import de.learnlib.algorithms.lstar.mealy.ExtensibleLStarMealy;
import de.learnlib.algorithms.lstar.mealy.ExtensibleLStarMealyBuilder;
import de.learnlib.api.SUL;
import de.learnlib.api.oracle.EquivalenceOracle;
import de.learnlib.api.oracle.MembershipOracle;
import de.learnlib.api.query.DefaultQuery;
import de.learnlib.api.statistic.StatisticSUL;
import de.learnlib.datastructure.observationtable.ObservationTable;
import de.learnlib.driver.util.MealySimulatorSUL;
import de.learnlib.filter.cache.sul.SULCaches;
import de.learnlib.filter.statistic.sul.ResetCounterSUL;
import de.learnlib.filter.statistic.sul.SymbolCounterSUL;
import de.learnlib.oracle.equivalence.RandomWordsEQOracle;
import de.learnlib.oracle.equivalence.WpMethodEQOracle;
import de.learnlib.oracle.equivalence.RandomWpMethodEQOracle;
import de.learnlib.oracle.equivalence.mealy.RandomWalkEQOracle;
import de.learnlib.oracle.membership.SULOracle;
import de.learnlib.util.statistics.SimpleProfiler;
import net.automatalib.automata.transducers.MealyMachine;
import net.automatalib.automata.transducers.impl.compact.CompactMealy;
import net.automatalib.commons.util.Pair;
import net.automatalib.serialization.InputModelDeserializer;
import net.automatalib.serialization.dot.DOTParsers;
import net.automatalib.serialization.dot.GraphDOT;
import net.automatalib.util.automata.Automata;
import net.automatalib.visualization.VisualizationHelper;
import net.automatalib.words.Word;
import org.apache.commons.cli.*;
import org.checkerframework.checker.nullness.qual.Nullable;
import de.learnlib.api.logging.LearnLogger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Function;

/**
 * Example runner for adaptive Lsharp
 *
 * @author Loes Kruger
 */
@SuppressWarnings("PMD.SystemPrintln")
public class ExperimentRunnerAdaptiveLSharp {

    public static final String SUL = "sul";
    public static final String REF = "ref";
    public static final String HELP = "help";
    public static final String EQ = "eq";
    public static final String CACHE = "cache";
    public static final String SEED = "seed";
    public static final String LEARN = "learn";
    public static final String RESFILE = "resfile";
    public static final String[] eqMethodsAvailable = {"rndWalks", "rndWp", "rndWords", "Wp"};
    public static final String[] learningMethodsAvailable = {"lsharp", "adaptivelsharp", "kv", "adaptivekv", "pdlstar"};
    public static final String REBUILDING = "R";
    public static final String STATEMATCHING = "M";
    public static final String TIMEOUT = "timeout";
    public static final String APPROXIMATE = "A";


    public static final Function<Map<String, String>, Pair<@Nullable String, @Nullable Word<String>>>
            MEALY_EDGE_WORD_STR_PARSER = attr -> {
        final String label = attr.get(VisualizationHelper.EdgeAttrs.LABEL);
        if (label == null) {
            return Pair.of(null, null);
        }

        final String[] tokens = label.split("/");

        if (tokens.length != 2) {
            return Pair.of(null, null);
        }

        Word<String> token2 = Word.epsilon();
        token2 = token2.append(tokens[1]);
        return Pair.of(tokens[0], token2);
    };


    public static void main(String[] args) throws Exception {
        // create the command line parser
        CommandLineParser parser = new BasicParser();
        // create the Options
        Options options = createOptions();
        // automatically generate the help statement
        HelpFormatter formatter = new HelpFormatter();

        long tstamp = System.currentTimeMillis();
        Instant start = Instant.now();
        // random seed
        Random rnd_seed = new Random(tstamp);

        // parse the command line arguments
        CommandLine line = parser.parse(options, args);
        // default timeout is 5 minutes
        int timeoutsec = 300;
        if (line.hasOption(TIMEOUT)) {
            timeoutsec = Integer.valueOf(line.getOptionValue(TIMEOUT));
        }

        // ------------------
        try {
            if (line.hasOption(HELP)) {
                formatter.printHelp("Adaptive LSharp Experiments", options);
                System.exit(0);
            }
            if (!line.hasOption(SUL)) {
                throw new IllegalArgumentException("Must provide a SUL");
            }

            // set SUL path
            File sul = new File(line.getOptionValue(SUL));

            // create result file
            String result_filename = null;
            FileWriter mFileWriter = null;
            if (line.hasOption(RESFILE)) {
                result_filename = line.getOptionValue(RESFILE);
                File result_file = new File(result_filename);
                result_file.createNewFile();
                mFileWriter = new FileWriter(result_filename, true);
            }

            // create log
            File out_dir = sul.getParentFile();
            LearnLogger logger = LearnLogger.getLogger(ExperimentRunnerAdaptiveLSharp.class);

            // load mealy machine
            InputModelDeserializer<String, CompactMealy<String, Word<String>>> mealy_parser = DOTParsers.mealy(MEALY_EDGE_WORD_STR_PARSER);

            CompactMealy<String, Word<String>> mealyss = null;

            mealyss = mealy_parser.readModel(sul).model;

            logger.logEvent("SUL name: " + sul.getName());
            logger.logEvent("SUL dir: " + sul.getAbsolutePath());

            // if passed as argument, load references
            ArrayList<CompactMealy<String, Word<String>>> references = new ArrayList<>();
            String[] referenceString = null;
            if (line.hasOption(REF)) {
                referenceString = line.getOptionValues(REF);
                for (String ref : referenceString) {
                    File myref = new File(ref);
                    logger.logEvent("Reference model name: " + myref.getName());
                    references.add(mealy_parser.readModel(myref).model);
                }
            }

            if (line.hasOption(SEED)) tstamp = Long.valueOf(line.getOptionValue(SEED));
            rnd_seed.setSeed(tstamp);
            logger.logEvent("Seed: " + Long.toString(tstamp));

            logger.logEvent("Timeout: " + Integer.valueOf(line.getOptionValue(TIMEOUT)));

            logger.logEvent("Cache: " + (line.hasOption(CACHE) ? "Y" : "N"));

            logger.logConfig("Qsize: " + mealyss.getStates().size());
            logger.logConfig("Isize: " + mealyss.getInputAlphabet().size());

            String learnAlgorithm;
            if (line.hasOption(LEARN)) learnAlgorithm = line.getOptionValue(LEARN).toLowerCase();
            else {
                learnAlgorithm = "lsharp";
            }

            // ------------------

            CompactMealy<String, Word<String>> finalMealyss = mealyss;
            Future<String> future = CompletableFuture.supplyAsync(() -> learn(line, rnd_seed, learnAlgorithm, logger, finalMealyss, references));
            String learningstats = "timeout,0,0,0,0,0,0,0,0,0,0,0";
            try {
                learningstats = future.get(timeoutsec, TimeUnit.SECONDS);
            } catch (TimeoutException e) {
                System.out.println("Timeout has occurred");
                future.cancel(true);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(e);
            }

            if (result_filename != null) {
                String refs = "_";
                if (referenceString != null) {
                    for (String ref : referenceString) {
                        File myref = new File(ref);
                        refs += myref.getName() + "_";
                    }
                }
                String learningalg = learnAlgorithm;
                if (learnAlgorithm.equals("adaptivelsharp")) {
                    if (line.hasOption(REBUILDING)) {
                        learningalg += "_R";
                    }
                    if (line.hasOption(STATEMATCHING)) {
                        if (line.hasOption(APPROXIMATE)) {
                            learningalg += "_AM";
                        } else {
                            learningalg += "_M";
                        }
                    }
                }
                Instant end = Instant.now();
                Duration timeElapsed = Duration.between(start, end);
                String sulstats = sul.getName() + "," + refs + "," + learningalg + "," + mealyss.getStates().size() + "," + mealyss.getInputAlphabet().size() + "," + timeElapsed.toMillis()/1000;
                mFileWriter.write(sulstats + "," + learningstats + "\n");
                mFileWriter.flush();
                mFileWriter.close();
            }

            // --------------

        } catch(Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }
    }

    private static String learn(CommandLine line, Random rnd_seed, String learnAlgorithm, LearnLogger logger, CompactMealy<String, Word<String>> mealyss, ArrayList<CompactMealy<String, Word<String>>> references) {
        int rounds = 1;

        // SUL simulator
        SUL<String, Word<String>> sulSim = new MealySimulatorSUL<>(mealyss, null);

        // Counters for MQs
        StatisticSUL<String, Word<String>> mq_sym = new SymbolCounterSUL<>("MQ", sulSim);
        StatisticSUL<String, Word<String>> mq_rst = new ResetCounterSUL<>("MQ", mq_sym);

        // SUL for counting queries wraps sul
        SUL<String, Word<String>> mq_sul = mq_rst;

        // use caching to avoid duplicate queries
        if (line.hasOption(CACHE)) {
            // SULs for associating the IncrementalMealyBuilder 'mq_cbuilder' to MQs
            mq_sul = SULCaches.createDAGCache(mealyss.getInputAlphabet(), mq_rst);
        }
        MembershipOracle<String, Word<Word<String>>> mqOracle = new SULOracle<>(mq_sul);

        // Counters for EQs
        StatisticSUL<String, Word<String>> eq_sym = new SymbolCounterSUL<>("EQ", sulSim);
        StatisticSUL<String, Word<String>> eq_rst = new ResetCounterSUL<>("EQ", eq_sym);

        // SUL for counting queries wraps sul
        SUL<String, Word<String>> eq_sul = eq_rst;

        // use caching to avoid duplicate queries
        if (line.hasOption(CACHE)) {
            // SULs for associating the IncrementalMealyBuilder 'cbuilder' to EQs
            eq_sul = SULCaches.createDAGCache(mealyss.getInputAlphabet(), eq_rst);
        }

        EquivalenceOracle<MealyMachine<?, String, ?, Word<String>>, String, Word<Word<String>>> eqOracle = null;
        eqOracle = buildEqOracle(rnd_seed, line, logger, mealyss, eq_sul);

        ExtensibleLStarMealy<String, Word<String>> LearnerLStar = null;
        ExtensibleDLStarMealy<String, Word<String>> LearnerPDLStar = null;
        IKearnsVaziraniMealy<String, Word<String>> LearnerAKV = null;
        KearnsVaziraniMealy<String, Word<String>> LearnerKV = null;
        LSharpMealy<String, Word<String>> LearnerLSharp = null;
        AdaptiveLSharpMealy<String, Word<String>> LearnerALSharp = null;
        NormalObservationTree<String, Word<String>> obsTree;
        LSOracle<String, Word<String>> lsOracle;
        LSOracleMatching<String, Word<String>> lsOracleMatching;
        switch (learnAlgorithm) {
            case "lstar":
                logger.logConfig("Method: L*");
                List<Word<String>> initPrefixes = new ArrayList<>();
                initPrefixes.add(Word.epsilon());
                List<Word<String>> initSuffixes = new ArrayList<>();
                Word<String> word = Word.epsilon();
                for (String symbol : mealyss.getInputAlphabet()) {
                    initSuffixes.add(word.append(symbol));
                }

                ObservationTableCEXHandler<Object, Object> handlerstar = ObservationTableCEXHandlers.RIVEST_SCHAPIRE;
                ClosingStrategy<Object, Object> strategystar = ClosingStrategies.CLOSE_FIRST;
                ExtensibleLStarMealyBuilder<String, Word<String>> lsbuilder = new ExtensibleLStarMealyBuilder<>();
                lsbuilder.setAlphabet(mealyss.getInputAlphabet());
                lsbuilder.setOracle(mqOracle);
                lsbuilder.setInitialPrefixes(initPrefixes);
                lsbuilder.setInitialSuffixes(initSuffixes);
                lsbuilder.setCexHandler(handlerstar);
                lsbuilder.setClosingStrategy(strategystar);

                LearnerLStar = lsbuilder.create();
                LearnerLStar.startLearning();
                break;
            case "pdlstar":
                logger.logConfig("Method: partial dynamic L*");
                if (references.size() != 1) {
                    throw new IllegalArgumentException("Partial Dynamic L* only works with one reference model");
                }

                ObservationTable<String, Word<Word<String>>> prevTable = learnPrevTable(references.get(0));

                List<Word<String>> initPrefixes0 = new ArrayList<>(prevTable.getShortPrefixes());
                List<Word<String>> initSuffixes0 = new ArrayList<>(prevTable.getSuffixes());

                ObservationTableCEXHandler<Object, Object> handler = ObservationTableCEXHandlers.RIVEST_SCHAPIRE;
                ClosingStrategy<Object, Object> strategy = ClosingStrategies.CLOSE_FIRST;
                LearnerPDLStar = new ExtensibleDLStarMealy<String, Word<String>>(
                        mealyss.getInputAlphabet(),
                        mqOracle,
                        initPrefixes0,
                        initSuffixes0,
                        handler,
                        strategy);

                LearnerPDLStar.startLearning();
                break;
            case "adaptivelsharp":
                logger.logConfig("Method: Adaptive L#");
                boolean rebuild = line.hasOption(REBUILDING);
                boolean statematching = line.hasOption(STATEMATCHING);
                boolean approximate = line.hasOption(APPROXIMATE);

                if (rebuild) {
                    logger.logConfig("Rebuilding [Enabled]");
                } else {
                    logger.logConfig("Rebuilding [Disabled]");
                }
                if (statematching) {
                    if (approximate) {
                        logger.logConfig("Approximate State Matching [Enabled]");
                    } else {
                        logger.logConfig("State Matching [Enabled]");
                    }
                } else {
                    logger.logConfig("State Matching [Disabled]");
                }
                obsTree = new NormalObservationTree<>(mealyss.getInputAlphabet());
                lsOracleMatching = new LSOracleMatching<>(mqOracle, obsTree, Rule2.NOTHING, Rule3.SEPSEQ, null, null, rnd_seed);
                LearnerALSharp = new AdaptiveLSharpMealy<>(lsOracleMatching, mealyss.getInputAlphabet(), references, rebuild, statematching, approximate);
                LearnerALSharp.startLearning();
                break;
            case "kv":
                logger.logConfig("Method: KV");
                KearnsVaziraniMealyBuilder<String, Word<String>> builder = new KearnsVaziraniMealyBuilder<>();
                builder.setAlphabet(mealyss.getInputAlphabet());
                builder.setOracle(mqOracle);
                LearnerKV = builder.create();
                LearnerKV.startLearning();
                // learning statistics
                break;
            case "adaptivekv":
                logger.logConfig("Method: Adaptive KV");
                // first build previous model!
                if (references.size() != 1) {
                    throw new IllegalArgumentException("Adaptive KV only works with one reference model");
                }
                KearnsVaziraniMealyState<String, Word<String>> prevModel = learnPrevModel(references.get(0));
                IKearnsVaziraniMealyBuilder<String, Word<String>> builder1 = new IKearnsVaziraniMealyBuilder<>();
                builder1.setAlphabet(mealyss.getInputAlphabet());
                builder1.setOracle(mqOracle);
                builder1.setStartingState(prevModel);
                builder1.setCounterexampleAnalyzer(AcexAnalyzers.BINARY_SEARCH_FWD);
                LearnerAKV = builder1.create();
                LearnerAKV.startLearning();
                break;
            default: // lsharp is default
                logger.logConfig("Method: L#");
                obsTree = new NormalObservationTree<>(mealyss.getInputAlphabet());
                lsOracle = new LSOracle<>(mqOracle, obsTree, Rule2.NOTHING, Rule3.SEPSEQ, null, null, rnd_seed);
                LearnerLSharp = new LSharpMealy<>(lsOracle, mealyss.getInputAlphabet());
                LearnerLSharp.startLearning();
                break;
        }

        MealyMachine<?, String, ?, Word<String>> hyp = null;
        // learning rounds
        while (true) {
            switch (learnAlgorithm) {
                case "lstar":
                    hyp = LearnerLStar.getHypothesisModel();
                    break;
                case "pdlstar":
                    hyp = LearnerPDLStar.getHypothesisModel();
                    break;
                case "lsharp":
                    hyp = LearnerLSharp.getHypothesisModel();
                    break;
                case "adaptivelsharp":
                    hyp = LearnerALSharp.getHypothesisModel();
                    break;
                case "kv":
                    hyp = LearnerKV.getHypothesisModel();
                    break;
                case "adaptivekv":
                    hyp = LearnerAKV.getHypothesisModel();
                    break;
            }

            logger.logEvent("Hypothesis " + rounds + " has size " + hyp.size());
            try {
                GraphDOT.write(hyp, mealyss.getInputAlphabet(), System.out);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            // early stopping if target and hyp are bisimulation equiv
            Word<String> sep = Automata.findSeparatingWord(mealyss, hyp, mealyss.getInputAlphabet());
            if (sep == null) {
                break;
            }

            DefaultQuery<String, Word<Word<String>>> ce = eqOracle.findCounterExample(hyp, mealyss.getInputAlphabet());
            if (ce == null) {
                break;
            }
            logger.logCounterexample(ce.toString());
            System.out.println("Counterexample " + ce);

            rounds += 1;

            switch (learnAlgorithm) {
                case "lstar":
                    LearnerLStar.refineHypothesis(ce);
                    break;
                case "pdlstar":
                    LearnerPDLStar.refineHypothesis(ce);
                    break;
                case "lsharp":
                    LearnerLSharp.refineHypothesis(ce);
                    break;
                case "adaptivelsharp":
                    LearnerALSharp.refineHypothesis(ce);
                    break;
                case "kv":
                    LearnerKV.refineHypothesis(ce);
                    break;
                case "adaptivekv":
                    LearnerAKV.refineHypothesis(ce);
                    break;
            }
        }

        // learning statistics
        logger.logConfig("Rounds: " + rounds);
        logger.logStatistic(mq_rst.getStatisticalData());
        logger.logStatistic(mq_sym.getStatisticalData());
        logger.logStatistic(eq_rst.getStatisticalData());
        logger.logStatistic(eq_sym.getStatisticalData());

        // OQs for rebuilding/matchRefinement/matchSeparation
        String adaptivestats = "0,0,0,0,0,0";
        if (learnAlgorithm.equals("adaptivelsharp")) {
            logger.logEvent("Rebuilding [Symbols]: " + LearnerALSharp.rebuildOQs);
            logger.logEvent("Rebuilding [States]: " + LearnerALSharp.rebuildStates);
            logger.logEvent("Match Refinement [Symbols]: " + LearnerALSharp.matchRefinementOQs);
            logger.logEvent("Match Refinement [States]: " + LearnerALSharp.matchRefinementStates);
            logger.logEvent("Match Separation [Symbols]: " + LearnerALSharp.matchSeparationOQs);
            logger.logEvent("Match Separation [States]: " + LearnerALSharp.matchSeparationStates);
            adaptivestats = LearnerALSharp.rebuildOQs + "," + LearnerALSharp.rebuildStates + "," + LearnerALSharp.matchRefinementOQs + "," + LearnerALSharp.matchRefinementStates + "," + LearnerALSharp.matchSeparationOQs + "," + LearnerALSharp.matchSeparationStates;
            logger.logEvent("Matching Table: ");
            LearnerALSharp.printMatchTable();
        }
        if (learnAlgorithm.equals("adaptivekv")) {
            logger.logEvent("Rebuilding [States]: " + LearnerAKV.rebuildingStates);
            adaptivestats = "0," + LearnerAKV.rebuildingStates + ",0,0,0,0";
        }
        if (learnAlgorithm.equals("pdlstar")) {
            logger.logEvent("Rebuilding [Prefixes]: " + LearnerPDLStar.getReusedPref());
            logger.logEvent("Rebuilding [Suffixes]: " + LearnerPDLStar.getReusedSuff());
            adaptivestats = "0," + LearnerPDLStar.getReusedPref() + ",0,0,0,0";
        }

        // profiling
        SimpleProfiler.logResults();

        MealyMachine finalHyp = (MealyMachine) hyp;

        boolean isEquiv = Automata.findSeparatingWord(mealyss, finalHyp, mealyss.getInputAlphabet()) == null;
        if (isEquiv) {
            logger.logConfig("Equivalent: OK");
        } else {
            logger.logConfig("Equivalent: NOK");
        }

        String learningstats = isEquiv + "," + rounds + "," + mq_sym.getStatisticalData().toString().substring(14) + "," + mq_rst.getStatisticalData().toString().substring(13) + "," + eq_sym.getStatisticalData().toString().substring(14) + "," + eq_rst.getStatisticalData().toString().substring(13) + "," + adaptivestats;
        return learningstats;
    }

    private static  ObservationTable<String, Word<Word<String>>> learnPrevTable(CompactMealy<String, Word<String>> ref) {
        SUL<String,Word<String>> sulSimL = new MealySimulatorSUL<>(ref, null);
        StatisticSUL<String, Word<String>>  mq_symL = new SymbolCounterSUL<>("MQ", sulSimL);
        StatisticSUL<String, Word<String>>  mq_rstL = new ResetCounterSUL <>("MQ", mq_symL);

        SUL<String, Word<String>> mq_sulL = SULCaches.createDAGCache(ref.getInputAlphabet(), mq_rstL);
        MembershipOracle<String, Word<Word<String>>> mqOracleL = new SULOracle<>(mq_sulL);

        StatisticSUL<String, Word<String>>  eq_symL = new SymbolCounterSUL<>("EQ", sulSimL);
        StatisticSUL<String, Word<String>>  eq_rstL = new ResetCounterSUL <>("EQ", eq_symL);

        // SUL for counting queries wraps sul
        SUL<String, Word<String>> eq_sulL = SULCaches.createDAGCache(ref.getInputAlphabet(), eq_rstL);
        MembershipOracle<String,Word<Word<String>>> oracleForEQoracleL = new SULOracle<>(eq_sulL);
        EquivalenceOracle<MealyMachine<?, String, ?, Word<String>>, String, Word<Word<String>>> eqOracle = new WpMethodEQOracle<>(oracleForEQoracleL, 3);

        List<Word<String>> initPrefixes = new ArrayList<>();
        initPrefixes.add(Word.epsilon());
        List<Word<String>> initSuffixes = new ArrayList<>();
        Word<String> word = Word.epsilon();
        for (String symbol : ref.getInputAlphabet()) {
            initSuffixes.add(word.append(symbol));
        }

        ObservationTableCEXHandler<Object, Object> handlerstar 	= ObservationTableCEXHandlers.RIVEST_SCHAPIRE;
        ClosingStrategy<Object, Object> strategystar 			= ClosingStrategies.CLOSE_FIRST;
        ExtensibleLStarMealyBuilder<String, Word<String>> lsbuilder = new ExtensibleLStarMealyBuilder<>();
        lsbuilder.setAlphabet(ref.getInputAlphabet());
        lsbuilder.setOracle(mqOracleL);
        lsbuilder.setInitialPrefixes(initPrefixes );
        lsbuilder.setInitialSuffixes(initSuffixes);
        lsbuilder.setCexHandler(handlerstar);
        lsbuilder.setClosingStrategy(strategystar);

        ExtensibleLStarMealy<String, Word<String>> lslearner = lsbuilder.create();

        lslearner.startLearning();

        while (true) {
            MealyMachine<?, String, ?, Word<String>> hyp = lslearner.getHypothesisModel();

            DefaultQuery<String, Word<Word<String>>> ce1 = eqOracle.findCounterExample(hyp, ref.getInputAlphabet());
            if (ce1 == null) {
                break;
            }
            lslearner.refineHypothesis(ce1);
        }

        return lslearner.getObservationTable();
    }

    private static KearnsVaziraniMealyState<String, Word<String>> learnPrevModel(CompactMealy<String, Word<String>> ref) {
        SUL<String,Word<String>> sulSimKV = new MealySimulatorSUL<>(ref, null);
        StatisticSUL<String, Word<String>>  mq_symKV = new SymbolCounterSUL<>("MQ", sulSimKV);
        StatisticSUL<String, Word<String>>  mq_rstKV = new ResetCounterSUL <>("MQ", mq_symKV);

        SUL<String, Word<String>> mq_sulKV = SULCaches.createDAGCache(ref.getInputAlphabet(), mq_rstKV);
        MembershipOracle<String, Word<Word<String>>> mqOracleKV = new SULOracle<>(mq_sulKV);

        StatisticSUL<String, Word<String>>  eq_symKV = new SymbolCounterSUL<>("EQ", sulSimKV);
        StatisticSUL<String, Word<String>>  eq_rstKV = new ResetCounterSUL <>("EQ", eq_symKV);

        // SUL for counting queries wraps sul
        SUL<String, Word<String>> eq_sulKV = SULCaches.createDAGCache(ref.getInputAlphabet(), eq_rstKV);
        MembershipOracle<String,Word<Word<String>>> oracleForEQoracleKV = new SULOracle<>(eq_sulKV);
        EquivalenceOracle<MealyMachine<?, String, ?, Word<String>>, String, Word<Word<String>>> eqOracle = new WpMethodEQOracle<>(oracleForEQoracleKV, 3);

        KearnsVaziraniMealyBuilder<String, Word<String>> builder = new KearnsVaziraniMealyBuilder<>();
        builder.setAlphabet(ref.getInputAlphabet());
        builder.setOracle(mqOracleKV);
        KearnsVaziraniMealy<String, Word<String>> RefLearnerKV = builder.create();

        RefLearnerKV.startLearning();

        while (true) {
            MealyMachine<?, String, ?, Word<String>> hyp = RefLearnerKV.getHypothesisModel();

            DefaultQuery<String, Word<Word<String>>> ce1 = eqOracle.findCounterExample(hyp, ref.getInputAlphabet());
            if (ce1 == null) {
                break;
            }
            RefLearnerKV.refineHypothesis(ce1);
        }

        return RefLearnerKV.suspend();
    }


    private static EquivalenceOracle<MealyMachine<?, String, ?, Word<String>>, String, Word<Word<String>>> buildEqOracle(
            Random rnd_seed, CommandLine line, LearnLogger logger, CompactMealy<String, Word<String>> mealyss,
            SUL<String, Word<String>> eq_sul) {
        MembershipOracle<String,Word<Word<String>>> oracleForEQoracle = new SULOracle<>(eq_sul);

        EquivalenceOracle<MealyMachine<?, String, ?, Word<String>>, String, Word<Word<String>>> eqOracle;
        if(!line.hasOption(EQ)){
            logger.logEvent("EquivalenceOracle: WpMethodEQOracle("+3+")");
            return new WpMethodEQOracle<>(oracleForEQoracle, 3);
        }

        int minimalSize = 3;
        int rndLength = 3;
        int batchSize = 1;
        double restartProbability = 0.05;
        int maxSteps = 100;
        int maxTests = 100000000;
        int minLength = 30;
        int maxLength = 100;
        boolean resetStepCount = true;

        switch (line.getOptionValue(EQ)) {
            case "rndWalk":
                // create RandomWalkEQOracle
                eqOracle = new RandomWalkEQOracle<String, Word<String>>(
                        eq_sul, // sul
                        restartProbability,// reset SUL w/ this probability before a step
                        maxSteps, // max steps (overall)
                        resetStepCount, // reset step count after counterexample
                        rnd_seed // make results reproducible
                );
                logger.logEvent("EquivalenceOracle: RandomWalkEQOracle("+restartProbability+","+maxSteps+","+resetStepCount+")");
                break;
            case "rndWords":
                // create RandomWordsEQOracle
                eqOracle = new RandomWordsEQOracle<>(oracleForEQoracle, minLength, maxLength, maxTests, rnd_seed);
                logger.logEvent("EquivalenceOracle: RandomWordsEQOracle("+minLength+", "+maxLength+", "+maxTests+")");
                break;
            case "rndWp":
                eqOracle = new RandomWpMethodEQOracle<>(oracleForEQoracle, minimalSize, rndLength, maxTests, rnd_seed, batchSize);
                logger.logEvent("EquivalenceOracle: RndWpMethodEQOracle("+minimalSize+", "+rndLength+", "+maxTests+", "+batchSize+")");
                break;
            default:
                eqOracle = new WpMethodEQOracle<>(oracleForEQoracle,minimalSize);
                logger.logEvent("EquivalenceOracle: WpMethodEQOracle("+minimalSize+")");
                break;
        }
        return eqOracle;
    }

    private static Options createOptions() {
        // create the Options
        Options options = new Options();
        Option refOption = new Option(REF, true, "Reference Mealy Machine(s) (REF)");
        refOption.setArgs(Option.UNLIMITED_VALUES);
        options.addOption(refOption);
        options.addOption( HELP, false, "Shows help" );
        options.addOption( SUL,  true, "System Under Learning (SUL)" );
        options.addOption( EQ, 	 true, "Set equivalence query generator."
                + "\nOptions: {"+String.join(", ", eqMethodsAvailable)+"}");
        options.addOption( RESFILE,  true, "Set result file path" );
        options.addOption( CACHE,false,"Use caching.");
        options.addOption( LEARN,true, "Model learning algorithm."
                +"\nOptions: {"+String.join(", ", learningMethodsAvailable)+"}");
        options.addOption( SEED, true, "Seed used by the random generator");
        options.addOption( TIMEOUT, true, "Set timeout in seconds");
        options.addOption( REBUILDING, false, "Use rebuilding with adaptive L#");
        options.addOption( STATEMATCHING, false, "Use state matching with adaptive L#");
        options.addOption( APPROXIMATE, false, "Use approximate state matching when state matching is enabled");
        return options;
    }
}