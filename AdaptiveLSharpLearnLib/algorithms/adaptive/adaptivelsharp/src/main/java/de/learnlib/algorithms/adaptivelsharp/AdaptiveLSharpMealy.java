package de.learnlib.algorithms.adaptivelsharp;

import com.google.common.collect.HashBiMap;
import de.learnlib.algorithms.lsharp.*;
import de.learnlib.api.algorithm.LearningAlgorithm.MealyLearner;
import de.learnlib.api.query.DefaultQuery;
import de.learnlib.util.mealy.MealyUtil;
import net.automatalib.automata.transducers.MealyMachine;
import net.automatalib.automata.transducers.impl.compact.CompactMealy;
import net.automatalib.commons.util.Pair;
import net.automatalib.util.automata.equivalence.DeterministicEquivalenceTest;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import static de.learnlib.algorithms.lsharp.Apartness.treeAndHypComputeWitness;
import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * Adaptive version of the LSharp algorithm
 *
 * @param <I>
 *         input symbol type
 *
 * @author Loes Kruger
 */
public class AdaptiveLSharpMealy<I, O> implements MealyLearner<I, O> {
    protected LSOracleMatching<I, O> oqOracle;
    protected Alphabet<I> inputAlphabet;
    protected List<Word<I>> basis;
    protected HashMap<Word<I>, List<Word<I>>> frontierToBasisMap;
    private Integer consistentCECount;
    private HashSet<Word<I>> frontierTransitionsSet;
    private HashBiMap<Word<I>, LSState> basisMap;
    private Integer round;
    private List<CompactMealy<I,O>> references;
    public CompactMealy<I,O> combinedModel;
    public int rebuildOQs;
    public int matchRefinementOQs;
    public int matchSeparationOQs;

    public int rebuildStates;
    public int matchRefinementStates;
    public int matchSeparationStates;
    public RebuildObsTree<I,O> rebuilder;
    public StateMatching<I,O> stateMatcher;
    private boolean stateMatchingEnabled;
    private boolean approximate;


    public AdaptiveLSharpMealy(LSOracleMatching<I, O> oqOracle, Alphabet<I> inputAlphabet, List<CompactMealy<I,O>> references, boolean rebuild, boolean statematching, boolean approximate) {
        this.oqOracle = oqOracle;
        this.inputAlphabet = inputAlphabet;
        this.consistentCECount = 0;
        this.basisMap = HashBiMap.create();
        this.frontierTransitionsSet = new HashSet<>();
        this.round = 1;
        this.references = references;
        this.matchRefinementOQs = 0;
        this.matchSeparationOQs = 0;
        this.matchRefinementStates = 0;
        this.matchSeparationStates = 0;
        this.stateMatchingEnabled = statematching;
        this.approximate = approximate;


        this.rebuilder = new RebuildObsTree<>(oqOracle, inputAlphabet, references, rebuild);
        this.combinedModel = rebuilder.getCombinedModel();
        this.basis = rebuilder.getBasis();
        this.frontierToBasisMap = rebuilder.getFTBM();
        this.rebuildOQs = rebuilder.getRebuildOQs();
        this.rebuildStates = rebuilder.getRebuildStates();

        if(approximate) {
            this.stateMatcher = new StateMatchingApproximate<>(inputAlphabet, this.combinedModel);
        } else {
            this.stateMatcher = new StateMatchingQualitative<>(inputAlphabet, this.combinedModel);
        }
        this.stateMatcher.initializeStateMatching(this.basis);

    }

    public void printMatchTable() {
        this.stateMatcher.printMatchTable(this.basis);
    }

    public void processCex(DefaultQuery<I, Word<O>> cex, LSMealyMachine<I, O> mealy) {
        this.round += 1;
        Objects.requireNonNull(cex);
        Word<I> ceInput = cex.getInput();
        Word<O> ceOutput = cex.getOutput();
        oqOracle.addObservation(ceInput, ceOutput);
        Integer prefixIndex = MealyUtil.findMismatch(mealy, ceInput, ceOutput);
        this.processBinarySearch(ceInput.prefix(prefixIndex), ceOutput.prefix(prefixIndex), mealy);
    }

    public void processBinarySearch(Word<I> ceInput, Word<O> ceOutput, LSMealyMachine<I, O> mealy) {
        LSState r = oqOracle.getTree().getSucc(oqOracle.getTree().defaultState(), ceInput);
        Objects.requireNonNull(r);
        this.updateFrontierAndBasis();
        if (this.frontierToBasisMap.containsKey(ceInput) || basis.contains(ceInput)) {
            return;
        }

        LSState q = mealy.getSuccessor(mealy.getInitialState(), ceInput);
        Word<I> accQT = basisMap.inverse().get(q);
        Objects.requireNonNull(accQT);

        NormalObservationTree<I, O> oTree = oqOracle.getTree();
        LSState qt = oTree.getSucc(oTree.defaultState(), accQT);
        Objects.requireNonNull(qt);

        Integer x = ceInput.prefixes(false).stream().filter(seq -> seq.length() != 0)
                .filter(seq -> frontierToBasisMap.containsKey(seq)).findFirst().get()
                .length();

        Integer y = ceInput.size();
        Integer h = (int) Math.floor((x.floatValue() + y.floatValue()) / 2.0);

        Word<I> sigma1 = ceInput.prefix(h);
        Word<I> sigma2 = ceInput.suffix(ceInput.size() - h);
        LSState qp = mealy.getSuccessor(mealy.getInitialState(), sigma1);
        Objects.requireNonNull(qp);
        Word<I> accQPt = basisMap.inverse().get(qp);
        Objects.requireNonNull(accQPt);

        Word<I> eta = Apartness.computeWitness(oTree, r, qt);
        Objects.requireNonNull(eta);

        Word<I> outputQuery = accQPt.concat(sigma2).concat(eta);
        Word<O> sulResponse = oqOracle.outputQuery(outputQuery);
        LSState qpt = oTree.getSucc(oTree.defaultState(), accQPt);
        Objects.requireNonNull(qpt);

        LSState rp = oTree.getSucc(oTree.defaultState(), sigma1);
        Objects.requireNonNull(rp);

        @Nullable
        Word<I> wit = Apartness.computeWitness(oTree, qpt, rp);
        if (wit != null) {
            processBinarySearch(sigma1, ceOutput.prefix(sigma1.length()), mealy);
        } else {
            Word<I> newInputs = accQPt.concat(sigma2);
            processBinarySearch(newInputs, sulResponse.prefix(newInputs.length()), mealy);
        }
    }

    private Set<Word<I>> DFS(LSState q, NormalObservationTree<I, O> tree, Set<Word<I>> seqs) {
        boolean no_outs = true;
        for(I sym : this.inputAlphabet){
            Pair<O, LSState> stepFree = tree.getOutSucc(q, sym);
            if (stepFree != null) {
                Set<Word<I>> newseqs = DFS(stepFree.getSecond(), tree, seqs);
                seqs.addAll(newseqs);
                no_outs = false;
            }
        }
        if(no_outs) {
            seqs.add(tree.getAccessSeq(q));
        }
        return seqs;
    }

    private void prioritizedSeparation() {
        Set<Word<I>> tree = DFS(oqOracle.getTree().defaultState(), oqOracle.getTree(), new HashSet<>());
        this.stateMatcher.updateMatching(tree, this.basis, this.oqOracle.getTree());
        for (Entry<Word<I>, List<Word<I>>> entry : frontierToBasisMap.entrySet()) {
            if (entry.getValue().size() <= 1) {
                continue;
            }
            Word<I> thisbasis = entry.getKey().subWord(0, entry.getKey().length()-1);
            List<Integer> match = this.stateMatcher.getBestMatch(this.basis.indexOf(thisbasis));
            Set<Word<I>> identifiers = new HashSet<>();
            for(Integer m : match) {
                Integer fm = this.rebuilder.getCombinedModel().getSuccessor(m, entry.getKey().lastSymbol());
                for(Integer st : this.stateMatcher.combinedModel.getStates()) {
                    identifiers.add(this.rebuilder.splittree.getDistinguishingSeq(fm, st));
                }
            }
            List<Word<I>> newCands = oqOracle.identifyFrontierMatching(entry.getKey(), entry.getValue(), identifiers);
            frontierToBasisMap.put(entry.getKey(), newCands);
        }
    }

    private void matchRefinement() {
        Set<Word<I>> tree = DFS(oqOracle.getTree().defaultState(), oqOracle.getTree(), new HashSet<>());
        this.stateMatcher.updateMatching(tree, this.basis, this.oqOracle.getTree());
        // if multiple highest matchings, use separating sequence output queries
        updateFrontierAndBasis();
        int oldbasissize = this.basis.size();
        for(int i=0; i < this.basis.size(); i++) {
            List<Integer> bestmatch = this.stateMatcher.getBestMatch(i);
            if(bestmatch.size() > 1) {
                // retrieve states
                for(int j=0; j < bestmatch.size(); j++) {
                    for(int k=j+1; k < bestmatch.size(); k++) {
                        Word<I> sepseq = this.rebuilder.splittree.getDistinguishingSeq(bestmatch.get(j),bestmatch.get(k));
                        if(sepseq != null) {
                            Word<I> pref = this.basis.get(i);
                            // perform output query
                            Word<I> outputQuery = pref.concat(sepseq);
                            int curSize = this.oqOracle.getTree().size();
                            Word<O> sulResponse = oqOracle.outputQuery(outputQuery);
                            if(this.oqOracle.getTree().size() > curSize) {
                                this.matchRefinementOQs += outputQuery.size();
                            }
                            // some bookkeeping for termination
                            NormalObservationTree<I,O> obstree = this.oqOracle.getTree();
                            LSState prefs = obstree.getSucc(obstree.defaultState(),basis.get(i));
                            if(myTreeAndHypShowsStatesAreApart(obstree, prefs, bestmatch.get(j), this.combinedModel) != null) {
                                this.stateMatcher.unmatched.add(Pair.of(pref,bestmatch.get(j)));
                            }
                            if(myTreeAndHypShowsStatesAreApart(obstree, prefs, bestmatch.get(k), this.combinedModel) != null) {
                                this.stateMatcher.unmatched.add(Pair.of(pref,bestmatch.get(k)));
                            }

                            updateFrontierAndBasis();
                            if(oldbasissize < this.basis.size()) {
                                this.matchRefinementStates += 1;
                                return;
                            }
                        }

                    }
                }
            }
        }
    }

    private Word<I> myTreeAndHypShowsStatesAreApart(
            NormalObservationTree<I, O> tree, LSState st, Integer sh, CompactMealy<I, O> fsm) {
        ArrayDeque<Pair<LSState, Integer>> queue = new ArrayDeque<>();
        if(sh == null) {
            return null;
        }
        queue.push(Pair.of(st, sh));
        while (!queue.isEmpty()) {
            Pair<LSState, Integer> pair = queue.pop();
            LSState q = pair.getFirst();
            Integer r = pair.getSecond();
            for (I i : tree.getInputAlphabet()) {
                Pair<O, LSState> stepFree = tree.getOutSucc(q, i);
                Integer dh = fsm.getSuccessor(r, i);
                if (stepFree != null && dh != null) {
                    Objects.requireNonNull(dh);
                    O outHyp = fsm.getOutput(r, i);
                    Objects.requireNonNull(outHyp);

                    if (outHyp.equals(stepFree.getFirst())) {
                        queue.push(Pair.of(stepFree.getSecond(), dh));
                    } else {
                        return tree.getTransferSeq(stepFree.getSecond(), st);
                    }
                }
            }
        }
        return null;
    }

    private void matchSeparation() {
        Set<Word<I>> tree = DFS(oqOracle.getTree().defaultState(), oqOracle.getTree(), new HashSet<>());
        this.stateMatcher.updateMatching(tree, this.basis, this.oqOracle.getTree());
        updateFrontierAndBasis();
        int oldbasissize = this.basis.size();
        // all basis states are sort of matched
        // now we want to check if the states in the reference lead to states that are not matched
        List<Integer> matched = new ArrayList<>(); // for each basis state, a list of best matched
        for(int i=0; i < this.basis.size(); i++) {
            List<Integer> bestmatches = this.stateMatcher.getBestMatch(i);
            if(bestmatches.size() == 1) { // only say that a state if matched if there is exactly one
                matched.add(bestmatches.get(0));
            }
        }
        // if a matched state has a transition to unmatched state, perform query
        for(int q=0; q < this.basis.size(); q++) {
            for (I sym : this.inputAlphabet) {
                List<Integer> bestmatches = this.stateMatcher.getBestMatch(q);
                for(Integer p : bestmatches) { // for each matched p
                    Integer pp = this.combinedModel.getSuccessor(p,sym);
                    Word<I> qp = this.basis.get(q).append(sym);
                    // pp unmatched and qp in ftbm
                    if(!matched.contains(pp) && this.frontierToBasisMap.containsKey(qp)) {
                        for(Word<I> r : this.basis) {
                            NormalObservationTree<I,O> obstree = this.oqOracle.getTree();
                            LSState rs = obstree.getSucc(obstree.defaultState(), r);
                            LSState qps = obstree.getSucc(obstree.defaultState(), qp);
                            Word<I> sepseqcheck = Apartness.computeWitness(obstree, qps, rs);
                            if(sepseqcheck==null && !this.stateMatcher.unmatched.contains(Pair.of(qp,pp))){
                                // compute sep seq between r and pp
                                Word<I> sepseq = myTreeAndHypShowsStatesAreApart(obstree, rs, pp, this.combinedModel);
                                if(sepseq != null) {
                                    int curSize = this.oqOracle.getTree().size();
                                    Word<O> sulResponse = oqOracle.outputQuery(qp.concat(sepseq));
                                    if(this.oqOracle.getTree().size() > curSize) {
                                        this.matchSeparationOQs += qp.concat(sepseq).size();
                                    }

                                    // either q' # p' -> unmatch
                                    // or q' and p' match and we should have found at leas a new apartness pair!
                                    if(myTreeAndHypShowsStatesAreApart(obstree, qps, pp, this.combinedModel) != null) {
                                        this.stateMatcher.unmatched.add(Pair.of(qp,pp));
                                    }
                                    updateFrontierAndBasis();
                                    if (oldbasissize < this.basis.size()) {
                                        this.matchSeparationStates += 1;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void makeObsTreeAdequate() {
        while (true) {
            List<Pair<Word<I>, List<Word<I>>>> newFrontier = oqOracle.exploreFrontier(basis);

            for (Pair<Word<I>, List<Word<I>>> pair : newFrontier) {
                frontierToBasisMap.put(pair.getFirst(), pair.getSecond());
            }

            int oldtreesize = this.oqOracle.getTree().size();
            if(this.stateMatchingEnabled) {
                prioritizedSeparation();
            }

            if(oldtreesize == this.oqOracle.getTree().size()) {
                for (Entry<Word<I>, List<Word<I>>> entry : frontierToBasisMap.entrySet()) {
                    if (entry.getValue().size() <= 1) {
                        continue;
                    }
                    List<Word<I>> newCands = oqOracle.identifyFrontier(entry.getKey(), entry.getValue());
                    frontierToBasisMap.put(entry.getKey(), newCands);
                }
            }

            this.promoteFrontierState();

            int oldbasissize = this.basis.size();
            if (this.treeIsAdequate() && this.references.size() > 0 && this.stateMatchingEnabled) {
                matchRefinement();
                matchSeparation();
            }

            if (this.treeIsAdequate() && oldbasissize == this.basis.size()) {
                break;
            }
        }
    }

    public void promoteFrontierState() {
        Word<I> newBS = frontierToBasisMap.entrySet().stream().filter(e -> e.getValue().isEmpty()).findFirst()
                .map(e -> e.getKey()).orElse(null);
        if (newBS == null) {
            return;
        }

        Word<I> bs = Word.fromWords(newBS);
        basis.add(bs);
        this.stateMatcher.updateMatchingForNewBasis(bs, this.basis, this.oqOracle.getTree());
        frontierToBasisMap.remove(bs);
        NormalObservationTree<I, O> oTree = oqOracle.getTree();
        frontierToBasisMap.entrySet().parallelStream().filter(e -> !Apartness.accStatesAreApart(oTree, e.getKey(), bs))
                .forEach(e -> {
                    frontierToBasisMap.get(e.getKey()).add(bs);
                });
    }


    public boolean treeIsAdequate() {
        this.checkFrontierConsistency();
        if (frontierToBasisMap.values().stream().anyMatch(x -> x.size() != 1)) {
            return false;
        }

        NormalObservationTree<I, O> oTree = oqOracle.getTree();
        LinkedList<Pair<Word<I>, I>> basisIpPairs = new LinkedList<>();
        for (Word<I> b : basis) {
            for (I i : inputAlphabet) {
                basisIpPairs.add(Pair.of(b, i));
            }
        }

        boolean check = basisIpPairs.stream().anyMatch(p -> {
            LSState q = oTree.getSucc(oTree.defaultState(), p.getFirst());
            return oTree.getOut(q, p.getSecond()) == null;
        });

        if (check) {
            return false;
        }

        return true;
    }

    public void updateFrontierAndBasis() {
        NormalObservationTree<I, O> oTree = oqOracle.getTree();
        frontierToBasisMap.entrySet().parallelStream()
                .forEach(e -> e.getValue().removeIf(bs -> Apartness.accStatesAreApart(oTree, e.getKey(), bs)));

        this.promoteFrontierState();
        this.checkFrontierConsistency();

        frontierToBasisMap.entrySet().parallelStream()
                .forEach(e -> e.getValue().removeIf(bs -> Apartness.accStatesAreApart(oTree, e.getKey(), bs)));
    }

    public LSMealyMachine<I, O> buildHypothesis() {
        while (true) {
            this.makeObsTreeAdequate();
            LSMealyMachine<I, O> hyp = this.constructHypothesis();
            DefaultQuery<I, Word<O>> ce = this.checkConsistency(hyp);
            if (ce != null) {
                consistentCECount += 1;
                this.processCex(ce, hyp);
            } else {
                return hyp;
            }
        }
    }

    public LSMealyMachine<I, O> constructHypothesis() {
        basisMap.clear();
        frontierTransitionsSet.clear();

        for (Word<I> bAcc : basis) {
            LSState s = new LSState(basisMap.size());
            basisMap.put(bAcc, s);
        }

        NormalObservationTree<I, O> oTree = oqOracle.getTree();
        LinkedList<Word<I>> basisCopy = new LinkedList<>(basis);
        HashSet<Word<I>> loopbacks = new HashSet<>();
        HashMap<Pair<LSState, I>, Pair<LSState, O>> transFunction = new HashMap<>();
        for (Word<I> q : basisCopy) {
            for (I i : inputAlphabet) {
                LSState bs = oTree.getSucc(oTree.defaultState(), q);
                Objects.requireNonNull(bs);
                O output = oTree.getOut(bs, i);
                Objects.requireNonNull(output);
                Word<I> fAcc = q.append(i);

                Pair<Word<I>, Boolean> pair = this.identifyFrontierOrBasis(fAcc);
                Word<I> dest = pair.getFirst();
                Boolean isLoopback = pair.getSecond();

                if (isLoopback) {
                    loopbacks.add(fAcc);
                }

                LSState hypBS = basisMap.get(q);
                Objects.requireNonNull(hypBS);
                LSState hypDest = basisMap.get(dest);
                Objects.requireNonNull(hypDest);
                transFunction.put(Pair.of(hypBS, i), Pair.of(hypDest, output));
            }
        }

        return new LSMealyMachine<I, O>(inputAlphabet, basisMap.values(), new LSState(0), transFunction);
    }

    public Pair<Word<I>, Boolean> identifyFrontierOrBasis(Word<I> seq) {
        if (basis.contains(seq)) {
            return Pair.of(seq, false);
        }

        Word<I> bs = frontierToBasisMap.get(seq).stream().findFirst().get();
        return Pair.of(bs, true);
    }

    public void initObsTree(@Nullable List<Pair<Word<I>, Word<O>>> logs) {
        if (logs != null) {
            for (Pair<Word<I>, Word<O>> pair : logs) {
                oqOracle.addObservation(pair.getFirst(), pair.getSecond());
            }
        }
    }

    public void checkFrontierConsistency() {
        LinkedList<Word<I>> basisSet = new LinkedList<>(basis);
        NormalObservationTree<I, O> oTree = oqOracle.getTree();

        List<Pair<Word<I>, I>> stateInputIterator = new LinkedList<>();
        for (Word<I> bs : basisSet) {
            for (I i : inputAlphabet) {
                stateInputIterator.add(Pair.of(bs, i));
            }
        }
        stateInputIterator.stream().map(p -> {
            Word<I> fsAcc = p.getFirst().append(p.getSecond());
            if (oTree.getSucc(oTree.defaultState(), fsAcc) != null) {
                return fsAcc;
            } else {
                return null;
            }
        }).filter(s -> s != null).filter(x -> !basis.contains(x)).filter(x -> !frontierToBasisMap.containsKey(x))
                .map(fs -> {
                    List<Word<I>> cands = basis.parallelStream().filter(s -> !Apartness.accStatesAreApart(oTree, fs, s))
                            .collect(Collectors.toList());
                    return Pair.of(fs, cands);
                }).forEach(p -> {
                    frontierToBasisMap.put(p.getFirst(), p.getSecond());
                });
    }

    public DefaultQuery<I, Word<O>> checkConsistency(LSMealyMachine<I, O> mealy) {
        NormalObservationTree<I, O> oTree = oqOracle.getTree();
        @Nullable
        Word<I> wit = treeAndHypComputeWitness(oTree, oTree.defaultState(), mealy, new LSState(0));
        if (wit == null) {
            return null;
        }

        Word<O> os = oTree.getObservation(null, wit);
        Objects.requireNonNull(os);
        return new DefaultQuery<>(wit, os);
    }

    @Override
    public void startLearning() {
        this.initObsTree(null);
    }

    @Override
    public boolean refineHypothesis(DefaultQuery<I, Word<O>> ceQuery) {
        LSMealyMachine<I, O> oldHyp = this.constructHypothesis();
        processCex(ceQuery, oldHyp);
        MealyMachine<?, I, ?, O> newHyp = getHypothesisModel();
        return DeterministicEquivalenceTest.findSeparatingWord(oldHyp, newHyp, inputAlphabet) != null;
    }

    @Override
    public MealyMachine<?, I, ?, O> getHypothesisModel() {
        return buildHypothesis();
    }
}
