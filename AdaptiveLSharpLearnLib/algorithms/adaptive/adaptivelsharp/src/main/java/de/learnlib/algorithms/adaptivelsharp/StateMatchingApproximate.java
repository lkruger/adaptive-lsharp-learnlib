package de.learnlib.algorithms.adaptivelsharp;

import de.learnlib.algorithms.lsharp.*;
import net.automatalib.automata.transducers.impl.compact.CompactMealy;
import net.automatalib.commons.util.Pair;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;

import java.util.*;

/**
 * Quantitative state matching
 *
 * @param <I>
 *         input symbol type
 *
 * @author Loes Kruger
 */
public class StateMatchingApproximate<I, O> extends StateMatching<I,O>{
    protected ArrayList<int[]> totalmatchings;
    public StateMatchingApproximate(Alphabet<I> inputAlphabet, CompactMealy<I,O> combinedModel) {
        super(inputAlphabet, combinedModel);
        super.matchings.add(new int[combinedModel.size()]);
        this.totalmatchings = new ArrayList();
        this.totalmatchings.add(new int[combinedModel.size()]);
    }

    public void initializeStateMatching(List<Word<I>> basis) {
        for(Word<I> b : basis) {
            if(!b.isEmpty()) {
                this.matchings.add(new int[this.combinedModel.size()]);
                this.totalmatchings.add(new int[this.combinedModel.size()]);
            }
        }
    }

    public List<Integer> getBestMatch(int i) {
        return argmax(i);
    }

    public void printMatchTable(List<Word<I>> basis) {
        System.out.format("%7s - %7s - %7s  | %10s \n", "state", "match", "score", "old states");
        for(int i=0; i < basis.size(); i++) {
            int highest;
            float score = 0;
            if(!argmax(i).isEmpty()) {
                highest = argmax(i).get(0);
                score = (float) this.matchings.get(i)[highest]/this.totalmatchings.get(i)[highest];
            }
            System.out.format("%7d - %7s - %7f |", i, argmax(i).toString(), score);
            for(int y = 0; y < this.matchings.get(i).length; y++) {
                System.out.format("%10s", this.matchings.get(i)[y] + "/" + this.totalmatchings.get(i)[y]);
            }
            System.out.println();
        }
    }

    public void updateMatching(Set<Word<I>> tree, List<Word<I>> basis, NormalObservationTree<I,O> obsTree) {
        // find words that are not in prev tree
        for (Word<I> w : tree) {
            if (this.prevMatchTree.contains(w)) {
                continue;
            }
            // found a seq that was not in the tree
            Word<I> commonPart = longestCommonPref(this.prevMatchTree, w);
            for (int i = 0; i < basis.size(); i++) {
                Word<I> b = basis.get(i);
                if (!b.isPrefixOf(w)) {
                    continue;
                }
                Word<I> part1 = Word.epsilon();
                Word<I> part2;
                // we want to score the new part of w, which is called part2
                // if some part1 under b was already present, we need to check if it only contains
                // input symbols in the alphabet of reference state j
                if(commonPart.size() > b.size()) {
                    part1 = commonPart.subWord(b.size());
                    part2 = w.subWord(commonPart.size());
                } else {
                    part2 = w.subWord(b.size());
                }
                // basis state b should be recalculated
                for (int j = 0; j < this.combinedModel.size(); j++) {
                    if(checkPart1(part1,j)) {
                        LSState newb = obsTree.getSucc(obsTree.defaultState(), b.concat(part1));
                        int newj = this.combinedModel.getSuccessor(j, part1);
                        scorePart2(part2,newb,newj,i,j,obsTree);
                    }
                }
            }
            // add w to prevTree and delete strict subwords
            this.prevMatchTree.removeIf(wp -> wp.isPrefixOf(w));
            this.prevMatchTree.add(w);

        }
        this.prevMatchTree = tree;
    }

    public void scorePart2(Word<I> part2, LSState targetState, int referenceState, int basisIndex, int referenceIndex, NormalObservationTree<I,O> obsTree) {
        for (I sym : part2) {
            Pair<O, LSState> targetpair = obsTree.getOutSucc(targetState, sym);
            O targetOutput = targetpair.getFirst();
            O referenceOutput = this.combinedModel.getOutput(referenceState, sym);
            if (referenceOutput == null) {
                break;
            }
            this.totalmatchings.get(basisIndex)[referenceIndex] += 1;
            if (targetOutput.equals(referenceOutput)) {
                matchings.get(basisIndex)[referenceIndex] += 1;
            }
            targetState = targetpair.getSecond();
            referenceState = this.combinedModel.getSuccessor(referenceState, sym);
        }
    }

    public List<Integer> argmax(int idx) {
        int[] array = this.matchings.get(idx);
        int[] total = this.totalmatchings.get(idx);
        float max = 0; //array[0]/total[0];
        List<Integer> re = new ArrayList<>();
        for (int i = 0; i < this.combinedModel.size(); i++) {
            if(total[i] == 0) {
                continue;
            }
            if (Math.abs((float) array[i]/total[i] - max) < 0.00001 ) {
                re.add(i);
            }
            if ((float) array[i]/total[i] > max) {
                max = (float) array[i]/total[i];
                re = new ArrayList<>();
                re.add(i);
            }
         }

        return re;
    }

    public void updateMatchingForNewBasis(Word<I> bs, List<Word<I>> basis, NormalObservationTree<I,O> obsTree) {
        this.matchings.add(new int[combinedModel.size()]);
        this.totalmatchings.add(new int[combinedModel.size()]);

        for (Word<I> w : this.prevMatchTree) {
            if (!bs.isPrefixOf(w)) {
                continue;
            }
            Word<I> part2 = w.subWord(bs.size());
            for (int j = 0; j < this.combinedModel.size(); j++) {
                int i = basis.indexOf(bs);
                LSState bstate = obsTree.getSucc(obsTree.defaultState(), bs);
                scorePart2(part2,bstate,j,i,j,obsTree);
            }
        }
    }
}
