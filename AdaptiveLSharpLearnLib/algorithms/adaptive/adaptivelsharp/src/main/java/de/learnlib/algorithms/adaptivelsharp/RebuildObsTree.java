package de.learnlib.algorithms.adaptivelsharp;

import com.google.common.collect.Sets;
import de.learnlib.algorithms.lsharp.*;
import net.automatalib.automata.base.compact.CompactTransition;
import net.automatalib.automata.transducers.impl.compact.CompactMealy;
import net.automatalib.commons.util.Pair;
import net.automatalib.util.automata.cover.Covers;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import net.automatalib.words.impl.Alphabets;

import java.util.*;

/**
 * Class to rebuild the obs tree based on the references for adaptive L#
 *
 * @param <I>
 *         input symbol type
 *
 * @author Loes Kruger
 */
public class RebuildObsTree<I, O>{
    protected LSOracleMatching<I, O> oqOracle;
    protected Alphabet<I> inputAlphabet;
    protected List<Word<I>> initbasis;
    protected HashMap<Word<I>, List<Word<I>>> initFrontierToBasisMap;
    protected SplittingTree<Integer, I, O> splittree;
    private HashSet<Word<I>> unionW;
    public CompactMealy<I, O> combinedModel;
    public int rebuildOQs;
    public List<CompactMealy<I,O>> references;
    public HashMap<Pair<CompactMealy<I, O>, Integer>, Integer> maptonew;



    public RebuildObsTree(LSOracleMatching<I, O> oqOracle, Alphabet<I> inputAlphabet, List<CompactMealy<I, O>> references, boolean rebuild) {
        this.inputAlphabet = inputAlphabet;
        this.initbasis = new LinkedList<>();
        initbasis.add(Word.epsilon());
        this.initFrontierToBasisMap = new HashMap<>();
        this.oqOracle = oqOracle;

        this.unionW = new HashSet<>();
        this.rebuildOQs = 0;
        this.references = references;

        this.maptonew = buildCombinedModel();

        // combine the models to compute splitting tree
        this.splittree = new SplittingTree<>(combinedModel, this.inputAlphabet, (List<Integer>) combinedModel.getStates());

        // save the separating sequences
        for (int x = 0; x <= combinedModel.size(); x++) {
            for (int y = x + 1; y <= combinedModel.size(); y++) {
                Word<I> entry = this.splittree.getDistinguishingSeq(x, y);
                if (entry != null) {
                    this.unionW.add(entry);
                }
            }
        }

        // gather state cover and characterization set for reference models
        Map<CompactMealy<I, O>, List<Word<I>>> stateCoverMMap = new HashMap<>();
        List<Word<I>> stateCovers = new ArrayList<>();
        for (CompactMealy<I, O> M : this.references) {
            // Compute intersection between alphabets
            Set<I> setTarget = new HashSet<>(this.inputAlphabet);
            Set<I> setM = new HashSet<>(M.getInputAlphabet());
            setTarget.retainAll(setM);
            List<I> newinputs = new ArrayList<>(setTarget);
            Alphabet<I> CommonAlphabet = Alphabets.fromList(newinputs);

            // Compute state cover with common inputs
            List<Word<I>> stateCover = new ArrayList<>();
            Set<Word<I>> transitionCover = Sets.newHashSetWithExpectedSize(M.size() * M.numInputs());
            Covers.cover(M, CommonAlphabet, stateCover, transitionCover);
            stateCoverMMap.put(M, stateCover);
            stateCovers.addAll(stateCover);
        }
        HashSet<Word<I>> stateCoverSet = new HashSet(stateCovers);
        stateCovers = new ArrayList(stateCoverSet);

        if(rebuild) {
            rebuild(stateCovers, stateCoverMMap);
        }
        rebuildFTBM();
    }

    protected CompactMealy<I,O> getCombinedModel() {
        return this.combinedModel;
    }

    protected List<Word<I>> getBasis() {
        return this.initbasis;
    }

    protected HashMap<Word<I>, List<Word<I>>> getFTBM() {
        return this.initFrontierToBasisMap;
    }

    private void rebuildFTBM() {
        // make new ftbm
        NormalObservationTree<I, O> oTree = this.oqOracle.getTree();
        HashMap<Word<I>, List<Word<I>>> newftbm = new HashMap<>();
        for (Word<I> b : this.initbasis) {
            for (I i : this.inputAlphabet) {
                Word<I> qf = b.append(i);
                LSState iqf = oTree.getSucc(oTree.defaultState(), qf);
                if (iqf != null && !this.initbasis.contains(qf)) {
                    List<Word<I>> notapart = new ArrayList<>();
                    for (Word<I> bp : this.initbasis) {
                        if (qf != bp) {
                            LSState ibp = oTree.getSucc(oTree.defaultState(), bp);
                            if (Apartness.computeWitness(oTree, iqf, ibp) == null) {
                                notapart.add(bp);
                            }
                        }
                    }
                    newftbm.put(qf, notapart);
                }
            }
        }
        this.initFrontierToBasisMap = newftbm;
    }

    private Pair<CompactMealy<I,O>,Pair<Word<I>,Word<I>>> findPair(List<Word<I>> stateCovers, List<Word<I>> mybasis, NormalObservationTree<I,O> tree, Map<CompactMealy<I, O>, List<Word<I>>> stateCoverMMap) {
        // returns frontier state in statecovers
        for (Word<I> b : mybasis) {
            for (I i : this.inputAlphabet) {
                Word<I> qf = b.append(i);
                if (!stateCovers.contains(qf) || mybasis.contains(qf)) {
                    continue;
                }
                for (Word<I> r : mybasis) {
                    // consider all models where basis and this state are in the state cover
                    for (CompactMealy<I, O> M : this.references) {
                        List<Word<I>> mystatecover = stateCoverMMap.get(M);
                        // only go to compute if the state cover contains b and qf
                        if (!mystatecover.contains(r) || !mystatecover.contains(qf)) {
                            continue;
                        }

                        // if qf is defined, check if it is not already apart
                        LSState iqf = tree.getSucc(tree.defaultState(), qf);
                        LSState ir = tree.getSucc(tree.defaultState(), r);
                        if (tree.getSucc(tree.defaultState(), qf) != null) {
                            if (Apartness.computeWitness(tree, iqf, ir) != null) {
                                // already apart
                                continue;
                            }
                        }

                        Integer rstate = M.getState(r);
                        Integer qfstate = M.getState(qf);
                        Integer newrstate = this.maptonew.get(Pair.of(M, rstate));
                        Integer newqfstate = this.maptonew.get(Pair.of(M, qfstate));

                        Word<I> sigma = this.splittree.getDistinguishingSeq(newrstate, newqfstate);
                        // can be null due to other alphabets
                        if(sigma != null) {
                            if(tree.getSucc(tree.defaultState(), qf.concat(sigma)) == null || tree.getSucc(tree.defaultState(), r.concat(sigma)) == null) {
                                return Pair.of(M,Pair.of(qf, r));
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    // manual check if qf is apart from all states in the basis
    private boolean checkIfApart(Word<I> qf, List<Word<I>> mybasis) {
        NormalObservationTree<I, O> oTree = this.oqOracle.getTree();
        LSState iqf = oTree.getSucc(oTree.defaultState(), qf);
        boolean allapart = true;
        for (Word<I> mb : mybasis) {
            LSState imb = oTree.getSucc(oTree.defaultState(), mb);
            Word<I> eta = Apartness.computeWitness(oTree, imb, iqf);
            if (eta == null) {
                allapart = false;
                break;
            }
        }
        return allapart;
    }

    private void rebuild(List<Word<I>> stateCovers, Map<CompactMealy<I, O>, List<Word<I>>> stateCoverMMap) {
        while (findPair(stateCovers, this.initbasis, this.oqOracle.getTree(), stateCoverMMap) != null) {
            // choose a frontier state which is undefined or not isolated
            Pair<CompactMealy<I,O>,Pair<Word<I>,Word<I>>> pair = findPair(stateCovers, this.initbasis, this.oqOracle.getTree(), stateCoverMMap);
            CompactMealy<I,O> M = pair.getFirst();
            Word<I> qf = pair.getSecond().getFirst();
            Word<I> b = pair.getSecond().getSecond();

            Integer bstate = M.getState(b);
            Integer qfstate = M.getState(qf);
            Integer newbstate = this.maptonew.get(Pair.of(M, bstate));
            Integer newqfstate = this.maptonew.get(Pair.of(M, qfstate));

            Word<I> sigma = this.splittree.getDistinguishingSeq(newbstate, newqfstate);
            // only ask OQ if there is a distinguishing sequence between qf and b
            if (sigma == null) {
                continue;
            }
            // add witness sigma to qf and to b
            Word<I> query1 = qf.concat(sigma);

            int curSize = this.oqOracle.getTree().size();
            Word<O> sulResponse1 = oqOracle.outputQuery(query1);
            if (this.oqOracle.getTree().size() > curSize) {
                this.rebuildOQs += query1.size();
            }

            Word<I> query2 = b.concat(sigma);
            Word<O> sulResponse2 = oqOracle.outputQuery(query2);
            this.rebuildOQs += query2.size();

            // if any of the states in the statecover is apart from the basis we can add them
            for(Word<I> f : stateCovers) {
                if(this.oqOracle.getTree().getSucc(this.oqOracle.getTree().defaultState(), f) != null) {
                    // f should be apart from all basis states and in the frontier
                    if (checkIfApart(f, this.initbasis) && !this.initbasis.contains(f) && this.initbasis.contains(f.subWord(0,f.length()-1))) {
                        this.initbasis.add(f);
                    }
                }
            }
        }
    }

    // combines all reference models into one combined model
    private HashMap<Pair<CompactMealy<I, O>, Integer>, Integer> buildCombinedModel() {
        HashMap<Pair<CompactMealy<I, O>, Integer>, Integer> maptonew = new HashMap<>();
        CompactMealy<I, O> combinedModel = new CompactMealy<>(this.inputAlphabet);
        int counter = 0;
        // new id number for each state
        for (CompactMealy<I, O> M : this.references) {
            for (Integer q : M.getStates()) {
                maptonew.put(Pair.of(M, q), counter);
                combinedModel.addState();
                counter += 1;
            }
        }
        // add the transitions
        for (CompactMealy<I, O> M : this.references) {
            for (Integer q : M.getStates()) {
                for (I i : this.inputAlphabet) {
                    if (M.getInputAlphabet().containsSymbol(i)) {
                        CompactTransition<O> trans = M.getTransition(q, i);
                        if (trans != null) {
                            Integer combinedModelState = maptonew.get(Pair.of(M, q));
                            Integer successorInCombined = maptonew.get(Pair.of(M, trans.getSuccId()));
                            CompactTransition<O> newtrans = new CompactTransition<>(successorInCombined, trans.getProperty());
                            combinedModel.addTransition(combinedModelState, i, newtrans);
                        }
                    }
                }
            }
        }
        combinedModel.setInitial(0, true);
        this.combinedModel = combinedModel;
        return maptonew;
    }

    public int getRebuildOQs() {
        return this.rebuildOQs;
    }

    public int getRebuildStates() {
        return this.initbasis.size() - 1;
    }
}


