package de.learnlib.algorithms.adaptivelsharp;

import net.automatalib.words.Word;

import java.util.*;

/**
 * Adapted splitting node class
 *
 * @author Loes Kruger
 */
public class SplittingNode<S, I, O> {
    public List<S> label;
    public HashMap<O, Integer> children = new HashMap<>();
    public Word<I> splitter;

    public SplittingNode(List<S> block) {
        this.label = new LinkedList<>(new HashSet<>(block));
    }


    public boolean hasState(S state) {
        return this.label.contains(state);
    }

    public boolean isSeparated() {
        return !this.children.isEmpty();
    }

    public Integer size() {
        return this.label.size();
    }

}
