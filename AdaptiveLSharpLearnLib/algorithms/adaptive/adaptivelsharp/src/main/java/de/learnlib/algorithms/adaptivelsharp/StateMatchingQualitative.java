package de.learnlib.algorithms.adaptivelsharp;

import de.learnlib.algorithms.lsharp.LSState;
import de.learnlib.algorithms.lsharp.NormalObservationTree;
import net.automatalib.automata.transducers.impl.compact.CompactMealy;
import net.automatalib.commons.util.Pair;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;

import java.util.*;

/**
 * Qualitative State Matching
 *
 * @param <I>
 *         input symbol type
 *
 * @author Loes Kruger
 */
public class StateMatchingQualitative<I, O> extends StateMatching<I,O>{

    public StateMatchingQualitative(Alphabet<I> inputAlphabet, CompactMealy<I,O> combinedModel) {
        super(inputAlphabet, combinedModel);
        int[] arr = new int[this.combinedModel.size()];
        Arrays.fill(arr, 1);
        super.matchings.add(arr);
    }

    public void initializeStateMatching(List<Word<I>> basis) {
        for(Word<I> b : basis) {
            if(!b.isEmpty()) {
                int[] arr = new int[this.combinedModel.size()];
                Arrays.fill(arr, 1);
                this.matchings.add(arr);
            }
        }
    }

    public List<Integer> getBestMatch(int i) {
        return argmax(i);
    }

    public void printMatchTable(List<Word<I>> basis) {
        System.out.format("%7s - %7s - %7s  | %10s \n", "state", "match", "score", "old states");
        for(int i=0; i < basis.size(); i++) {
            int highest;
            float score = 0;
            if(!argmax(i).isEmpty()) {
                highest = argmax(i).get(0);
                score = this.matchings.get(i)[highest];
            }
            System.out.format("%7d - %7s - %7s |", i, argmax(i).toString(), score);
            for(int y = 0; y < this.matchings.get(i).length; y++) {
                System.out.format("%7s", this.matchings.get(i)[y]);
            }
            System.out.println();
        }
    }

    public void updateMatching(Set<Word<I>> tree, List<Word<I>> basis, NormalObservationTree<I,O> obsTree) {
        // find words that are not in prev tree
        for (Word<I> w : tree) {
            if (this.prevMatchTree.contains(w)) {
                continue;
            }
            // found a seq that was not in the tree
            Word<I> commonPart = longestCommonPref(this.prevMatchTree, w);
            for (int i = 0; i < basis.size(); i++) {
                Word<I> b = basis.get(i);
                if (!b.isPrefixOf(w)) {
                    continue;
                }
                Word<I> part1 = Word.epsilon();
                Word<I> part2;
                // we want to score the new part of w, which is called part2
                // if some part1 under b was already present, we need to check if it only contains
                // input symbols in the alphabet of reference state j
                if(commonPart.size() > b.size()) {
                    part1 = commonPart.subWord(b.size());
                    part2 = w.subWord(commonPart.size());
                } else {
                    part2 = w.subWord(b.size());
                }
                // basis state b should be recalculated
                for (int j = 0; j < this.combinedModel.size(); j++) {
                    // only recalculate if prev match is 1
                    if(this.matchings.get(i)[j] == 1) {
                        if(checkPart1(part1,j)) {
                            LSState newb = obsTree.getSucc(obsTree.defaultState(), b.concat(part1));
                            int newj = this.combinedModel.getSuccessor(j, part1);
                            scorePart2(part2,newb,newj,i,j,obsTree);
                        }
                    }
                }
            }
            // add w to prevTree and delete strict subwords
            this.prevMatchTree.removeIf(wp -> wp.isPrefixOf(w));
            this.prevMatchTree.add(w);

        }
        this.prevMatchTree = tree;
    }

    public void scorePart2(Word<I> part2, LSState targetState, int referenceState, int basisIndex, int referenceIndex, NormalObservationTree<I,O> obsTree) {
        for (I sym : part2) {
            Pair<O, LSState> targetpair = obsTree.getOutSucc(targetState, sym);
            O targetOutput = targetpair.getFirst();
            O referenceOutput = this.combinedModel.getOutput(referenceState, sym);
            if (referenceOutput == null) {
                break;
            }
            if (!targetOutput.equals(referenceOutput)) {
                matchings.get(basisIndex)[referenceIndex] = 0;
                break;
            }
            targetState = targetpair.getSecond();
            referenceState = this.combinedModel.getSuccessor(referenceState, sym);
        }
    }

    public List<Integer> argmax(int idx) {
        int[] array = this.matchings.get(idx);
        List<Integer> re = new ArrayList<>();
        for (int i = 0; i < this.combinedModel.size(); i++) {
            if(array[i] == 1) {
                re.add(i);
            }
         }
        return re;
    }

    public void updateMatchingForNewBasis(Word<I> bs, List<Word<I>> basis, NormalObservationTree<I,O> obsTree) {
        int[] arr = new int[this.combinedModel.size()];
        Arrays.fill(arr, 1);
        this.matchings.add(arr);

        for (Word<I> w : this.prevMatchTree) {
            if (!bs.isPrefixOf(w)) {
                continue;
            }
            Word<I> part2 = w.subWord(bs.size());
            for (int j = 0; j < this.combinedModel.size(); j++) {
                int i = basis.indexOf(bs);
                LSState bstate = obsTree.getSucc(obsTree.defaultState(), bs);
                scorePart2(part2,bstate,j,i,j,obsTree);
            }
        }
    }
}
