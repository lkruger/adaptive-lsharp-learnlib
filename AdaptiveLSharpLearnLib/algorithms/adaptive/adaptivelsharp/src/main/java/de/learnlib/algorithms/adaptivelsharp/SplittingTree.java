package de.learnlib.algorithms.adaptivelsharp;

import de.learnlib.algorithms.lsharp.ads.ArenaTree;
import net.automatalib.automata.transducers.MealyMachine;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Adapted splitting tree class
 *
 * @author Loes Kruger
 */
public class SplittingTree<S, I, O> {
    public de.learnlib.algorithms.lsharp.ads.ArenaTree<SplittingNode<S, I, O>, Void> tree = new ArenaTree<>();


    public SplittingTree(MealyMachine<S, I, ?, O> fsm, Alphabet<I> inputAlphabet, List<S> rootLabel) {
        this.construct(fsm, inputAlphabet, rootLabel);
    }

    public void construct(MealyMachine<S, I, ?, O> fsm, Alphabet<I> inputAlphabet, List<S> initialLabel) {

        // Should contain all states that we want to split
        SplittingNode<S, I, O> rootNode;
        rootNode = new SplittingNode<>(initialLabel);
        List<SplittingNode<S, I, O>> workqueue = new ArrayList<>();
        List<SplittingNode<S, I, O>> workqueueStateSplit = new ArrayList<>();
        workqueue.add(rootNode);
        Integer rootIndex = this.tree.node(rootNode);

        // splitting on input
        while (!workqueue.isEmpty()) {
            boolean splitted = false;
            SplittingNode q = workqueue.remove(0);
            for (I input : inputAlphabet) {
                HashMap<O, List<S>> splits = new HashMap<>();
                // check if i splits the block
                for (Object child : q.label) {
                    O outp = fsm.getOutput((S) child, input);
                    if (splits.containsKey(outp)) {
                        List<S> thissplit = splits.get(outp);
                        thissplit.add((S) child);
                    } else {
                        List<S> newsplit = new ArrayList<>();
                        newsplit.add((S) child);
                        splits.put(outp, newsplit);
                    }
                }
                // if splits the block
                if (splits.size() > 1) {
                    for (O outp : splits.keySet()) {
                        splitted = true;
                        SplittingNode<S, I, O> c = new SplittingNode<>(splits.get(outp));
                        Integer childNodeIndex = this.tree.node(c);
                        q.children.put(outp, childNodeIndex);
                        q.splitter = Word.fromLetter(input);
                        // if there is a child which is not completely split, we add it to the work queue
                        if (splits.get(outp).size() > 1) {
                            workqueue.add(c);
                        }
                    }
                }
                if (splitted) {
                    break;
                }
            }
            // node cannot be split on input, try split on state later on
            if (!splitted && !workqueueStateSplit.contains(q)) {
                workqueueStateSplit.add(q);
            }
        }

        // Splitting on state
        int counter = 0;
        while (!workqueueStateSplit.isEmpty()) {
            counter += 1;
            if (counter > 2000) {
                break;
            }
            boolean splitted = false;
            SplittingNode q = workqueueStateSplit.remove(0);
            for (I input : inputAlphabet) {
                // determine successors states
                List<S> successorsStates = new ArrayList<>();
                for (Object qs : q.label) {
                    successorsStates.add(fsm.getSuccessor((S) qs, input));
                }
                // check if successors states are split in any of the nodes
                Word<I> splittingword = getSeparatingSeq(rootNode, successorsStates);
                if (splittingword != null) {
                    Word<I> finalsplittingword = Word.fromLetter(input).concat(splittingword);
                    HashMap<Word<O>, List<S>> splits = new HashMap<>();
                    // check if i splits the block
                    for (Object child : q.label) {
                        Word<O> outp = fsm.computeStateOutput((S) child, finalsplittingword);
                        if (splits.containsKey(outp)) {
                            List<S> thissplit = splits.get(outp);
                            thissplit.add((S) child);
                        } else {
                            List<S> newsplit = new ArrayList<>();
                            newsplit.add((S) child);
                            splits.put(outp, newsplit);
                        }
                    }
                    // if splits the block
                    if (splits.size() > 1) {
                        for (Word<O> outp : splits.keySet()) {
                            splitted = true;
                            SplittingNode<S, I, O> c = new SplittingNode<>(splits.get(outp));
                            Integer childNodeIndex = this.tree.node(c);
                            q.children.put(outp, childNodeIndex);
                            q.splitter = finalsplittingword;
                            // if there is a child which is not completely split, we add it to the work queue
                            if (splits.get(outp).size() > 1 && !workqueueStateSplit.contains(c)) {
                                workqueueStateSplit.add(c);
                            }
                        }
                    }
                }
                if (splitted) {
                    break;
                }
            }
            if (!splitted) {
                // insert the not split block at the end of the worklist, amybe we cna split next time
                workqueueStateSplit.add(q);
            }

        }

    }

    public Word<I> getDistinguishingSeq(S q, S p) {
        List<S> tosep = new ArrayList<>();
        tosep.add(q);
        tosep.add(p);
        SplittingNode<S, I, O> rootNode = this.tree.get(0);
        return getSeparatingSeq(rootNode, tosep);
    }

    public Word<I> getSeparatingSeq(SplittingNode<S,I,O> rootNode, List<S> blockIn) {
        ArrayList<SplittingNode<S,I,O>> tocheck = new ArrayList<>();
        tocheck.add(rootNode);

        while(!tocheck.isEmpty()) {
            SplittingNode<S,I,O> q = tocheck.remove(0);
            for(Integer child : q.children.values()) {
                SplittingNode<S, I, O> mychild = this.tree.get(child);
                HashSet<S> mychildset = new HashSet<>(mychild.label);
                HashSet<S> mylabelset = new HashSet<>(blockIn);
                mychildset.retainAll(mylabelset);
                // split on states if the child node has only a partition of blockin
                if(!mychildset.isEmpty() && !mychildset.equals(mylabelset)) {
                    // return splitter of that split
                    return q.splitter;
                }
                // if splitter of q does not split, we check the children
                tocheck.add(mychild);
            }
        }

        return null;
    }

}
