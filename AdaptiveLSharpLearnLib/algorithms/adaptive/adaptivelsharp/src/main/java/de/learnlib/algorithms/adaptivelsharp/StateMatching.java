package de.learnlib.algorithms.adaptivelsharp;

import de.learnlib.algorithms.lsharp.LSState;
import de.learnlib.algorithms.lsharp.NormalObservationTree;
import net.automatalib.automata.transducers.impl.compact.CompactMealy;
import net.automatalib.commons.util.Pair;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;

import java.util.*;

/**
 * State matching
 *
 * @param <I>
 *         input symbol type
 *
 * @author Loes Kruger
 */
abstract class StateMatching<I, O>{
    public ArrayList<int[]> matchings;
    public Set<Word<I>> prevMatchTree;
    public ArrayList<Pair<Word<I>, Integer>> unmatched;
    public CompactMealy<I,O> combinedModel;
    public Alphabet<I> inputAlphabet;

    public StateMatching(Alphabet<I> inputAlphabet, CompactMealy<I,O> combinedModel) {
        this.unmatched = new ArrayList<>();
        this.matchings = new ArrayList();
        this.prevMatchTree = new HashSet<>();
        this.combinedModel = combinedModel;
        this.inputAlphabet = inputAlphabet;
    }

    public abstract void initializeStateMatching(List<Word<I>> basis);

    public Word<I> longestCommonPref(Set<Word<I>> prevTree, Word<I> w) {
        Word<I> LCP = Word.epsilon();
        for(Word<I> seq : prevTree) {
            if(seq.longestCommonPrefix(w).length() > LCP.length()) {
                LCP = seq.longestCommonPrefix(w);
            }
        }
        return LCP;
    }

    public abstract List<Integer> getBestMatch(int i);

    public abstract void printMatchTable(List<Word<I>> basis);

    protected abstract void updateMatching(Set<Word<I>> tree, List<Word<I>> basis, NormalObservationTree<I,O> obsTree);

    public abstract void scorePart2(Word<I> part2, LSState targetState, int referenceState, int basisIndex, int referenceIndex, NormalObservationTree<I,O> obsTree);

    public boolean checkPart1(Word<I> part1, int referenceState) {
        for(I sym : part1) {
            O referenceOutput = this.combinedModel.getOutput(referenceState, sym);
            if(referenceOutput == null) {
                return false;
            }
            referenceState =  this.combinedModel.getSuccessor(referenceState, sym);
        }
        return true;
    }

    public abstract List<Integer> argmax(int idx);

    public abstract void updateMatchingForNewBasis(Word<I> bs, List<Word<I>> basis, NormalObservationTree<I,O> obsTree);
}
