package de.learnlib.algorithms.continuous.base;

public class LearningFinishedException extends IllegalArgumentException {
    public LearningFinishedException() {
        super();
    }
}
