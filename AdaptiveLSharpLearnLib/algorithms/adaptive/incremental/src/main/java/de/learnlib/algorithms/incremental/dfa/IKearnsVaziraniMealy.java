/* Copyright (C) 2013-2021 TU Dortmund
 * This file is part of LearnLib, http://www.learnlib.de/.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.algorithms.incremental.dfa;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.checkerframework.checker.nullness.qual.Nullable;

import com.github.misberner.buildergen.annotations.GenerateBuilder;
import de.learnlib.acex.AcexAnalyzer;
import de.learnlib.algorithms.kv.StateInfo;
import de.learnlib.algorithms.kv.mealy.KearnsVaziraniMealy;
import de.learnlib.algorithms.kv.mealy.KearnsVaziraniMealyState;
import de.learnlib.api.oracle.MembershipOracle;
import de.learnlib.api.query.DefaultQuery;
import de.learnlib.datastructure.discriminationtree.iterators.DiscriminationTreeIterators;
import de.learnlib.datastructure.discriminationtree.model.AbstractWordBasedDTNode;
import de.learnlib.datastructure.discriminationtree.model.LCAInfo;
import de.learnlib.util.mealy.MealyUtil;
import net.automatalib.automata.base.compact.CompactTransition;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.automata.transducers.impl.compact.CompactMealy;
import net.automatalib.commons.smartcollections.ArrayStorage;
import net.automatalib.commons.util.Pair;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;

/**
 * The Mealy machine version of IKeansVaziraniDFA
 * Combination of IKeansVaziraniDFA and KearnsVaziraniMealy (the active algorithm)
 *
 * @param <I>
 *         input symbol type
 *
 * @author Loes Kruger
 */
public class IKearnsVaziraniMealy<I,O> extends KearnsVaziraniMealy<I,O> {

    public int rebuildingStates;

    /**
     * Constructor.
     *
     * @param alphabet
     *         the learning alphabet
     * @param oracle
     *         the membership oracle
     */
    @GenerateBuilder
    public IKearnsVaziraniMealy(Alphabet<I> alphabet,
                                MembershipOracle<I, Word<O>> oracle,
                                AcexAnalyzer counterexampleAnalyzer,
                                KearnsVaziraniMealyState<I, O> startingState) {
        super(alphabet, oracle, true, counterexampleAnalyzer);
        super.discriminationTree = startingState.getDiscriminationTree();
        super.discriminationTree.setOracle(oracle);
        super.hypothesis = startingState.getHypothesis();
        super.stateInfos = startingState.getStateInfos();
        this.rebuildingStates = 0;
    }

    @Override
    public void startLearning() {
        if (discriminationTree.getNodes().size() == 1) {
            super.startLearning();
        } else {
            initialize();
        }
    }

    private void initialize() {
        // Minimising the tree at the start allows us the make the tree smaller, limiting sift depth.
        minimiseTree();
        DiscriminationTreeIterators.leafIterator(discriminationTree.getRoot()).forEachRemaining(l -> {
            assert l.getData().dtNode.equals(l);
            assert stateInfos.contains(l.getData()); //TODO
        });
    }

    @Override
    public boolean refineHypothesis(DefaultQuery<I, Word<O>> ceQuery) {
        if (hypothesis.size() == 0) {
            throw new IllegalStateException("Not initialized");
        }
        Word<I> input = ceQuery.getInput();
        Word<O> output = ceQuery.getOutput();
        if (!refineHypothesisSingle(input, output)) {
            return false;
        }
        if (repeatedCounterexampleEvaluation) {
            while (refineHypothesisSingle(input, output)) {}
        }
        return true;
    }

    protected boolean refineHypothesisSingle(Word<I> input, Word<O> output) {
        int inputLen = input.length();

        int mismatchIdx = MealyUtil.findMismatch(hypothesis, input, output);
        if (mismatchIdx == MealyUtil.NO_MISMATCH) {
            return false;
        }

        if (inputLen < 2) {
            StateInfo<I, Word<O>> startState = sift(Collections.singletonList(Word.epsilon()), hypothesis).get(0);
            StateInfo<I, Word<O>> newDest = sift(Collections.singletonList(input), hypothesis).get(0);
            newDest.addIncoming(sift(Collections.singletonList(Word.epsilon()), hypothesis).get(0), input.getSymbol(0));
            stateInfos.get(hypothesis.getTransition(startState.id, input.getSymbol(0)).getSuccId()).removeIncoming(startState, input.getSymbol(0));

            hypothesis.removeAllTransitions(startState.id, input.getSymbol(0));
            hypothesis.addTransition(startState.id, input.getSymbol(0), new CompactTransition<O>(newDest.id, hypothesis.getTransition(startState.id, input.getSymbol(0)).getProperty()));
            assert hypothesis.computeOutput(input) == output;
            return true;
        }

        Word<I> effInput = input.prefix(mismatchIdx + 1);
        Word<O> effOutput = output.prefix(mismatchIdx + 1);

        KVAbstractCounterexample acex = new KVAbstractCounterexample(effInput, effOutput, oracle);
        int idx = ceAnalyzer.analyzeAbstractCounterexample(acex, 0);

        Word<I> prefix = effInput.prefix(idx);
        StateInfo<I, Word<O>> srcStateInfo = acex.getStateInfo(idx);
        I sym = effInput.getSymbol(idx);
        LCAInfo<Word<O>, @Nullable AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>>> lca =
                acex.getLCA(idx + 1);
        assert lca != null;

        splitState(srcStateInfo, prefix, sym, lca);

        return true;
    }

    private void minimiseTree() {
        boolean hasRemovedLeaf = true;
        while (hasRemovedLeaf) {
            hasRemovedLeaf = false;
            Iterator<AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>>> nodeIt =
                    DiscriminationTreeIterators.leafIterator(discriminationTree.getRoot());

            while (nodeIt.hasNext()) {
                AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>> currentNode = nodeIt.next();
                if (currentNode.getData() != null) {
                    AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>> siftedNode =
                            discriminationTree.sift(currentNode.getData().accessSequence);
                    if (!currentNode.getData().dtNode.equals(siftedNode)) {
                        hasRemovedLeaf = true;
                        if (siftedNode.getData() == null) {
                            siftedNode.setData(currentNode.getData());
                            siftedNode.getData().dtNode = siftedNode;
                            currentNode.setData(null);
                        } else {
                            if (currentNode.getData().accessSequence.getClass() == Word.epsilon().getClass()) {
                                siftedNode.getData().accessSequence = Word.epsilon();
                            }
                            removeLeaf(currentNode);
                        }
                        break;
                    }
                }
            }
        }
        rebuildHypothesis();
    }

    private void removeLeaf(AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>> leaf) {
        AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>> parentNode = leaf.getParent();

        // if 2 children left, no problem, just remove child
        if(parentNode.getChildren().size() > 2) {
            parentNode.getChildren().remove(leaf);
            leaf.setData(null);
        } else {
            //if parent has 2 children
            Collection<AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>>> children = parentNode.getChildren();
            children.remove(leaf);
            AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>> siblingNode = children.iterator().next();

            //if not root node
            if (parentNode.getDiscriminator().getClass() != Word.epsilon().getClass()) {
                // replace parent by sibling node
                parentNode.replaceChildren(siblingNode.getChildMap());
                // if parent node not a leaf, set the parent of the children
                if (!parentNode.isLeaf()) {
                    for(AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>> child : parentNode.getChildren()) {
                        child.setParent(parentNode);
                    }
                }

                parentNode.setDiscriminator(siblingNode.getDiscriminator());
                if (siblingNode.isLeaf()) {
                    parentNode.setData(siblingNode.getData());
                } else {
                    parentNode.clearData();
                }
                if (parentNode.isLeaf() && parentNode.getData() != null) {
                    parentNode.getData().dtNode = parentNode;
                }
                reduceDepth(siblingNode);
            } else {
                leaf.setData(null);
            }
        }
    }

    private void reduceDepth(AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>> leaf) {
        Iterator<AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>>> nodeIt =
                DiscriminationTreeIterators.nodeIterator(leaf);
        while (nodeIt.hasNext()) {
            AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>> node = nodeIt.next();
            node.setDepth(node.getDepth() - 1);
        }
    }

    private void rebuildHypothesis() {
        stateInfos.clear();

        CompactMealy<I,O> newhyp = new CompactMealy<>(alphabet, 20);
        // iterate over all children
        Iterator<AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>>> nodeIt =
                DiscriminationTreeIterators.leafIterator(discriminationTree.getRoot());

        while (nodeIt.hasNext()) {
            AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>> currentNode = nodeIt.next();
            if(currentNode.getData() != null) {
                currentNode.getData().id = newhyp.addIntState();

                stateInfos.add(currentNode.getData().id, currentNode.getData());
                this.rebuildingStates += 1;
                if (currentNode.getData().accessSequence.getClass() == Word.epsilon().getClass()) {
                    newhyp.setInitialState(currentNode.getData().id);
                }
            }
        }

        // naively resifting everything to rebuild the hypothesis
        // this could be further optimized
        for (int i = 0; i < stateInfos.size(); i++) {
            StateInfo<I, Word<O>> newState = stateInfos.get(i);
            for (I sym : alphabet) {
                Word<I> transAS = newState.accessSequence.append(sym);
                StateInfo<I, Word<O>> newTransState = sift(Collections.singletonList(transAS), newhyp).get(0);
                if(!stateInfos.contains(newTransState)){
                    stateInfos.add(newTransState);
                }
                Word<I> query = newState.accessSequence.append(sym);
                setTransition(newState, sym, newTransState, oracle.answerQuery(query).lastSymbol(), newhyp);
            }
        }

        hypothesis = newhyp;

        for (StateInfo<I, Word<O>> state : stateInfos) {
            state.clearIncoming();
        }

        for (StateInfo<I, Word<O>> state : stateInfos) {
            for (I sym : alphabet) {
                stateInfos.get(hypothesis.getTransition(state.id, sym).getSuccId()).addIncoming(state, sym);
            }
        }
    }


    protected void splitState(StateInfo<I, Word<O>> stateInfo,
                              Word<I> newPrefix,
                              I sym,
                              LCAInfo<Word<O>, @Nullable AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>>> separatorInfo) {
        int state = stateInfo.id;

        Set<Pair<StateInfo<I, Word<O>>, I>> oldIncoming = stateInfo.fetchIncoming();

        StateInfo<I, Word<O>> newStateInfo = createState(newPrefix, hypothesis);

        AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>> stateLeaf = stateInfo.dtNode;

        AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>> separator = separatorInfo.leastCommonAncestor;
        Word<I> newDiscriminator;
        Word<O> oldOut, newOut;
        if (separator == null) {
            newDiscriminator = Word.fromLetter(sym);
            oldOut = separatorInfo.subtree1Label;
            newOut = separatorInfo.subtree2Label;
        } else {
            newDiscriminator = newDiscriminator(sym, separator.getDiscriminator());
            CompactTransition<O> transition = hypothesis.getTransition(state, sym);
            assert transition != null;
            O transOut = hypothesis.getTransitionOutput(transition);
            oldOut = newOutcome(transOut, separatorInfo.subtree1Label);
            newOut = newOutcome(transOut, separatorInfo.subtree2Label);
        }

        final AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>>.SplitResult sr =
                stateLeaf.split(newDiscriminator, oldOut, newOut, newStateInfo);

        stateInfo.dtNode = sr.nodeOld;
        newStateInfo.dtNode = sr.nodeNew;

        initState(newStateInfo, hypothesis);
        updateTransitions(oldIncoming, stateLeaf);
    }

    private Word<O> newOutcome(O transOutput, Word<O> succOutcome) {
        return succOutcome.prepend(transOutput);
    }

    private void updateTransitions(Set<Pair<StateInfo<I, Word<O>>, I>> transSet,
                                   AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>> oldDtTarget) {
        List<Pair<StateInfo<I, Word<O>>, I>> trans = new ArrayList<>(transSet);
        final List<Word<I>> transAs = trans.stream()
                .map(t -> t.getFirst().accessSequence.append(t.getSecond()))
                .collect(Collectors.toList());

        final List<StateInfo<I, Word<O>>> succs = sift(Collections.nCopies(trans.size(), oldDtTarget), transAs, hypothesis);


        for (int i = 0; i < trans.size(); i++) {
            Pair<StateInfo<I, Word<O>>, I> t = trans.get(i);

            CompactTransition<O> transition = hypothesis.getTransition(t.getFirst().id, t.getSecond());
            assert transition != null;
            setTransition(t.getFirst(), t.getSecond(), succs.get(i), transition.getProperty(), hypothesis);
        }
    }

    private Word<I> newDiscriminator(I symbol, Word<I> succDiscriminator) {
        return succDiscriminator.prepend(symbol);
    }

    private StateInfo<I, Word<O>> createState(Word<I> prefix, CompactMealy<I,O> currentHyp) {
        int state = currentHyp.addIntState();

        StateInfo<I, Word<O>> stateInfo = new StateInfo<>(state, prefix);
        stateInfos.add(stateInfo);

        return stateInfo;
    }

    private void initState(StateInfo<I, Word<O>> stateInfo, CompactMealy<I,O> currentHyp) {
        int alphabetSize = alphabet.size();

        Word<I> accessSequence = stateInfo.accessSequence;

        final List<Word<I>> transAs = new ArrayList<>(alphabetSize);
        final List<DefaultQuery<I, Word<O>>> outputQueries = new ArrayList<>(alphabetSize);

        for (int i = 0; i < alphabetSize; i++) {
            I sym = alphabet.getSymbol(i);
            transAs.add(accessSequence.append(sym));
            outputQueries.add(new DefaultQuery<>(accessSequence, Word.fromLetter(sym)));
        }

        final List<StateInfo<I, Word<O>>> succs = sift(transAs, currentHyp);
        this.oracle.processQueries(outputQueries);

        for (int i = 0; i < alphabetSize; i++) {
            setTransition(stateInfo, alphabet.getSymbol(i), succs.get(i), outputQueries.get(i).getOutput().firstSymbol(), currentHyp);
        }
    }

    private void setTransition(StateInfo<I, Word<O>> state, I symbol, StateInfo<I, Word<O>> succInfo, O output, CompactMealy<I,O> currentHyp) {
        succInfo.addIncoming(state, symbol);
        currentHyp.setTransition(state.id, alphabet.getSymbolIndex(symbol), succInfo.id, output);
    }

    private List<StateInfo<I, Word<O>>> sift(List<Word<I>> prefixes, CompactMealy<I,O> currenthyp) {
        return sift(Collections.nCopies(prefixes.size(), discriminationTree.getRoot()), prefixes, currenthyp);
    }

    private List<StateInfo<I, Word<O>>> sift(List<AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>>> starts,
                                             List<Word<I>> prefixes, CompactMealy<I,O> currenthyp) {

        final List<AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>>> leaves =
                discriminationTree.sift(starts, prefixes);
        final List<StateInfo<I, Word<O>>> result = new ArrayList<>(leaves.size());

        for (int i = 0; i < leaves.size(); i++) {
            final AbstractWordBasedDTNode<I, Word<O>, StateInfo<I, Word<O>>> leaf = leaves.get(i);

            StateInfo<I, Word<O>> succStateInfo = leaf.getData();
            if (succStateInfo == null) {
                // Special case: this is the *first* state of a different
                // acceptance than the initial state
                succStateInfo = createState(prefixes.get(i), currenthyp);
                leaf.setData(succStateInfo);
                succStateInfo.dtNode = leaf;

                initState(succStateInfo, currenthyp);
            }

            result.add(succStateInfo);
        }

        return result;
    }
}
