FROM ubuntu:22.04

# Update default packages
RUN apt-get update

# Get Ubuntu packages
RUN apt-get install -y \
    build-essential \
    curl \
    maven \
    unzip

# Update new packages
RUN apt-get update

# Get openjdk
RUN apt-get install openjdk-17-jdk -y

# Get minimal texlive for pictures
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get install texlive-pictures -y

RUN apt-get install nano
RUN apt-get install pip -y
RUN pip install pandas
RUN pip install Jinja2

WORKDIR /artifact
COPY . .
ENTRYPOINT ["bash"]