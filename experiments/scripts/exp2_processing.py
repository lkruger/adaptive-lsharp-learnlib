import os
import sys
import pandas as pd


def getTarget(row):
    mystr = row['sul']
    newstr = mystr.split("_")[-1].split(".dot")[0]
    if "full" in newstr:
        newstr = newstr.split("-full")[0].replace("learnresult","model")
    return newstr

def getRef(row):
    ref = row['refs']
    newstr = mystr.split("_")[-2].split(".dot")[0]
    if "full" in newstr:
        newstr = newstr.split("-full")[0].replace("learnresult","model")
    return newstr

def getPair(row):
    namemapper = {"097c":"097", "097e":"097c", "098f":"097e", "098l":"098f", "098m":"098l", "100":"098m", "100f":"100", "100h":"100f", "101":"100h", "101h":"100", "101k":"101h", "102":"101k", "110pre1":"102", "100m":"100h", "098s":"098m", "098u":"098s", "098za":"098u", "model2":"model1", "model3":"model2", "model4":"model3", "model5":"model4", "model6":"model5"}
    sul = row['sul'].split("_")[-1].split(".dot")[0]
    ref = row['refs'].split("_")[-2].split(".dot")[0]
    if "full" in sul:
        sul = sul.split("-full")[0].replace("learnresult","model")
        ref = ref.split("-full")[0].replace("learnresult","model")
    if ref == "":
        ref = namemapper[sul]
    return sul+"-"+ref

def makePlot(df,algset,benchmark):
    vsdf = df[df["algorithm"].isin(algset)]
    vsdf = df[df["target"].isin(benchmark)]
    pairs = vsdf["pair"].unique()
    targets = vsdf["target"].unique()
    print(targets)
    algorithm_renaming = {"lstar":"$L^*$", "kv":"KV", "lsharp":"\lsharp{}", "pdlstar":"\pdlstar~", "adaptivekv":"IKV", "adaptivelsharp_R":"\lsharprebuild", "adaptivelsharp_M":"\lsharpmatch", "adaptivelsharp_AM":"\lsharpapproxmatch", "adaptivelsharp_R_M":"\lsharprebuildmatch","adaptivelsharp_R_AM":"\\adaptivelsharp"}
    vsgroupeddf = vsdf.groupby(["algorithm", "target"])["total"].mean()
    vsgroupeddf = vsgroupeddf.round(0)

    vsgroupeddfmax = vsdf.groupby(["algorithm", "target"])["total"].quantile(0.95)
    vsgroupeddfmax = vsgroupeddfmax.round(0)

    vsgroupeddfmin = vsdf.groupby(["algorithm", "target"])["total"].quantile(0.05)
    vsgroupeddfmin = vsgroupeddfmin.round(0)

    lowest = min(vsdf["total"])/2
    highest = 200000
    xtickslabel = ""
    xtick = ""

    for i in range(0,len(targets)):
        xtick += str(i) + ", "
        xtickslabel += targets[i] + ", "
    xtick = xtick[:-2]
    xtickslabel = xtickslabel[:-2]
    mywidth = "8" 

    with open('./experiments/results/plots/exp2.tex', 'w') as pgftex:
        pgftex.write("\\begin{figure}[h]\n\\resizebox{.99\\textwidth}{!}{")
        mys = ('\\begin{tikzpicture}\n'
                 '\\begin{axis}[\n'
                 'legend style={nodes={scale=0.5, transform shape},anchor=north east},\n'
                 'width='+mywidth+'cm,height=4cm,\n'
                 'ymode=log,\n'
                 'ylabel=\\tiny{Total Symbols},\n'
                 'xtick={'+xtick+'},\n'
                 'xticklabels={'+xtickslabel+'},\n'
                 'x tick label style={rotate=70,anchor=east},\n'
                 'xtick pos=bottom,\n'
                 'ytick pos=left,\n'
                 'ylabel shift = -0.1cm,\n'
                 'xmin=-1,xmax='+str(len(targets)-0.2)+',\n'
                 'ymin='+str(lowest)+
                 ',\nymax='+str(highest)+
                 ',\nlegend pos=north east,'
                 ']\n\n')
        pgftex.write(mys)

        mycolors = ["cyan", "red", "green", "black", "teal", "purple", "pink", "orange", "yellow", "blue"]
        for j in range(0,len(algset)):
            coordstr = ""
            offset = 0.12*j - 0.24
            for i in range(0,len(targets)):
                coordstr += "("+str(i+offset)+","+str(vsgroupeddf[algset[j]][targets[i]])+") += (0," + str(vsgroupeddfmax[algset[j]][targets[i]]-vsgroupeddf[algset[j]][targets[i]]) + ") -= (0," + str(vsgroupeddf[algset[j]][targets[i]]-vsgroupeddfmin[algset[j]][targets[i]]) + ")\n"
            pgftex.write("\\addplot+ [\n" + \
                         "only marks,\n" + \
                         "color="+mycolors[j]+",\n" + \
                         "mark=o,\n" + \
                         "mark size=1pt,\n" + \
                         "error bars/.cd," + \
                         "x dir=both, x explicit," + \
                         "y dir=both, y explicit," + \
                         "]\n" + \
                         "coordinates {\n" + \
                         coordstr + \
                         "};\n" + \
                         "\\addlegendentry{"+algorithm_renaming[algset[j]]+"}\n\n\n")

        pgftex.write("\\end{axis}\n" + \
                    "\\end{tikzpicture}}\n" + \
                    "\\caption*{Fig. 4a: Averaged inputs for learning \\textit{Adaptive-Philips} and \\textit{Adaptive-OpenSSL}," + \
                    " the \\textit{Adaptive-Philips} models start with \\textit{m}.}\end{figure}")


if __name__ in ["__main__"]:
    df = pd.read_csv("./experiments/results/experiment2/results_exp2.csv", sep=",")
    df = df.sort_values(by=["sul", "algorithm"], ignore_index=True)
    df["target"] = df.apply(getTarget, axis=1)
    df["pair"] = df.apply(getPair, axis=1)
    df["finished"] = 1
    df["total"] = df["mq_sym"] + df["eq_sym"]

    combined = ["097c", "097e", "098f", "098l", "098m", "100", "100f", "100h", "101", "101h", "101k", "102", "110pre1", "100m", "098s", "098u", "098za","model2", "model3", "model4", "model5", "model6"]
    makePlot(df,["lsharp", "pdlstar", "adaptivekv", "adaptivelsharp_R_AM"], combined)

