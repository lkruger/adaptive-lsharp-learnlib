import os
import sys
import pandas as pd


def getTarget(row):
    mystr = row['sul']
    return "_".join(mystr.split("_")[1:-1])

def getMut(row):
    mystr = row['sul']
    return mystr.split("_")[0]

def getSeed(row):
    mystr = row['sul']
    return mystr.split("_")[-1].split(".")[0]

def checkTimeouts(df):
    timeouts = df.loc[df["isEquiv"] == "timeout"]
    timeouts.to_csv('./timeoutchecker.csv', index=False)

def makeTable(cur_df):
    group_cur = cur_df.groupby(["algorithm", "mutation"])["total"].sum() / 1000000

    algorithms = ["lstar", "kv", "lsharp", "pdlstar", "adaptivekv", "adaptivelsharp_R_AM", "adaptivelsharp_R", "adaptivelsharp_M", "adaptivelsharp_AM", "adaptivelsharp_R_M"]
    algorithmsnames = ["$L^*$", "KV", "\lsharp", "\pdlstar", "IKV", "\\adaptivelsharp{} (\\textcolor{red!60!black}{new!})", "\lsharprebuild{} (\\textcolor{red!60!black}{new!})",  "\lsharpmatch{} (\\textcolor{red!60!black}{new!})", "\lsharpapproxmatch{} (\\textcolor{red!60!black}{new!})", "\lsharprebuildmatch{} (\\textcolor{red!60!black}{new!})"]
    mutations = ["1dummy", "2init", "3addstate", "4remstate", "5diverttrans", "6changeout", "7remsym", "8concatA", "9concatB", "10mutate4", "11mutate5", "12changetrans", "13manymut", "14union"]
    mutationsnames = ["$M_1$", "$M_2$", "$M_3$", "$M_4$", "$M_5$", "$M_6$", "$M_7$", "$M_8$", "$M_9$", "$M_{10}$", "$M_{11}$", "$M_{12}$", "$M_{13}$", "$M_{14}$"]
    cols = [*["Algorithm"], *mutationsnames]

    mydf = pd.DataFrame(columns=cols)
    for i in range(1,len(algorithms)+1):
        mydf.at[i,'Algorithm'] = algorithmsnames[i-1]
        for j in range(0, len(mutations)):
            if ((algorithms[i-1]=="adaptivekv") or (algorithms[i-1]=="pdlstar")) and (mutations[j]=="7remsym"):
                mydf.at[i,mutationsnames[j]] = None
            else:
                mydf.at[i,mutationsnames[j]] = group_cur[algorithms[i-1]][mutations[j]]


    data = mydf.style.hide(axis="index").format(precision=3).to_latex(column_format="lp{1.2cm}p{1.2cm}p{1.2cm}p{1.2cm}p{1.2cm}p{1.2cm}p{1.2cm}p{1.2cm}p{1.2cm}p{1.2cm}p{1.2cm}p{1.2cm}p{1.2cm}p{1.2cm}",hrules=True)
    data = data.replace("None", "-")
    data = data.replace("\\pdlstar", "\midrule \\pdlstar")
    data = data.replace("\\adaptivelsharp{}", "\midrule \\adaptivelsharp{}")
    data = data.replace("\lsharprebuild{}", "\midrule \lsharprebuild{}")
    data = data.replace("\colorteal", "\\textcolor{teal}")
    with open('./experiments/results/plots/exp1.tex', 'w') as tabletex:
        tabletex.write("\\renewcommand{\\arraystretch}{1.4}\n")
        tabletex.write("\\begin{table}[t]\n\\centering\n\\resizebox{.99\\textwidth}{!}{\n")
        tabletex.write(data)
        tabletex.write("}\n\\caption*{Table 2: Summed inputs in millions for learning the mutated models with the original models.}\n\\label{tab:my_exp1}\n\\end{table}\n")

def compare2(df,alg1,alg2):
    df = df.loc[df["isEquiv"] == True]
    vsdf = df[(df["algorithm"] == alg1) | (df["algorithm"] == alg2)]
    suls = vsdf["target"].unique()
    sulsnames = {'DropBear':'DropBear', 'GnuTLS_3.3.8_client_full':'GnuTLS', 'NSS_3.17.4_server_regular':'NSS', 'learnresult_fix':'learnresult-fix', 'OpenSSH':'OpenSSH', 'model1':'model1'}
    algorithm_renaming = {"lstar":"$L^*$", "kv":"KV", "lsharp":"\lsharp{}", "pdlstar":"\pdlstar{}", "adaptivekv":"IKV", "adaptivelsharp_R":"\lsharprebuild{}", "adaptivelsharp_RM":"\lsharprebuildmatch{}", "adaptivelsharp_M":"\lsharpmatch{}", "adaptivelsharp_AM":"\lsharpapproxmatch{}", "adaptivelsharp_R_AM":"\\adaptivelsharp{}"}
    muts = vsdf["mutation"].unique()
    vsgroupeddf = vsdf.groupby(["algorithm", "target", "mutation"])["total"].mean()
    vsgroupeddf = vsgroupeddf.round(0)

    lowest = min(vsdf["total"])/2
    highest = max(vsdf["total"])*2
    lowest10 = min(vsdf["total"])/10
    highest10 = max(vsdf["total"])*10
    coordstr = ""
    alg1name = algorithm_renaming[alg1] if alg1 in algorithm_renaming else alg1
    alg2name = algorithm_renaming[alg2] if alg2 in algorithm_renaming else alg2
    with open('./experiments/results/plots/exp1_'+alg1+"_vs_"+alg2+'.tex', 'w') as pgftex:
        mys = ('\\begin{tikzpicture}\n'
                 '\\begin{axis}[\n'
                 'legend style={nodes={scale=0.3, transform shape},anchor=north west},\n'
                 'width=4cm,height=4cm,\n'
                 'xmode=log,\n'
                 'ymode=log,\n'
                 'xlabel=\\tiny{\\vphantom{\lsharpapproxmatch{} \lsharprebuildmatch{} IKV}Total Symbols '+str(alg1name)+'},\n'
                 'ylabel=\\tiny{\\vphantom{\lsharpapproxmatch{} \lsharprebuildmatch{} IKV}Total Symbols '+str(alg2name)+'},\n'
                 'xtick pos=bottom,\n'
                 'ytick pos=left,\n'
                 'ylabel shift = -0.1cm,\n'
                 'xmin='+str(lowest)+
                 ',\nxmax=57518574'+
                 ',\nymin='+str(lowest)+
                 ',\nymax=57518574'+
                 ',\nlegend pos=north west,'
                 ']\n\n')
        pgftex.write(mys)
        for i in range(0,len(suls)):
            coordstr = ""
            for j in range(0,len(muts)):
                if not (((alg1=="adaptivekv") or (alg1=="pdlstar") or (alg2=="adaptivekv") or (alg2=="pdlstar")) and (muts[j]=="7remsym")):
                    coordstr += "("+str(vsgroupeddf[alg1][suls[i]][muts[j]])+","+str(vsgroupeddf[alg2][suls[i]][muts[j]])+")"
            mycolors = {'DropBear':"cyan", 'GnuTLS_3.3.8_client_full':"red", 'NSS_3.17.4_server_regular':"green", 'OpenSSH':"violet", 'learnresult_fix':"teal", 'model1':"purple"}
            pgftex.write("\\addplot[\n" + \
                         "only marks,\n" + \
                         "color="+mycolors[suls[i]]+",\n" + \
                         "mark=o,\n" + \
                         "mark size=1pt\n" + \
                         "]\n" + \
                         "coordinates {\n" + \
                         coordstr + \
                         "};\n" + \
                         "\\addlegendentry{"+sulsnames[suls[i]]+"}\n\n\n")

        pgftex.write("\\addplot[color=gray]\n" + \
                            "coordinates {("+str(lowest10)+","+str(lowest10)+")("+str(highest10)+","+str(highest10)+") };\n" + \
                            "\\addplot[color=gray,style=densely dotted]\n" + \
                            "coordinates {("+str(lowest10)+","+str(lowest10/10)+")("+str(highest10)+","+str(highest10/10)+") };\n" + \
                            "\\addplot[color=gray,style=densely dotted]\n" + \
                            "coordinates {("+str(lowest10)+","+str(lowest10*10)+")("+str(highest10)+","+str(highest10*10)+") };\n\n")
        pgftex.write("\\addplot[color=gray]\n" + \
                    "coordinates {("+str(lowest)+","+str(lowest)+")("+str(highest)+","+str(highest)+") };\n" + \
                    "\\addplot[color=gray,style=dashed]\n" + \
                    "coordinates {("+str(lowest)+","+str(lowest/2)+")("+str(highest)+","+str(highest/2)+") };\n" + \
                    "\\addplot[color=gray,style=dashed]\n" + \
                    "coordinates {("+str(lowest)+","+str(lowest*2)+")("+str(highest)+","+str(highest*2)+") };\n" + \
                    "\\end{axis}\n" + \
                    "\\end{tikzpicture}\n")





if __name__ in ["__main__"]:
    #general df stuff
    df = pd.read_csv("./experiments/results/experiment1/results_exp1.csv", sep=",")
    df = df.sort_values(by=["sul", "algorithm"], ignore_index=True)
    df["target"] = df.apply(getTarget, axis=1)
    df["mutation"] = df.apply(getMut, axis=1)
    df["seed"] = df.apply(getSeed, axis=1)
    df["finished"] = 1
    df["total"] = df["mq_sym"] + df["eq_sym"]

    makeTable(df)
    compare2(df,"lsharp","adaptivelsharp_R_AM") 
    compare2(df,"adaptivelsharp_R","adaptivelsharp_R_AM") 
    compare2(df,"adaptivelsharp_AM","adaptivelsharp_R_AM")
    compare2(df,"adaptivelsharp_R","adaptivelsharp_AM") 
    compare2(df,"adaptivekv","adaptivelsharp_R_AM") 
    compare2(df,"adaptivelsharp_M","adaptivelsharp_AM") 

