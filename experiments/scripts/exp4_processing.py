import os
import sys
import pandas as pd

mapping = {"scandiumlatestecdhecertreq.dot":"scand-lat",
                "scandium-2.0.0ecdhecertreq.dot":"scand-2",
                "ctinydtlsecdhecertreq.dot":"ctiny",
                "wolfssl-4.0.0dheecdhersacertreq.dot":"wolfssl",
                "gnutls-3.6.7allcertreq.dot":"gnutls",
                "mbedtlsallcertreq.dot":"mbedtls",
                "etinydtlsecdhecertreq.dot":"etiny",
                "TCPWindows8Client.dot":"windows",
                "TCPLinuxClient.dot":"linux",
                "TCPFreeBSDClient.dot":"freeBSD",
                "TCPWindows8Server.dot":"Windows8-server",
                "TCPLinuxServer.dot":"Linux-server",
                "TCPFreeBSDServer.dot":"FreeBSD-server",
                }

def getTarget(row):
    mystr = row['sul']
    return mapping[mystr.replace("_","")]

def getRefs(row):
    ref = row['refs']
    if ref.count(".dot") == 0:
        return "-"
    if ref.count(".dot") > 1:
        return "all"
    else:
        return mapping[ref.replace("_","")]

def getPair(row):
    namemapper = {"097c":"097", "097e":"097c", "098f":"097e", "098l":"098f", "098m":"098l", "100":"098m", "100f":"100", "100h":"100f", "101":"100h", "101h":"100", "101k":"101h", "102":"101k", "110pre1":"102", "100m":"100h", "098s":"098m", "098u":"098s", "098za":"098u", "model2":"model1", "model3":"model2", "model4":"model3", "model5":"model4", "model6":"model5"}
    sul = row['sul'].split("_")[-1].split(".dot")[0]
    ref = row['refs'].split("_")[-2].split(".dot")[0]
    if "full" in sul:
        sul = sul.split("-full")[0].replace("learnresult","model")
        ref = ref.split("-full")[0].replace("learnresult","model")
    if ref == "":
        ref = namemapper[sul]
    return sul+"-"+ref



def makePlot(df,algset,benchmark,benchmarkname):
    vsdf = df[df["target"].isin(benchmark)]
    myrefs = vsdf["myref"].unique()
    myrefs = list(myrefs)
    myrefs.remove("all")
    myrefs.append("all")
    targets = vsdf["target"].unique()
    algorithm_renaming = {"lstar":"$L^*$", "kv":"KV", "lsharp":"\lsharp{}", "pdlstar":"\pdlstar~", "adaptivekv":"IKV", "adaptivelsharp_R":"\lsharprebuild", "adaptivelsharp_M":"\lsharpmatch", "adaptivelsharp_AM":"\lsharpapproxmatch", "adaptivelsharp_R_M":"\lsharprebuildmatch","adaptivelsharp_R_AM":"\\adaptivelsharp"}
    vsgroupeddf = vsdf.groupby(["myref", "target"])["total"].mean()
    vsgroupeddf = vsgroupeddf.round(0)

    vsgroupeddfmax = vsdf.groupby(["myref", "target"])["total"].max()
    vsgroupeddfmax = vsgroupeddfmax.round(0)

    vsgroupeddfmin = vsdf.groupby(["myref", "target"])["total"].min()
    vsgroupeddfmin = vsgroupeddfmin.round(0)

    lowest = min(vsdf["total"])/1.5 if benchmarkname=="TLS" else 3000
    highest = max(vsdf["total"])*0.8 if benchmarkname=="TLS" else 120000
    xtickslabel = ""
    xtick = ""

    for i in range(0,len(targets)):
        xtick += str(i) + ", "
        xtickslabel += targets[i] + ", "
    xtick = xtick[:-2]
    xtickslabel = xtickslabel[:-2]
    mywidth = "5" if benchmarkname=="TLS" else "3"
    myoffset = "-0.6" if benchmarkname=="TLS" else "-0.6" 
    myoffset2 = 0.6 if benchmarkname=="TLS" else 0.5
    mylegend = str(1+0.1/7) if benchmarkname=="TLS" else str(1+0.1/3)

    with open('./experiments/results/plots/exp4.tex', 'a') as pgftex:
        mys = ('\\begin{tikzpicture}\n'
                 '\\begin{axis}[\n'
                 'xtick style={draw=none},\n'
                 'legend style={nodes={scale=0.5, transform shape},at={('+mylegend+',1)}, anchor=north west},\n'
                 'width='+mywidth+'cm,height=4cm,\n'
                 'ymode=log,\n'
                 'ylabel=\\tiny{Total Symbols},\n'
                 'xtick={'+xtick+'},\n'
                 'xticklabels={'+xtickslabel+'},\n'
                 'x tick label style={rotate=50,anchor=east},\n'
                 'xtick pos=bottom,\n'
                 'ytick pos=left,\n'
                 'ylabel shift = -0.1cm,\n'
                 'xmin='+myoffset+',xmax='+str(len(targets)-myoffset2)+',\n'+
                 'ymin='+str(lowest)+
                 ',\nymax='+str(highest)+
                 ']\n\n')
        pgftex.write(mys)

        mycoord = 0.075 if benchmarkname=="TLS" else 0.1
        myother = 0.3 if benchmarkname=="TLS" else 0.15
        myother2 = 0.45 if benchmarkname=="TLS" else 0.5

        colormapping = {"scand-lat":"pink",
            "scand-2":"green",
            "ctiny":"red",
            "wolfssl":"cyan",
            "gnutls":"teal",
            "mbedtls":"violet",
            "etiny":"purple",
            "windows":"cyan",
            "linux":"red",
            "freeBSD":"green"
                }
        for j in range(0,len(myrefs)):
            if myrefs[j]=="all":
                mcolor = "black"
                mymark = "x"
            else:
                mcolor = colormapping[myrefs[j]]
                mymark = "o"
            coordstr = ""
            for i in range(0,len(targets)): 
                if targets[i] == myrefs[j] and benchmarkname != "TLS":
                    coordstr += "(0,0)\n"
                else:
                    coordstr += "("+str(i+(j*mycoord)-myother)+","+str(vsgroupeddf[myrefs[j]][targets[i]])+")\n" 
            pgftex.write("\\addplot+ [\n" + \
                         "only marks,\n" + \
                         "color="+mcolor+",\n" + \
                         "mark="+mymark+",\n" + \
                         "mark size=1pt,\n" + \
                         "error bars/.cd," + \
                         "x dir=both, x explicit," + \
                         "y dir=both, y explicit," + \
                         "]\n" + \
                         "coordinates {\n" + \
                         coordstr + \
                         "};\n" + \
                         "\\addlegendentry{"+myrefs[j]+"}\n\n\n")

        for i in range(0,len(targets)-1):
            pgftex.write("\\addplot[color=gray,style=dashed]\n" + \
                "coordinates {("+str(i+myother2)+",1)("+str(i+myother2)+","+str(highest)+") };\n")

        pgftex.write("\\end{axis}\n" + \
                    "\\end{tikzpicture}\n")


if __name__ in ["__main__"]:
    #general df stuff
    df = pd.read_csv("./experiments/results/experiment4/results_exp4.csv", sep=",")
    df = df.sort_values(by=["sul", "algorithm"], ignore_index=True)
    df = df.loc[df["algorithm"] == "adaptivelsharp_R_AM"]
    df["target"] = df.apply(getTarget, axis=1)
    df["myref"] = df.apply(getRefs, axis=1)
    df["finished"] = 1
    df["total"] = df["mq_sym"] + df["eq_sym"]

    with open('./experiments/results/plots/exp4.tex', 'w') as pgftex:
        pgftex.write("\\begin{figure}[H]\n\\centering\\begin{subfigure}[b]{0.55\\textwidth}\n\\resizebox{.99\\textwidth}{!}{")

    tls = ["ctiny", "scand-lat", "mbedtls", "etiny", "wolfssl", "scand-2", "gnutls"]
    makePlot(df,["lsharp", "adaptivelsharp_R_AM", "adaptivekv"],tls,"TLS")

    with open('./experiments/results/plots/exp4.tex', 'a') as pgftex:
        pgftex.write("}\\end{subfigure}\n\hfill\n\\begin{subfigure}[b]{0.4\\textwidth}\n\\resizebox{.99\\textwidth}{!}{")

    tcp_client = ["windows", "linux", "freeBSD"]
    makePlot(df,["lsharp", "adaptivelsharp_R_AM", "adaptivekv"],tcp_client,"TCP-client")

    with open('./experiments/results/plots/exp4.tex', 'a') as pgftex:
        pgftex.write("}\\end{subfigure}\n\\caption*{Fig. 5: Averaged inputs for learning $\S$ with multiple references.}\n\\end{figure}")




