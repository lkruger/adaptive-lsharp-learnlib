import os
import sys
import pandas as pd

def getTarget(row):
    mystr = row['sul']
    return "_".join(mystr.split("_")[1:-1])

def getMut(row):
    mystr = row['sul']
    return mystr.split("_")[0]

def getSeed(row):
    mystr = row['sul']
    return mystr.split("_")[-1].split(".")[0]

def checkTimeouts(df):
    timeouts = df.loc[df["isEquiv"] == "timeout"]
    timeouts.to_csv('./timeoutchecker.csv', index=False)

def mycheck(row):
    mut = row['mutation']
    reftype = row['myrefs']
    if (mut == "N1" or mut == "N2") and (reftype == 2 or reftype == 3):
        return False
    if reftype == 123:
        reffies = row['refs']
        reffies = reffies.split(".dot")
        if reffies[0] == reffies[1] and reffies[0] == reffies[2]:
            return False
    return True

def typemodel(row):
    mut = row['mutation']
    reftype = row['myrefs']
    if (mut == "N3" and reftype == 2) or (mut == "N4" and reftype == 2) or (mut == "N5" and reftype == 2):
        return "\{M13\}"
    elif (mut == "N3" and reftype == 1) or (mut == "N4" and reftype == 1) or (mut == "N5" and reftype == 1):
        return "\{SUL\}"
    elif (mut == "N3" and reftype == 12) or (mut == "N4" and reftype == 12) or (mut == "N5" and reftype == 12):
        return "\{SUL,M13\}"
    elif mut == "N1" and reftype != 123:
        return "\{M10\}"
    elif mut == "N1" and reftype == 123:
        return "\{M10,M10,M10\}"
    elif mut == "N2" and reftype != 123:
        return "\{M12\}"
    elif mut == "N2" and reftype == 123:
        return "\{M12,M12,M12\}"

def makeTable1(vsdf):
    myrefs = vsdf["myrefs"].unique()
    mytypemodel = ["\{M10\}", "\{M12\}", "\{M10,M10,M10\}", "\{M12,M12,M12\}"]
    targets = ["N1", "N2"]
    vsdf.drop_duplicates(subset=['sul', 'refs','mutation','myrefs'], inplace=True)

    group_cur = vsdf.groupby(["typemodel", "mutation"])["total"].sum() / 1000000
    print(vsdf.groupby(["typemodel", "mutation"])["total"].sum() / 1000000)

    cols = [*['SUL'], *mytypemodel]

    d1 = str(group_cur["\{M10\}"]["N1"]).format(precision=1)
    d2 = str(group_cur["\{M12\}"]["N2"]).format(precision=1)
    d3 = str(group_cur["\{M10,M10,M10\}"]["N1"]).format(precision=1)
    d4 = str(group_cur["\{M12,M12,M12\}"]["N2"]).format(precision=1)
    data = "$\S$ & " + d1 + " & " + d2 + " & ~" + d3 + " & ~" + d4 + "\\\ \n"

    with open('./experiments/results/plots/exp3_table1.tex', 'w') as tabletex:
        tabletex.write("\\begin{table}\n\centering\\begin{tabular}{lllll}\n\\toprule\n")
        tabletex.write("& & & \{$\\textit{mut}_{10}(\S)$, & \{$\\textit{mut}_{12}(\S)$, \\\ \n& & & ~$\\textit{mut}_{10}(\S)$, & ~$\\textit{mut}_{12}(\S)$, \\\ \nSUL & \{$\\textit{mut}_{10}(\S)$\} & \{$\\textit{mut}_{12}(\S)$\} & ~$\\textit{mut}_{10}(\S)$\} & ~$\\textit{mut}_{12}(\S)$\} \\\ \n\midrule\n")
        tabletex.write(data)
        tabletex.write("\\bottomrule\\\ \n\\end{tabular}\n\\caption*{Fig. 4b: Summed inputs in millions for learning \S.}\n\\label{tab:my_exp4b}\n\\end{table}")

def makeTable2(vsdf):
    myrefs = vsdf["myrefs"].unique()

    mytypemodel = ["\{SUL\}", "\{M13\}", "\{SUL,M13\}"]
    targets = ["N3", "N4", "N5"]
    targetnames = ["SUL+M13", "SUL;M13", "M13;SUL"]
    vsdf.drop_duplicates(subset=['sul', 'refs','mutation','myrefs'], inplace=True)

    group_cur = vsdf.groupby(["typemodel", "mutation"])["total"].sum() / 1000000
    print(vsdf.groupby(["typemodel", "mutation"])["finished"].sum())
    cols = [*['SUL'], *mytypemodel]

    d1 = str(group_cur["\{SUL\}"]["N4"]).format(precision=1)
    d2 = str(group_cur["\{M13\}"]["N4"]).format(precision=1)
    d3 = str(group_cur["\{SUL,M13\}"]["N4"]).format(precision=1)
    data1 = "$\\textit{mut}_{8}(\S)$ & " + d1 + " & " + d2 + " & " + d3 + "\\\ \n"

    d1 = str(group_cur["\{SUL\}"]["N5"]).format(precision=1)
    d2 = str(group_cur["\{M13\}"]["N5"]).format(precision=1)
    d3 = str(group_cur["\{SUL,M13\}"]["N5"]).format(precision=1)
    data2 = "$\\textit{mut}_{9}(\S)$ & " + d1 + " & " + d2 + " & " + d3 + "\\\ \n"

    d1 = str(group_cur["\{SUL\}"]["N3"]).format(precision=1)
    d2 = str(group_cur["\{M13\}"]["N3"]).format(precision=1)
    d3 = str(group_cur["\{SUL,M13\}"]["N3"]).format(precision=1)
    data3 = "$\\textit{mut}_{14}(\S)$ & " + d1 + " & " + d2 + " & " + d3 +  "\\\ \n"

    with open('./experiments/results/plots/exp3_table2.tex', 'w') as tabletex:
        tabletex.write("\\begin{table}\n\centering\\begin{tabular}{llll}\n\\toprule\n")
        tabletex.write("SUL & \{$\S$\} & \{$\\textit{mut}_{13}(\S)$\} & \{$\S$,$\\textit{mut}_{13}(\S)$\} \\\ \n\midrule\n")
        tabletex.write(data1)
        tabletex.write(data2)
        tabletex.write(data3)
        tabletex.write("\\bottomrule\\\ \n\\end{tabular}\n\\caption*{Fig. 4c: Summed inputs in millions for learning some mutated \S.}\n\\label{tab:my_exp4c}\n\\end{table}")

def firstProcessing():
    resfile = open("./experiments/results/experiment3/results_exp3.csv", "a")
    directory = os.fsencode("./experiments/results/experiment3/results/")

    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if filename.endswith(".csv"):
            runfile = open("./experiments/results/experiment3/results/"+filename, "r+")
            mylines = runfile.readlines()
            if len(mylines) != 0:
                myline = mylines[-1]
                myline = myline.replace("\n","")
                mutation = filename[:2]
                myrefs = filename.split("_")[1]
                if (mutation == "N4" or mutation == "N5") and myline.count("13manymut")==1:
                    myrefs = "1"
                elif (mutation == "N4" or mutation == "N5") and myline.count(".dot")==2:
                    myrefs = "2"
                elif (mutation == "N4" or mutation == "N5") and myline.count(".dot")==3:
                    myrefs = "12"
                elif (mutation == "N1" or mutation == "N2") and myline.count(".dot")==4:
                    murefs = "123"            
                elif (mutation == "N1" or mutation == "N2") and myrefs != "123":
                    myrefs = "1"     
                resfile.write(myline+","+mutation+","+myrefs+"\n")
            runfile.close()
    resfile.close()


if __name__ in ["__main__"]:
    firstProcessing()
    df = pd.read_csv("./experiments/results/experiment3/results_exp3.csv", sep=",")
    df = df.sort_values(by=["sul", "algorithm"], ignore_index=True)
    df["valid"] = df.apply(mycheck, axis=1)
    df["typemodel"] = df.apply(typemodel, axis=1)
    df["finished"] = 1
    df["total"] = df["mq_sym"] + df["eq_sym"]

    makeTable1(df)
    makeTable2(df)

