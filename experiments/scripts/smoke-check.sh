#!/bin/bash

java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed 51 -sul ./experiments/models/smokecheck_models/sul.dot -ref ./experiments/models/smokecheck_models/reference1.dot ./experiments/models/smokecheck_models/reference2.dot -resfile ./smokecheck.csv -R -M -A -timeout 60

rm ./smokecheck.csv

