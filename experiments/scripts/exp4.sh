#!/bin/bash
declare path_to_models="./experiments/models/automata_wiki/Mealy/principle/"
declare models=("BenchmarkTCP/TCP_Linux_Client.dot" "BenchmarkTCP/TCP_FreeBSD_Client.dot" "BenchmarkTCP/TCP_Windows8_Client.dot" "BenchmarkDTLS/ctinydtls_ecdhe_cert_req.dot" "BenchmarkDTLS/etinydtls_ecdhe_cert_req.dot" "BenchmarkDTLS/gnutls-3.6.7_all_cert_req.dot" "BenchmarkDTLS/mbedtls_all_cert_req.dot" "BenchmarkDTLS/scandium-2.0.0_ecdhe_cert_req.dot" "BenchmarkDTLS/scandium_latest_ecdhe_cert_req.dot" "BenchmarkDTLS/wolfssl-4.0.0_dhe_ecdhe_rsa_cert_req.dot")
declare modelsname=("TCP_Linux_Client" "TCP_FreeBSD_Client" "TCP_Windows8_Client" "ctinydtls" "etinydtls" "gnutls" "mbedtls" "scandium2" "scandium" "wolfssl")

declare path_to_logs="./experiments/results/experiment4/logs/"
declare path_to_results="./experiments/results/experiment4/results/"
declare -i TIMEOUT=18000
declare -i seedlimit=30
declare -i TOTAL=2250
N=24

## Run the no reference experiments with lsharp
for seed in $(seq 1 $seedlimit);
do
    for i in "${!models[@]}"
    do
        (java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn lsharp -seed "${seed}" -sul "${path_to_models}${models[i]}" -resfile "${path_to_results}${modelsname[i]}"_lsharp_${seed}.csv -timeout "${TIMEOUT}" > "${path_to_logs}${modelsname[i]}"_lsharp_${seed}.log ) &
        if [[ $(jobs -r -p | wc -l) -ge $N ]]; then
            # now there are $N jobs already running, so wait here for any job
            # to be finished so there is a place to start next one.
            wait -n
            dir="${path_to_results}"
            count=$(ls "${dir}" | wc -l)
            echo ${count}"/"${TOTAL} " runs for experiment 4 finished!"
        fi
    done
done
wait

## Run the reference experiments with adaptive lsharp
for seed in $(seq 1 $seedlimit);
    do
      (
        java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}BenchmarkTCP/TCP_Linux_Client.dot" -ref "${path_to_models}BenchmarkTCP/TCP_FreeBSD_Client.dot" -resfile ${path_to_results}LinuxClient_FreeBSDClient_adaptivelsharp_${seed}.csv -R -M -A -timeout "${TIMEOUT}" > ${path_to_logs}LinuxClient_FreeBSDClient_adaptivelsharp_${seed}.log
        java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}BenchmarkTCP/TCP_Linux_Client.dot" -ref "${path_to_models}BenchmarkTCP/TCP_Windows8_Client.dot" -resfile ${path_to_results}LinuxClient_Windows8Client_adaptivelsharp_${seed}.csv -R -M -A -timeout "${TIMEOUT}" > ${path_to_logs}LinuxClient_Windows8Client_adaptivelsharp_${seed}.log
        java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}BenchmarkTCP/TCP_Linux_Client.dot" -ref "${path_to_models}BenchmarkTCP/TCP_FreeBSD_Client.dot" "${path_to_models}BenchmarkTCP/TCP_Windows8_Client.dot" -resfile ${path_to_results}LinuxClient_FreeBSDClient_Windows8Client_adaptivelsharp_${seed}.csv -R -M -A -timeout "${TIMEOUT}" > ${path_to_logs}LinuxClient_FreeBSDClient_Windows8Client_adaptivelsharp_${seed}.log

        java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}BenchmarkTCP/TCP_Windows8_Client.dot" -ref "${path_to_models}BenchmarkTCP/TCP_FreeBSD_Client.dot" -resfile ${path_to_results}Windows8Client_FreeBSDClient_adaptivelsharp_${seed}.csv -R -M -A -timeout "${TIMEOUT}" > ${path_to_logs}Windows8Client_FreeBSDClient_adaptivelsharp_${seed}.log
        java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}BenchmarkTCP/TCP_Windows8_Client.dot" -ref "${path_to_models}BenchmarkTCP/TCP_Linux_Client.dot" -resfile ${path_to_results}Windows8Client_LinuxClient_adaptivelsharp_${seed}.csv -R -M -A -timeout "${TIMEOUT}" > ${path_to_logs}Windows8Client_LinuxClient_adaptivelsharp_${seed}.log
        java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}BenchmarkTCP/TCP_Windows8_Client.dot" -ref "${path_to_models}BenchmarkTCP/TCP_FreeBSD_Client.dot" "${path_to_models}BenchmarkTCP/TCP_Linux_Client.dot" -resfile ${path_to_results}Windows8Client_FreeBSDClient_LinuxClient_adaptivelsharp_${seed}.csv -R -M -A -timeout "${TIMEOUT}" > ${path_to_logs}Windows8Client_FreeBSDClient_LinuxClient_adaptivelsharp_${seed}.log

        java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}BenchmarkTCP/TCP_FreeBSD_Client.dot" -ref "${path_to_models}BenchmarkTCP/TCP_Windows8_Client.dot" -resfile ${path_to_results}FreeBSDClient_Windows8Client_adaptivelsharp_${seed}.csv -R -M -A -timeout "${TIMEOUT}" > ${path_to_logs}FreeBSDClient_Windows8Client_adaptivelsharp_${seed}.log
        java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}BenchmarkTCP/TCP_FreeBSD_Client.dot" -ref "${path_to_models}BenchmarkTCP/TCP_Linux_Client.dot" -resfile ${path_to_results}FreeBSDClient_LinuxClient_adaptivelsharp_${seed}.csv -R -M -A -timeout "${TIMEOUT}" > ${path_to_logs}FreeBSDClient_LinuxClient_adaptivelsharp_${seed}.log
        java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}BenchmarkTCP/TCP_FreeBSD_Client.dot" -ref "${path_to_models}BenchmarkTCP/TCP_Windows8_Client.dot" "${path_to_models}BenchmarkTCP/TCP_Linux_Client.dot" -resfile ${path_to_results}FreeBSDClient_Windows8Client_LinuxClient_adaptivelsharp_${seed}.csv -R -M -A -timeout "${TIMEOUT}" > ${path_to_logs}FreeBSDClient_Windows8Client_LinuxClient_adaptivelsharp_${seed}.log
      ) &
        if [[ $(jobs -r -p | wc -l) -ge $N ]]; then
            # now there are $N jobs already running, so wait here for any job
            # to be finished so there is a place to start next one.
            wait -n
            dir="${path_to_results}"
            count=$(ls "${dir}" | wc -l)
            echo ${count}"/"${TOTAL} " runs for experiment 4 finished!"
        fi
    done
wait

declare -a DTLS=("BenchmarkDTLS/ctinydtls_ecdhe_cert_req.dot" "BenchmarkDTLS/etinydtls_ecdhe_cert_req.dot" "BenchmarkDTLS/gnutls-3.6.7_all_cert_req.dot" "BenchmarkDTLS/mbedtls_all_cert_req.dot" "BenchmarkDTLS/scandium-2.0.0_ecdhe_cert_req.dot" "BenchmarkDTLS/scandium_latest_ecdhe_cert_req.dot" "BenchmarkDTLS/wolfssl-4.0.0_dhe_ecdhe_rsa_cert_req.dot")
declare -a DTLSnames=("ctiny" "etiny" "gnu" "mbedtls" "scandium2" "scandium" "wolfssl")


for seed in $(seq 1 $seedlimit);
do
  for i in "${!DTLS[@]}"
  do
     for j in "${!DTLS[@]}"
     do
       if [ $i != 14 ]; then
         ( java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}${DTLS[i]}" -ref "${path_to_models}${DTLS[j]}" -resfile "${path_to_results}${DTLSnames[i]}_${DTLSnames[j]}"_adaptivelsharp_${seed}_RMA.csv -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}${DTLSnames[i]}_${DTLSnames[j]}"_adaptivelsharp_${seed}.log) &
         if [[ $(jobs -r -p | wc -l) -ge $N ]]; then
             # now there are $N jobs already running, so wait here for any job
             # to be finished so there is a place to start next one.
             wait -n
             dir="${path_to_results}"
             count=$(ls "${dir}" | wc -l)
             echo ${count}"/"${TOTAL} " runs for experiment 4 finished!"
         fi
      fi
      done
    done
  done
wait

for seed in $(seq 1 $seedlimit);
do
  (
  java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}${DTLS[0]}" -ref "${path_to_models}${DTLS[1]}" "${path_to_models}${DTLS[2]}" "${path_to_models}${DTLS[3]}" "${path_to_models}${DTLS[4]}" "${path_to_models}${DTLS[5]}" "${path_to_models}${DTLS[6]}" -resfile "${path_to_results}${DTLSnames[0]}"_all_adaptivelsharp_${seed}_RMA.csv -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}${DTLSnames[0]}"_all_adaptivelsharp_${seed}.log
  java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}${DTLS[1]}" -ref "${path_to_models}${DTLS[0]}" "${path_to_models}${DTLS[2]}" "${path_to_models}${DTLS[3]}" "${path_to_models}${DTLS[4]}" "${path_to_models}${DTLS[5]}" "${path_to_models}${DTLS[6]}" -resfile "${path_to_results}${DTLSnames[1]}"_all_adaptivelsharp_${seed}_RMA.csv -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}${DTLSnames[1]}"_all_adaptivelsharp_${seed}.log
  java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}${DTLS[2]}" -ref "${path_to_models}${DTLS[0]}" "${path_to_models}${DTLS[1]}" "${path_to_models}${DTLS[3]}" "${path_to_models}${DTLS[4]}" "${path_to_models}${DTLS[5]}" "${path_to_models}${DTLS[6]}" -resfile "${path_to_results}${DTLSnames[2]}"_all_adaptivelsharp_${seed}_RMA.csv -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}${DTLSnames[2]}"_all_adaptivelsharp_${seed}.log
  java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}${DTLS[3]}" -ref "${path_to_models}${DTLS[0]}" "${path_to_models}${DTLS[1]}" "${path_to_models}${DTLS[2]}" "${path_to_models}${DTLS[4]}" "${path_to_models}${DTLS[5]}" "${path_to_models}${DTLS[6]}" -resfile "${path_to_results}${DTLSnames[3]}"_all_adaptivelsharp_${seed}_RMA.csv -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}${DTLSnames[3]}"_all_adaptivelsharp_${seed}.log
  java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}${DTLS[4]}" -ref "${path_to_models}${DTLS[0]}" "${path_to_models}${DTLS[1]}" "${path_to_models}${DTLS[2]}" "${path_to_models}${DTLS[3]}" "${path_to_models}${DTLS[5]}" "${path_to_models}${DTLS[6]}" -resfile "${path_to_results}${DTLSnames[4]}"_all_adaptivelsharp_${seed}_RMA.csv -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}${DTLSnames[4]}"_all_adaptivelsharp_${seed}.log
  java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}${DTLS[5]}" -ref "${path_to_models}${DTLS[0]}" "${path_to_models}${DTLS[1]}" "${path_to_models}${DTLS[2]}" "${path_to_models}${DTLS[3]}" "${path_to_models}${DTLS[4]}" "${path_to_models}${DTLS[6]}" -resfile "${path_to_results}${DTLSnames[5]}"_all_adaptivelsharp_${seed}_RMA.csv -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}${DTLSnames[5]}"_all_adaptivelsharp_${seed}.log
  java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}${DTLS[6]}" -ref "${path_to_models}${DTLS[0]}" "${path_to_models}${DTLS[1]}" "${path_to_models}${DTLS[2]}" "${path_to_models}${DTLS[3]}" "${path_to_models}${DTLS[4]}" "${path_to_models}${DTLS[5]}" -resfile "${path_to_results}${DTLSnames[6]}"_all_adaptivelsharp_${seed}_RMA.csv -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}${DTLSnames[6]}"_all_adaptivelsharp_${seed}.log
  ) &
        if [[ $(jobs -r -p | wc -l) -ge $N ]]; then
             # now there are $N jobs already running, so wait here for any job
             # to be finished so there is a place to start next one.
             wait -n
             dir="${path_to_results}"
             count=$(ls "${dir}" | wc -l)
             echo ${count}"/"${TOTAL} " runs for experiment 4 finished!"
         fi
done
wait

# make one combined file
touch ./experiments/results/experiment4/results_exp4.csv
echo "sul,refs,algorithm,states,inputs,time,isEquiv,rounds,mq_sym,mq_rst,eq_sym,eq_rst,rebuildOQ,rebuildStates,matchRefinementOQ,matchRefinementStates,matchSeparationOQ,matchSeparationStates" > ./experiments/results/experiment4/results_exp4.csv
find ./experiments/results/experiment4/results/ -type f -exec cat {} + >> ./experiments/results/experiment4/results_exp4.csv
python3 ./experiments/scripts/exp4_processing.py
# pdflatex -synctex=1 -interaction=nonstopmode -output-directory=./experiments/results/plots/ ./experiments/results/plots/main.tex
