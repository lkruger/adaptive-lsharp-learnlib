#!/bin/bash

declare path_to_models="./experiments/models/automata_wiki/Mealy/principle/"
declare path_to_mutants="./experiments/models/mutated_models/"
declare models=("BenchmarkBankcard/learnresult_fix.dot" "BenchmarkSSH/DropBear.dot" "BenchmarkTLS/NSS_3.17.4_server_regular.dot" "BenchmarkTLS/GnuTLS_3.3.8_client_full.dot")
declare modelsname=("learnresult_fix" "DropBear" "NSS_3.17.4_server_regular" "GnuTLS_3.3.8_client_full")
declare changestate=("7" "16" "5" "4")

declare mutantnameexp1=("0" "1dummy" "2init" "3addstate" "4remstate" "5diverttrans" "6changeout" "7remsym" "8concatA" "9concatB" "10mutate4" "11mutate5" "12changetrans" "13manymut" "14union")
declare mutantname=("0" "1_m13" "2_m12" "3_m14" "4_m8" "5_m9")

declare -i seedlimit=15
declare -i COUNTER=0
declare -i TIMEOUT=18000 # in seconds!
declare -i MUTATIONS=$((seedlimit * 132))
declare -i TOTAL=780
N=6

declare path_to_logs="./experiments/results/experiment3/logs/"
declare path_to_results="./experiments/results/experiment3/results/"

## Generate the mutant models
seedtimes3=$((seedlimit * 3))
for seed in $(seq 1 $seedtimes3);
do
   for model in ${!models[@]}
   do
         (
         java -jar ./experiments/scripts/GenerateMutatedModels.jar -sul "${path_to_models}${models[model]}" -out ${path_to_mutants}10mutate4_${modelsname[model]}_${seed}.dot -seed "${seed}" -mut 10 -state "${changestate[model]}"
         java -jar ./experiments/scripts/GenerateMutatedModels.jar -sul "${path_to_models}${models[model]}" -out ${path_to_mutants}12changetrans_${modelsname[model]}_${seed}.dot -seed "${seed}" -mut 12 -state "${changestate[model]}"
         ) &
         if [[ $(jobs -r -p | wc -l) -ge $N ]]; then
             wait -n
             dir="${path_to_mutants}"
             count=$(ls "${dir}" | wc -l)
             echo ${count}"/"$MUTATIONS" mutated models for experiment 3 generated!"
         fi
   done
done
wait

for seed in $(seq 1 $seedlimit);
do
    for model in ${!models[@]}
    do
          (
          java -jar ./experiments/scripts/GenerateMutatedModels.jar -sul "${path_to_models}${models[model]}" -out ${path_to_mutants}13manymut_${modelsname[model]}_${seed}.dot -seed "${seed}" -mut 13 -state "${changestate[model]}"
          java -jar ./experiments/scripts/GenerateMutatedModels.jar -sul "${path_to_models}${models[model]}" -out ${path_to_mutants}1dummy_${modelsname[model]}_${seed}.dot -seed "${seed}" -mut 1 -state "${changestate[model]}"
          ) &
          if [[ $(jobs -r -p | wc -l) -ge $N ]]; then
              wait -n
              dir="${path_to_mutants}"
              count=$(ls "${dir}" | wc -l)
              echo ${count}"/"$MUTATIONS" mutated models for experiment 3 generated!"
          fi
    done
done
wait

for seed in $(seq 1 $seedlimit);
do
    for model in ${!models[@]}
    do
          (
          java -jar ./experiments/scripts/GenerateMutatedModels.jar -sul "${path_to_models}${models[model]}" -sul2 "${path_to_mutants}13manymut_${modelsname[model]}_${seed}.dot" -out ${path_to_mutants}14union_orig_13manymut_${modelsname[model]}_${seed}.dot -seed "${seed}" -mut 15 -state "${changestate[model]}"
          java -jar ./experiments/scripts/GenerateMutatedModels.jar -sul "${path_to_models}${models[model]}" -sul2 "${path_to_mutants}13manymut_${modelsname[model]}_${seed}.dot" -out ${path_to_mutants}8concatA_orig_13manymut_${modelsname[model]}_${seed}.dot -seed "${seed}" -mut 16 -state "${changestate[model]}"
          java -jar ./experiments/scripts/GenerateMutatedModels.jar -sul "${path_to_models}${models[model]}" -sul2 "${path_to_mutants}13manymut_${modelsname[model]}_${seed}.dot" -out ${path_to_mutants}9concatB_orig_13manymut_${modelsname[model]}_${seed}.dot -seed "${seed}" -mut 17 -state "${changestate[model]}"
          ) &
          if [[ $(jobs -r -p | wc -l) -ge $N ]]; then
              wait -n
              dir="${path_to_mutants}"
              count=$(ls "${dir}" | wc -l)
              echo ${count}"/"$MUTATIONS" mutated models for experiment 3 generated!"
          fi
    done
done
wait
echo "All mutated models are generated!"

# mutation 1
for seed in $(seq 1 $seedlimit);
do
   # shellcheck disable=SC2068
   for model in ${!models[@]}
   do
         (
           seed2=$((seed + seedlimit))
           seed3=$((seed + 2*seedlimit))
           myref1=${path_to_mutants}10mutate4_${modelsname[model]}_${seed}.dot
           myref2=${path_to_mutants}10mutate4_${modelsname[model]}_${seed2}.dot
           myref3=${path_to_mutants}10mutate4_${modelsname[model]}_${seed3}.dot
           java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}${models[model]}" -ref "${myref1}" "${myref2}" "${myref3}" -resfile "${path_to_results}N1_123_${modelsname[model]}_adaptivelsharp_${seed}_RAM.csv" -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}N1_123_${modelsname[model]}_adaptivelsharp_${seed}_RAM.log"
           java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}${models[model]}" -ref "${myref1}" -resfile "${path_to_results}N1_1_${modelsname[model]}_adaptivelsharp_${seed}_RAM.csv" -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}N1_1_${modelsname[model]}_adaptivelsharp_${seed}_RAM.log"
           java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}${models[model]}" -ref "${myref2}" -resfile "${path_to_results}N1_1_${modelsname[model]}_adaptivelsharp_${seed}_RAM.csv" -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}N1_1_${modelsname[model]}_adaptivelsharp_${seed}_RAM.log"
           java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}${models[model]}" -ref "${myref3}" -resfile "${path_to_results}N1_1_${modelsname[model]}_adaptivelsharp_${seed}_RAM.csv" -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}N1_1_${modelsname[model]}_adaptivelsharp_${seed}_RAM.log"
         ) &

         if [[ $(jobs -r -p | wc -l) -ge $N ]]; then
             wait -n
             dir="${path_to_results}"
             count=$(ls "${dir}" | wc -l)
             echo ${count}"/"$TOTAL " runs for experiment 3 finished!"
         fi
   done
done
wait 

# mutation 2
for seed in $(seq 1 $seedlimit);
do
   for model in ${!models[@]}
   do
         (
           seed2=$((seed + seedlimit))
           seed3=$((seed + 2*seedlimit))
           myref1=${path_to_mutants}12changetrans_${modelsname[model]}_${seed}.dot
           myref2=${path_to_mutants}12changetrans_${modelsname[model]}_${seed2}.dot
           myref3=${path_to_mutants}12changetrans_${modelsname[model]}_${seed3}.dot
           java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}${models[model]}" -ref "${myref1}" "${myref2}" "${myref3}" -resfile "${path_to_results}N2_123__${modelsname[model]}_adaptivelsharp_${seed}_RAM.csv" -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}N2_123_${modelsname[model]}_adaptivelsharp_${seed}_RAM.log"
           java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}${models[model]}" -ref "${myref1}" -resfile "${path_to_results}N2_1_${modelsname[model]}_adaptivelsharp_${seed}_RAM.csv" -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}N2_1_${modelsname[model]}_adaptivelsharp_${seed}_RAM.log"
           java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}${models[model]}" -ref "${myref2}" -resfile "${path_to_results}N2_1_${modelsname[model]}_adaptivelsharp_${seed}_RAM.csv" -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}N2_1_${modelsname[model]}_adaptivelsharp_${seed}_RAM.log"
           java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_models}${models[model]}" -ref "${myref3}" -resfile "${path_to_results}N2_1_${modelsname[model]}_adaptivelsharp_${seed}_RAM.csv" -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}N2_1_${modelsname[model]}_adaptivelsharp_${seed}_RAM.log"
         ) &
         if [[ $(jobs -r -p | wc -l) -ge $N ]]; then
             wait -n
             dir="${path_to_results}"
             count=$(ls "${dir}" | wc -l)
             echo ${count}"/"$TOTAL " runs for experiment 3 finished!"
         fi
   done
done
wait

# mutation 3
for seed in $(seq 1 $seedlimit);
do
    for model in ${!models[@]}
    do
          (
            myref1=${path_to_mutants}1dummy_${modelsname[model]}_${seed}.dot
            myref2=${path_to_mutants}13manymut_${modelsname[model]}_${seed}.dot
            sul=${path_to_mutants}14union_orig_13manymut_${modelsname[model]}_${seed}.dot
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${sul}" -ref "${myref1}" "${myref2}" -resfile "${path_to_results}N3_12_orig_13manymut_${modelsname[model]}_${seed}_adaptivelsharp_${seed}_RAM.csv" -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}N3_12_orig_13manymut_${modelsname[model]}_adaptivelsharp_${seed}_RAM.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${sul}" -ref "${myref1}" -resfile "${path_to_results}N3_1_orig_13manymut_${modelsname[model]}_${seed}_adaptivelsharp_${seed}_RAM.csv" -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}N3_1_orig_13manymut_${modelsname[model]}_adaptivelsharp_${seed}_RAM.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${sul}" -ref "${myref2}" -resfile "${path_to_results}N3_2_orig_13manymut_${modelsname[model]}_${seed}_adaptivelsharp_${seed}_RAM.csv" -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}N3_2_orig_13manymut_${modelsname[model]}_adaptivelsharp_${seed}_RAM.log"
          ) &
          if [[ $(jobs -r -p | wc -l) -ge $N ]]; then
              dir="${path_to_results}"
              count=$(ls "${dir}" | wc -l)
              echo ${count}"/"$TOTAL " runs for experiment 3 finished!"
          fi
    done
done
wait

## mutation 4 + 5
for seed in $(seq 1 $seedlimit);
do
    for model in ${!models[@]}
    do
          (
            myref1=${path_to_models}${models[model]}
            myref2=${path_to_mutants}13manymut_${modelsname[model]}_${seed}.dot
            sul1=${path_to_mutants}8concatA_orig_13manymut_${modelsname[model]}_${seed}.dot
            sul2=${path_to_mutants}9concatB_orig_13manymut_${modelsname[model]}_${seed}.dot
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${sul1}" -ref "${myref1}" "${myref2}" -resfile "${path_to_results}N4_12_8concatA_orig_13manymut_${modelsname[model]}_${seed}_adaptivelsharp_${seed}_RAM.csv" -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}N4_12_8concatA_orig_${modelsname[model]}_adaptivelsharp_${seed}_RAM.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${sul1}" -ref "${myref1}" -resfile "${path_to_results}N4_1_8concatA_orig_${modelsname[model]}_${seed}_adaptivelsharp_${seed}_RAM.csv" -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}N4_1_8concatA_orig_${modelsname[model]}_adaptivelsharp_${seed}_RAM.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${sul1}" -ref "${myref2}" -resfile "${path_to_results}N4_2_8concatA_13manymut_${modelsname[model]}_${seed}_adaptivelsharp_${seed}_RAM.csv" -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}N4_2_8concatA_13manymut_${modelsname[model]}_adaptivelsharp_${seed}_RAM.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${sul2}" -ref "${myref1}" "${myref2}" -resfile "${path_to_results}N5_12_9concatB_orig_13manymut_${modelsname[model]}_${seed}_adaptivelsharp_${seed}_RAM.csv" -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}N5_12_9concatB_orig_${modelsname[model]}_adaptivelsharp_${seed}_RAM.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${sul2}" -ref "${myref1}" -resfile "${path_to_results}N5_1_9concatB_orig_${modelsname[model]}_${seed}_adaptivelsharp_${seed}_RAM.csv" -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}N5_1_9concatB_orig_${modelsname[model]}_adaptivelsharp_${seed}_RAM.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${sul2}" -ref "${myref2}" -resfile "${path_to_results}N5_2_9concatB_13manymut_${modelsname[model]}_${seed}_adaptivelsharp_${seed}_RAM.csv" -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}N5_2_9concatB_13manymut_${modelsname[model]}_adaptivelsharp_${seed}_RAM.log"
          ) &
          if [[ $(jobs -r -p | wc -l) -ge $N ]]; then
              dir="${path_to_results}"
              count=$(ls "${dir}" | wc -l)
              echo ${count}"/"$TOTAL " runs for experiment 3 finished!"
          fi
    done
done
wait

dir="${path_to_results}"
count=$(ls "${dir}" | wc -l)
echo ${count}"/"$TOTAL " runs for experiment 3 finished!"
echo "All runs for experiment 3 done!"

# make one combined file
touch ./experiments/results/experiment3/results_exp3_init.csv
find ./experiments/results/experiment3/results/ -type f -exec cat {} + >> ./experiments/results/experiment3/results_exp3_init.csv

touch ./experiments/results/experiment3/results_exp3.csv
echo "sul,refs,algorithm,states,inputs,time,isEquiv,rounds,mq_sym,mq_rst,eq_sym,eq_rst,rebuildOQ,rebuildStates,matchRefinementOQ,matchRefinementStates,matchSeparationOQ,matchSeparationStates,mutation,myrefs" > ./experiments/results/experiment3/results_exp3.csv

python3 ./experiments/scripts/exp3_processing.py
# pdflatex -synctex=1 -interaction=nonstopmode -output-directory=./experiments/results/plots/ ./experiments/results/plots/main.tex
