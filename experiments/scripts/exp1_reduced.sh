#!/bin/bash

declare path_to_models="./experiments/models/automata_wiki/Mealy/principle/"
declare path_to_mutants="./experiments/models/mutated_models/"
declare path_to_logs="./experiments/results/experiment1/logs/"
declare path_to_results="./experiments/results/experiment1/results/"

declare models=("BenchmarkBankcard/learnresult_fix.dot" "BenchmarkSSH/DropBear.dot" "BenchmarkTLS/NSS_3.17.4_server_regular.dot" "BenchmarkTLS/GnuTLS_3.3.8_client_full.dot")
declare modelsname=("learnresult_fix" "DropBear" "NSS_3.17.4_server_regular" "GnuTLS_3.3.8_client_full" )
declare mutantname=("0" "1dummy" "2init" "3addstate" "4remstate" "5diverttrans" "6changeout" "7remsym" "8concatA" "9concatB" "10mutate4" "11mutate5" "12changetrans" "13manymut" "14union")
declare changestate=("7" "16" "4" "0")

declare -i seedlimit=10
declare -i COUNTER=0
declare -i TIMEOUT=18000 # in seconds
declare -i MUTATIONS=560

# models * seeds * mutants * algorithms
# need to account that IKV and PDL* cannot remove symbols
declare -i TOTAL=5520
N=6

## Generate the mutant models
for seed in $(seq 1 $seedlimit);
do
    for model in ${!models[@]}
    do
        for i in $(seq 1 14);
        do
            (java -jar ./experiments/scripts/GenerateMutatedModels.jar -sul "${path_to_models}${models[model]}" -out ${path_to_mutants}${mutantname[i]}_${modelsname[model]}_${seed}.dot -seed "${seed}" -mut "${i}" -state "${changestate[model]}") &

            if [[ $(jobs -r -p | wc -l) -ge $N ]]; then
                wait -n
                dir="${path_to_mutants}"
                count=$(ls "${dir}" | wc -l)
                echo ${count}"/"$MUTATIONS" mutated models for experiment 1 generated!"
            fi
        done
    done
done
wait
echo "All mutated models are generated!"

# do the actual experiments
for seed in $(seq 1 $seedlimit);
do
    for model in ${!models[@]}
    do
        for i in $(seq 1 14);
        do
            (
            if [ $i != 14 ]; then
              myref="${path_to_models}${models[model]}"
              myrefname="${modelsname[model]}"
            else
              myref=${path_to_mutants}1dummy_${modelsname[model]}_${seed}.dot
              myrefname="1dummy_${modelsname[model]}_${seed}"
            fi

            myref="${path_to_models}${models[model]}"
            myrefname="${modelsname[model]}"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn lsharp -seed "${seed}" -sul ${path_to_mutants}${mutantname[i]}_${modelsname[model]}_${seed}.dot -resfile "${path_to_results}${mutantname[i]}_${modelsname[model]}_lsharp_${seed}.csv" -timeout "${TIMEOUT}" > "${path_to_logs}${mutantname[i]}_${modelsname[model]}_lsharp_${seed}.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn kv -seed "${seed}" -sul ${path_to_mutants}${mutantname[i]}_${modelsname[model]}_${seed}.dot -resfile "${path_to_results}${mutantname[i]}_${modelsname[model]}_kv_${seed}.csv" -timeout "${TIMEOUT}" > "${path_to_logs}${mutantname[i]}_${modelsname[model]}_kv_${seed}.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn lstar -seed "${seed}" -sul ${path_to_mutants}${mutantname[i]}_${modelsname[model]}_${seed}.dot -resfile "${path_to_results}${mutantname[i]}_${modelsname[model]}_lstar_${seed}.csv" -timeout "${TIMEOUT}" > "${path_to_logs}${mutantname[i]}_${modelsname[model]}_lstar_${seed}.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul ${path_to_mutants}${mutantname[i]}_${modelsname[model]}_${seed}.dot -ref "${myref}" -resfile "${path_to_results}${mutantname[i]}_${modelsname[model]}_adaptivelsharp_${seed}_RM.csv" -R -M -timeout "${TIMEOUT}" > "${path_to_logs}${mutantname[i]}_${myrefname}_adaptivelsharp_${seed}_RM.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul ${path_to_mutants}${mutantname[i]}_${modelsname[model]}_${seed}.dot -ref "${myref}" -resfile "${path_to_results}${mutantname[i]}_${modelsname[model]}_adaptivelsharp_${seed}_RAM.csv" -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}${mutantname[i]}_${myrefname}_adaptivelsharp_${seed}_RAM.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul ${path_to_mutants}${mutantname[i]}_${modelsname[model]}_${seed}.dot -ref "${myref}" -resfile "${path_to_results}${mutantname[i]}_${modelsname[model]}_adaptivelsharp_${seed}_R.csv" -R -timeout "${TIMEOUT}" > "${path_to_logs}${mutantname[i]}_${myrefname}_adaptivelsharp_${seed}_R.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul ${path_to_mutants}${mutantname[i]}_${modelsname[model]}_${seed}.dot -ref "${myref}" -resfile "${path_to_results}${mutantname[i]}_${modelsname[model]}_adaptivelsharp_${seed}_M.csv" -M -timeout "${TIMEOUT}" > "${path_to_logs}${mutantname[i]}_${myrefname}_adaptivelsharp_${seed}_M.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul ${path_to_mutants}${mutantname[i]}_${modelsname[model]}_${seed}.dot -ref "${myref}" -resfile "${path_to_results}${mutantname[i]}_${modelsname[model]}_adaptivelsharp_${seed}_MQuan.csv" -M -A -timeout "${TIMEOUT}" > "${path_to_logs}${mutantname[i]}_${myrefname}_adaptivelsharp_${seed}_MQuan.log"
            if [ $i != 7 ]; then
              java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivekv -seed "${seed}" -sul ${path_to_mutants}${mutantname[i]}_${modelsname[model]}_${seed}.dot -ref "${myref}" -resfile "${path_to_results}${mutantname[i]}_${modelsname[model]}_adaptivekv_${seed}.csv" -timeout "${TIMEOUT}" > "${path_to_logs}${mutantname[i]}_${myrefname}_adaptivekv_${seed}.log"
              java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn pdlstar -seed "${seed}" -sul ${path_to_mutants}${mutantname[i]}_${modelsname[model]}_${seed}.dot -ref "${myref}" -resfile "${path_to_results}${mutantname[i]}_${modelsname[model]}_pdlstar_${seed}.csv" -timeout "${TIMEOUT}" > "${path_to_logs}${mutantname[i]}_${myrefname}_pdlstar_${seed}.log"
            fi
            ) &
            if [[ $(jobs -r -p | wc -l) -ge $N ]]; then
                wait -n
                dir="${path_to_results}"
                count=$(ls "${dir}" | wc -l)
                echo ${count}"/"$TOTAL " runs for experiment 1 finished!"
            fi
        done
    done
done
wait
dir="${path_to_results}"
count=$(ls "${dir}" | wc -l)
echo ${count}"/"$TOTAL " runs for experiment 1 finished!"
echo "All runs for experiment 1 done!"

# make one combined file
touch ./experiments/results/experiment1/results_exp1.csv
echo "sul,refs,algorithm,states,inputs,time,isEquiv,rounds,mq_sym,mq_rst,eq_sym,eq_rst,rebuildOQ,rebuildStates,matchRefinementOQ,matchRefinementStates,matchSeparationOQ,matchSeparationStates" > ./experiments/results/experiment1/results_exp1.csv
find ./experiments/results/experiment1/results/ -type f -exec cat {} + >> ./experiments/results/experiment1/results_exp1.csv
python3 ./experiments/scripts/exp1_processing.py
# pdflatex -synctex=1 -interaction=nonstopmode -output-directory=./experiments/results/plots/ ./experiments/results/plots/main.tex
