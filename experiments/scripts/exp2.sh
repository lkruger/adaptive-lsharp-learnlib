#!/bin/bash
declare -a openSSL_target=("server_097c" "server_097e" "server_098f" "server_098l" "server_098m" "server_098s" "server_098u" "server_098za" "server_100" "server_100f" "server_100h" "server_100m" "server_101" "server_101h" "server_101k" "server_102" "server_110pre1")
declare -a openSSL_ref=("server_097" "server_097c" "server_097e" "server_098f" "server_098l" "server_098m" "server_098s" "server_098u" "server_098m" "server_100" "server_100f" "server_100h" "server_100h" "server_101" "server_101h" "server_101k" "server_102")

declare path_to_logs="./experiments/results/experiment2/logs/"
declare path_to_results="./experiments/results/experiment2/results/"
declare -i TIMEOUT=18000
declare -i seedlimit=30
declare -i COUNTER=0
declare -i TOTAL=6600
N=6

declare path_to_openSSL="./experiments/models/openSSL_models/"
## Run the no reference experiments with lsharp and kv
declare -a algorithms=("lsharp" "kv" "lstar")
for algorithm in ${algorithms[@]}
do
    for seed in $(seq 1 $seedlimit);
    do
        for model in ${openSSL_target[@]}
        do
            (java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn "${algorithm}" -seed "${seed}" -sul "${path_to_openSSL}${model}".dot -resfile "${path_to_results}${model}_${algorithm}_${seed}".csv -timeout "${TIMEOUT}" > "${path_to_logs}${model}_${algorithm}_${seed}.log") &
            if [[ $(jobs -r -p | wc -l) -ge $N ]]; then
                # now there are $N jobs already running, so wait here for any job
                # to be finished so there is a place to start next one.
                wait -n
                dir="${path_to_results}"
                count=$(ls "${dir}" | wc -l)
                echo ${count}"/"${TOTAL} " runs for experiment 2 finished!"
            fi
        done
    done
done
wait
echo "OpenSSL with lsharp, kv and lstar done!"

## Run the reference experiments with adaptive lsharp and kv
for seed in $(seq 1 $seedlimit);
do
    for i in "${!openSSL_target[@]}"
        do
          (
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_openSSL}${openSSL_target[i]}".dot -ref "${path_to_openSSL}${openSSL_ref[i]}".dot -resfile "${path_to_results}${openSSL_target[i]}"_${openSSL_ref[i]}"_adaptivelsharp_${seed}_RM.csv" -R -M -timeout "${TIMEOUT}" > "${path_to_logs}${openSSL_target[i]}"_${openSSL_ref[i]}"_adaptivelsharp_${seed}_RM.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_openSSL}${openSSL_target[i]}".dot -ref "${path_to_openSSL}${openSSL_ref[i]}".dot -resfile "${path_to_results}${openSSL_target[i]}"_${openSSL_ref[i]}"_adaptivelsharp_${seed}_RMQuan.csv" -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}${openSSL_target[i]}"_${openSSL_ref[i]}"_adaptivelsharp_${seed}_RMQuan.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_openSSL}${openSSL_target[i]}".dot -ref "${path_to_openSSL}${openSSL_ref[i]}".dot -resfile "${path_to_results}${openSSL_target[i]}"_${openSSL_ref[i]}"_adaptivelsharp_${seed}_R.csv" -R -timeout "${TIMEOUT}" > "${path_to_logs}${openSSL_target[i]}"_${openSSL_ref[i]}"_adaptivelsharp_${seed}_R.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_openSSL}${openSSL_target[i]}".dot -ref "${path_to_openSSL}${openSSL_ref[i]}".dot -resfile "${path_to_results}${openSSL_target[i]}"_${openSSL_ref[i]}"_adaptivelsharp_${seed}_M.csv" -M -timeout "${TIMEOUT}" > "${path_to_logs}${openSSL_target[i]}"_${openSSL_ref[i]}"_adaptivelsharp_${seed}_M.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_openSSL}${openSSL_target[i]}".dot -ref "${path_to_openSSL}${openSSL_ref[i]}".dot -resfile "${path_to_results}${openSSL_target[i]}"_${openSSL_ref[i]}"_adaptivelsharp_${seed}_MQuan.csv" -M -A -timeout "${TIMEOUT}" > "${path_to_logs}${openSSL_target[i]}"_${openSSL_ref[i]}"_adaptivelsharp_${seed}_MQuan.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivekv -seed "${seed}" -sul "${path_to_openSSL}${openSSL_target[i]}".dot -ref "${path_to_openSSL}${openSSL_ref[i]}".dot -resfile "${path_to_results}${openSSL_target[i]}"_${openSSL_ref[i]}"_adaptivekv_${seed}.csv" -timeout "${TIMEOUT}" > "${path_to_logs}${openSSL_target[i]}"_${openSSL_ref[i]}"_adaptivekv_${seed}.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn pdlstar -seed "${seed}" -sul "${path_to_openSSL}${openSSL_target[i]}".dot -ref "${path_to_openSSL}${openSSL_ref[i]}".dot -resfile "${path_to_results}${openSSL_target[i]}"_${openSSL_ref[i]}"_pdlstar_${seed}.csv" -timeout "${TIMEOUT}" > "${path_to_logs}${openSSL_target[i]}"_${openSSL_ref[i]}"_pdlstar_${seed}.log"
            ) &
            if [[ $(jobs -r -p | wc -l) -ge $N ]]; then
                wait -n
                dir="${path_to_results}"
                count=$(ls "${dir}" | wc -l)
                echo ${count}"/"${TOTAL} " runs for experiment 2 finished!"
            fi
        done
done
wait
echo "OpenSSL with adaptive lsharp, adaptive kv and adaptive lstar done!"


# Philips models
declare -a philips_target=("learnresult2-full" "learnresult3-full" "learnresult4-full" "learnresult5-full" "learnresult6-full")
declare -a philips_ref=("learnresult1-full" "learnresult2-full" "learnresult3-full" "learnresult4-full" "learnresult5-full")
declare path_to_philips="./experiments/models/automata_wiki/Mealy/principle/Philips/"
# shellcheck disable=SC2068
for algorithm in ${algorithms[@]}
do
    for seed in $(seq 1 $seedlimit);
    do
        for model in ${philips_target[@]}
        do
            (java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn "${algorithm}" -seed "${seed}" -sul "${path_to_philips}${model}".dot -resfile "${path_to_results}${model}_${algorithm}_${seed}".csv -timeout "${TIMEOUT}" > "${path_to_logs}${model}_${algorithm}_${seed}.log") &
            if [[ $(jobs -r -p | wc -l) -ge $N ]]; then
                wait -n
                dir="${path_to_results}"
                count=$(ls "${dir}" | wc -l)
                echo ${count}"/"${TOTAL} " runs for experiment 2 finished!"
            fi
        done
    done
done
wait
echo "Philips with lsharp, kv and lstar done!"

## Run the reference experiments with adaptive lsharp and kv
for seed in $(seq 1 $seedlimit);
do
    for i in "${!philips_target[@]}"
        do
          (
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_philips}${philips_target[i]}".dot -ref "${path_to_philips}${philips_ref[i]}".dot -resfile "${path_to_results}${philips_target[i]}"_${philips_ref[i]}"_adaptivelsharp_${seed}_RM.csv" -R -M -timeout "${TIMEOUT}" > "${path_to_logs}${philips_target[i]}"_${philips_ref[i]}"_adaptivelsharp_${seed}_RM.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_philips}${philips_target[i]}".dot -ref "${path_to_philips}${philips_ref[i]}".dot -resfile "${path_to_results}${philips_target[i]}"_${philips_ref[i]}"_adaptivelsharp_${seed}_R.csv" -R -timeout "${TIMEOUT}" > "${path_to_logs}${philips_target[i]}"_${philips_ref[i]}"_adaptivelsharp_${seed}_R.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_philips}${philips_target[i]}".dot -ref "${path_to_philips}${philips_ref[i]}".dot -resfile "${path_to_results}${philips_target[i]}"_${philips_ref[i]}"_adaptivelsharp_${seed}_M.csv" -M -timeout "${TIMEOUT}" > "${path_to_logs}${philips_target[i]}"_${philips_ref[i]}"_adaptivelsharp_${seed}_M.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_philips}${philips_target[i]}".dot -ref "${path_to_philips}${philips_ref[i]}".dot -resfile "${path_to_results}${philips_target[i]}"_${philips_ref[i]}"_adaptivelsharp_${seed}_RMQuan.csv" -R -M -A -timeout "${TIMEOUT}" > "${path_to_logs}${philips_target[i]}"_${philips_ref[i]}"_adaptivelsharp_${seed}_RMQuan.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivelsharp -seed "${seed}" -sul "${path_to_philips}${philips_target[i]}".dot -ref "${path_to_philips}${philips_ref[i]}".dot -resfile "${path_to_results}${philips_target[i]}"_${philips_ref[i]}"_adaptivelsharp_${seed}_MQuan.csv" -M -A -timeout "${TIMEOUT}" > "${path_to_logs}${philips_target[i]}"_${philips_ref[i]}"_adaptivelsharp_${seed}_MQuan.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn adaptivekv -seed "${seed}" -sul "${path_to_philips}${philips_target[i]}".dot -ref "${path_to_philips}${philips_ref[i]}".dot -resfile "${path_to_results}${philips_target[i]}"_${philips_ref[i]}"_adaptivekv_${seed}.csv" -timeout "${TIMEOUT}" > "${path_to_logs}${philips_target[i]}"_${philips_ref[i]}"_adaptivekv_${seed}.log"
            java -jar ./experiments/scripts/ExperimentRunner.jar -cache -eq rndWp -learn pdlstar -seed "${seed}" -sul "${path_to_philips}${philips_target[i]}".dot -ref "${path_to_philips}${philips_ref[i]}".dot -resfile "${path_to_results}${philips_target[i]}"_${philips_ref[i]}"_pdlstar_${seed}.csv" -timeout "${TIMEOUT}" > "${path_to_logs}${philips_target[i]}"_${philips_ref[i]}"_pdlstar_${seed}.log"
          ) &
        if [[ $(jobs -r -p | wc -l) -ge $N ]]; then
            wait -n
            dir="${path_to_results}"
            count=$(ls "${dir}" | wc -l)
            echo ${count}"/"${TOTAL} " runs for experiment 2 finished!"
        fi
    done
done
wait
echo "Philips with adaptive lsharp, adaptive kv and adaptive lstar done!"

# make one combined file
touch ./experiments/results/experiment2/results_exp2.csv
echo "sul,refs,algorithm,states,inputs,time,isEquiv,rounds,mq_sym,mq_rst,eq_sym,eq_rst,rebuildOQ,rebuildStates,matchRefinementOQ,matchRefinementStates,matchSeparationOQ,matchSeparationStates" > ./experiments/results/experiment2/results_exp2.csv
cat ./experiments/results/experiment2/results/* >> ./experiments/results/experiment2/results_exp2.csv
python3 ./experiments/scripts/exp2_processing.py
# pdflatex -synctex=1 -interaction=nonstopmode -output-directory=./experiments/results/plots/ ./experiments/results/plots/main.tex


