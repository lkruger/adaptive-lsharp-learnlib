State Matching and Multiple References in Adaptive Active Automata Learning
----------------------------------------------------------------------------
**Artifact build on top of the LearnLib Library**

**Authors: Loes Kruger, Sebastian Junges, and Jurriaan Rot**

For questions about the code/artifact/paper, feel free to send an email to `loes.kruger@ru.nl`.

Table Of Contents
-------------------------------------
0. Brief Summary
1. Code Usage
2. Additions to LearnLib
3. Benchmark Execution


## 0. Brief Summary
This github page contains information to reproduce the experiments as presented in the paper "State Matching and Multiple References in Adaptive Active Automata Learning".
It belongs to the artifact available at this link which contains the docker image. However, the experiment scripts already use a compiled version of the code, so using the docker image is not necessary to reproduce the results. 

Below, we explain how to use the jar files generated from the code in the `AdaptiveLSharpLearnLib` directory can be used to recreate the data (with the experiment scripts) and to run additional experiments. We also provide information on which parts of the code are new.

To check whether everything the jar files/experiment scripts are operating correctly, the following command can be executed:

```
bash ./experiments/scripts/smoke-check.sh
```
The output should look as follows (the times at the start of the lines will be different)
```
12:25:57.251 [main] INFO  ExperimentRunnerAdaptiveLSharp - SUL name: sul.dot
12:25:57.252 [main] INFO  ExperimentRunnerAdaptiveLSharp - SUL dir: /artifact/./experiments/models/smokecheck_models/sul.dot
12:25:57.252 [main] INFO  ExperimentRunnerAdaptiveLSharp - Reference model name: reference1.dot
12:25:57.254 [main] INFO  ExperimentRunnerAdaptiveLSharp - Reference model name: reference2.dot
12:25:57.255 [main] INFO  ExperimentRunnerAdaptiveLSharp - Seed: 51
12:25:57.255 [main] INFO  ExperimentRunnerAdaptiveLSharp - Timeout: 60
12:25:57.255 [main] INFO  ExperimentRunnerAdaptiveLSharp - Cache: Y
12:25:57.255 [main] INFO  ExperimentRunnerAdaptiveLSharp - Qsize: 6
12:25:57.255 [main] INFO  ExperimentRunnerAdaptiveLSharp - Isize: 3
12:25:57.263 [ForkJoinPool.commonPool-worker-1] INFO  ExperimentRunnerAdaptiveLSharp - EquivalenceOracle: RndWpMethodEQOracle(3, 3, 100000000, 1)
12:25:57.263 [ForkJoinPool.commonPool-worker-1] INFO  ExperimentRunnerAdaptiveLSharp - Method: Adaptive L#
12:25:57.263 [ForkJoinPool.commonPool-worker-1] INFO  ExperimentRunnerAdaptiveLSharp - Rebuilding [Enabled]
12:25:57.263 [ForkJoinPool.commonPool-worker-1] INFO  ExperimentRunnerAdaptiveLSharp - Approximate State Matching [Enabled]
12:25:57.325 [ForkJoinPool.commonPool-worker-1] INFO  ExperimentRunnerAdaptiveLSharp - Hypothesis 1 has size 6
digraph g {

        s0 [shape="circle" label="de.learnlib.algorithms.lsharp.LSState@0"];
        s1 [shape="circle" label="de.learnlib.algorithms.lsharp.LSState@1"];
        s2 [shape="circle" label="de.learnlib.algorithms.lsharp.LSState@2"];
        s3 [shape="circle" label="de.learnlib.algorithms.lsharp.LSState@3"];
        s4 [shape="circle" label="de.learnlib.algorithms.lsharp.LSState@4"];
        s5 [shape="circle" label="de.learnlib.algorithms.lsharp.LSState@5"];
        s0 -> s1 [label="c  /  0"];
        s0 -> s0 [label="b  /  0"];
        s0 -> s3 [label="a  /  1"];
        s1 -> s0 [label="c  /  0"];
        s1 -> s1 [label="b  /  0"];
        s1 -> s2 [label="a  /  1"];
        s2 -> s2 [label="c  /  1"];
        s2 -> s0 [label="b  /  0"];
        s2 -> s1 [label="a  /  1"];
        s3 -> s3 [label="c  /  0"];
        s3 -> s4 [label="b  /  1"];
        s3 -> s3 [label="a  /  0"];
        s4 -> s4 [label="c  /  0"];
        s4 -> s5 [label="b  /  1"];
        s4 -> s4 [label="a  /  0"];
        s5 -> s5 [label="c  /  0"];
        s5 -> s0 [label="b  /  2"];
        s5 -> s5 [label="a  /  0"];

__start0 [label="" shape="none" width="0" height="0"];
__start0 -> s0;

}
12:25:57.337 [ForkJoinPool.commonPool-worker-1] INFO  ExperimentRunnerAdaptiveLSharp - Rounds: 1
12:25:57.337 [ForkJoinPool.commonPool-worker-1] INFO  ExperimentRunnerAdaptiveLSharp - MQ [Resets]: 46
12:25:57.337 [ForkJoinPool.commonPool-worker-1] INFO  ExperimentRunnerAdaptiveLSharp - MQ [Symbols]: 179
12:25:57.337 [ForkJoinPool.commonPool-worker-1] INFO  ExperimentRunnerAdaptiveLSharp - EQ [Resets]: 0
12:25:57.337 [ForkJoinPool.commonPool-worker-1] INFO  ExperimentRunnerAdaptiveLSharp - EQ [Symbols]: 0
12:25:57.337 [ForkJoinPool.commonPool-worker-1] INFO  ExperimentRunnerAdaptiveLSharp - Rebuilding [Symbols]: 10
12:25:57.337 [ForkJoinPool.commonPool-worker-1] INFO  ExperimentRunnerAdaptiveLSharp - Rebuilding [States]: 2
12:25:57.337 [ForkJoinPool.commonPool-worker-1] INFO  ExperimentRunnerAdaptiveLSharp - Match Refinement [Symbols]: 0
12:25:57.337 [ForkJoinPool.commonPool-worker-1] INFO  ExperimentRunnerAdaptiveLSharp - Match Refinement [States]: 0
12:25:57.337 [ForkJoinPool.commonPool-worker-1] INFO  ExperimentRunnerAdaptiveLSharp - Match Separation [Symbols]: 8
12:25:57.337 [ForkJoinPool.commonPool-worker-1] INFO  ExperimentRunnerAdaptiveLSharp - Match Separation [States]: 1
12:25:57.337 [ForkJoinPool.commonPool-worker-1] INFO  ExperimentRunnerAdaptiveLSharp - Matching Table: 
  state -   match -   score  | old states 
      0 -     [3] - 1.000000 |     16/20     10/20      9/20     17/17     13/17      6/17      6/17
      1 -     [1] - 0.916667 |      6/12     11/12      5/12       3/7       0/7       0/7       2/7
      2 -     [2] - 1.000000 |       4/6       2/6       6/6       3/4       0/4       0/4       1/4
      3 -     [4] - 1.000000 |       3/6       3/6       2/6      5/13     13/13      6/13      4/13
      4 -     [5] - 1.000000 |       3/6       3/6       2/6       3/9       4/9       9/9       3/9
      5 -     [6] - 1.000000 |       3/6       3/6       2/6       2/5       1/5       1/5       5/5
12:25:57.343 [ForkJoinPool.commonPool-worker-1] INFO  ExperimentRunnerAdaptiveLSharp - Equivalent: OK
```

## 1. Code usage
-------------------------------------
For general usage of the jar file, we provide the command to learn S with references R1 and R2 from Figure 1 in the paper.
We refer the reader to the [the LearnLib documentation](https://learnlib.de/) for additional information.

```
java -jar ./experiments/scripts/ExperimentRunner.jar 
     -cache 
     -eq rndWp 
     -learn adaptivelsharp 
     -seed 51 
     -sul ./experiments/models/smokecheck_models/sul.dot
     -ref ./experiments/models/smokecheck_models/reference1.dot ./experiments/models/smokecheck_models/reference2.dot 
     -resfile ./smokecheck.csv 
     -R 
     -M 
     -A 
     -timeout 60
```

We will go over the used parameters and how they are used in the paper.

- `java -jar ./experiments/scripts/ExperimentRunner.jar`	
    This indicates that we use a compiled jar file with the name ExperimentRunner.jar
	This jar file can be compiled from `AdaptiveLSharpLearnLib/examples/.../adaptive_lsharp/ExperimentRunnerAdaptiveLSharp`.
- `-cache` This indicates that the MQ oracle and EQ oracle use caching.
- `-eq rndWp` The `-eq` indicates that we will specify the equivalence oracle next. The available equivalence oracles (all already implemented in LearnLib) are 
    + `rndWalks` Performs a random walk over the hypothesis. A random walk restarts with probability 0.05 after every step and terminates after a 100 steps or with a counterexample.
    + `rndWords` Generates a maximum of 100000000 random words with length between 30 and 100.
    + `Wp` Generates tests according to the Wp method with minimal size 3.
    + `rndWp` Randomised Wp method with minimal size 3, random length 3 and a maximum of 100000000 tests.
- `-learn adaptivelsharp` The `-learn` indicates that we will specify the learning algorithm next. The available learning algorithms are
    + `lsharp` This indicates the L# algorithm described [here](https://arxiv.org/pdf/2107.05419)
    + `adaptivelsharp` This indicates the adaptive L# algorithm which is the main contribution of the paper associated with this artifact.
    + `kv` This indicates the Kearns-Vazirani algorithm as implemented in LearnLib.
    + `adaptivekv` This indicates the Mealy machine version of the incremental Kearns-Vazirani algorithm described [here](https://arxiv.org/pdf/2209.00122).
    + `pdlstar` This indicates the partial dynamic L* algorithm described [here](https://nms.kcl.ac.uk/mohammad.mousavi/pub/mousavi-ifm-2019.pdf).
- `-seed 51` Seed used by the random generator.
- `-sul ./experiments/models/smokecheck_models/sul.dot` The `-sul` indicates that we will specify the path to the system under learning next. For this artifact, all the models can be found in the folder `./experiments/models`. Several subfolders exist:
    + `automata_wiki` This folder contains models available on the [AutomataWiki](https://automata.cs.ru.nl/). These models are used in all experiments.
    + `mutated_models` This folder contains mutated models for Experiment 1 and 3. Initially, the folder is empty but after running the experiment scripts for Experiment 1 and 3, this folder should be filled.
    + `openSSL_models` This folder contains the openSSL models used in Experiment 2 and described [here](https://link.springer.com/chapter/10.1007/978-3-319-47560-8_11).
    + `smokecheck_models` This folder contains the models used during the smoke check. These models correspond to the models depicted in Figure 1.
- `-ref ./experiments/models/smokecheck_models/reference1.dot ./experiments/models/smokecheck_models/reference2.dot` The `-ref` indicates that we will specify the paths to the reference models next. One or multiple paths can be specified. 
- `-resfile ./smokecheck.csv` The `-resfile` indicates that we will specify the path to a .csv file next where the abbreviated results for this experiment will be stored. The written line can be parsed as follows:```sul,refs,algorithm,states,inputs,time,isEquiv,rounds,mq_sym,mq_rst,eq_sym,eq_rst,rebuildOQ,rebuildStates,matchRefinementOQ,matchRefinementStates,matchSeparationOQ,matchSeparationStates```
- `-R` If this flag is used in combination with `-learn adaptivelsharp`, rebuilding is enabled.
- `-M` If this flag is used in combination with `-learn adaptivelsharp`, state matching is enabled.
- `-A` If this flag is used in combination with `-learn adaptivelsharp` and flag `-M`, approximate state matching is enabled.
- `-timeout 60` The `-timeout` indicates that we will specify the timeout in seconds next. The default timeout is 5 minutes.

## 2. Additions to LearnLib
-------------------------------------
The code in this artifact is based on the code available at
```
https://github.com/UCL-PPLV/learnlib
```

The following files/folders are added:
- `AdaptiveLSharpLearnLib/algorithms/adaptive/adaptivelsharp` This folder contains the implementation of lsharp with rebuilding and state matching.
- `AdaptiveLSharpLearnLib/algorithms/adaptive/incremental/.../IKearnsVaziraniMealy`: This file contains the Mealy machine adaptation of adaptive Kearns-Vazirani.
- `AdaptiveLSharpLearnLib/commons/util/.../mealy/MealyMutator` This file contains several options to mutate a Mealy machine.
- `AdaptiveLSharpLearnLib/examples/.../adaptive_lsharp/AdaptiveLSharpRunningExample` This file shows how a single experiment for adaptive lsharp works with the running example from the paper.
- `AdaptiveLSharpLearnLib/examples/.../adaptive_lsharp/ExperimentRunnerAdaptiveLSharp` This is the java file used to make ExperimentRunner.jar.
- `AdaptiveLSharpLearnLib/examples/.../adaptive_lsharp/GenerateMutatedModels` This is the java file used to make GenerateMutatedModels.jar.

## 3. Benchmark Execution
-------------------------------------
If the smoke-check gives the correct output, the benchmarks can be run to recreate the paper's results.
Because some of the benchmarks take long to learn or require too much memory, we provide alternative reduced versions of Experiments 1, 3 and 4 described in the paper.

Below, we provide the following information:
- (3.1) How to run each (reduced) experiment from the paper separately.
- (3.2) How to run all experiments after each other with one command.
- (3.3) How to run the full versions of Experiments 1, 3 and 4.
- (3.4) Overview table.

### 3.1 Experiments
Make sure that you are in the directory above `experiments` when running experiments.

#### Experiment 1
To execute the reduced version of Experiment 1 from the paper, use the following command:

```
bash ./experiments/scripts/exp1_reduced.sh
```

This script performs the experiments for 4 out of 6 models and with only 10 of the standard 30 seeds. The expected time for the reduced experiment is 60 minutes. The numbers displayed in the terminal while running indicate how many mutated models have been made or how many logs are currently in the log directory. After the script has terminated, the result csv can be found at `./experiments/results/experiment1/results_exp1.csv`.
The python scripts to generate the plots can be found under `./experiments/scripts` and a pdf containing all the plots can be generated using the command
```
pdflatex -synctex=1 -interaction=nonstopmode -output-directory=./experiments/results/plots/ ./experiments/results/plots/main.tex

```
if pdflatex is installed on your pc. The docker container can be used to generate all the plots immediately.

#### Experiment 2
To execute Experiment 2 from the paper, use the following command:

```
bash ./experiments/scripts/exp2.sh
```

This script performs Experiment 2 in full. The numbers displayed in the terminal while running indicate how many logs are currently in the log directory. The expected time for this experiment is 25 minutes. After the script has terminated, the results csv can be found at `./experiments/results/experiment2/results_exp2.csv`.

#### Experiment 3
To execute the reduced version of Experiment 3 from the paper, use the following command:

```
bash ./experiments/scripts/exp3_reduced.sh
```

This script performs the experiments for 4 out of 6 models and with only 15 of the standard 30 seeds. The expected time for the reduced experiment is 30 minutes. After the script has terminated, the results csv can be found at `./experiments/results/experiment3/results_exp3.csv`.

#### Experiment 4
To execute Experiment 4 from the paper, use the following command:

```
bash ./experiments/scripts/exp4_reduced.sh
```

This script performs Experiment 4 for all TCP models and 5/7 DTLS models. We only use 15 seeds instead of 30. The expected time for this experiment is 60 minutes. After the script has terminated, the results csv can be found at `./experiments/results/experiment4/results_exp4.csv`.

### 3.2 All Reduced Experiments
Instead of running all experiments manually, it is possible to run all experiments with one command:

```
bash ./experiments/scripts/all_reduced.sh
```

Note that this command uses the reduced version of Experiments 1, 3-4 and the full version of Experiment 2. This script simply runs all previous scripts sequentially, all the information about the models and seeds from the above section still holds.

### 3.3 Full Experiments
To run the full versions of Experiments 1, 3 and 4, the following commands can be used.
Note that some of these models can take very long and can use quite some memory.

```
bash ./experiments/scripts/run-exp1.sh
bash ./experiments/scripts/run-exp3.sh
bash ./experiments/scripts/run-exp4.sh
```

### 3.4 Overview
The table below summarizes the available benchmarks.
The time estimates are from running the benchmarks on a modern mid-range laptop.

| Script          | Description                            | Result           | Time Estimate |
|-----------------|----------------------------------------|------------------|---------------|
| exp1_reduced.sh | 4/6 models, 10 seeds                   | Table 2          | 60min         |
| exp3_reduced.sh | 4/6 models, 15 seeds                   | Figure 4b and 4c | 30min         |
| exp4_reduced.sh | 8/10 models, 15 seeds                  | Figure 5         | 60min         |
| exp1.sh         | Complete                               | Table 2          | 48h*          |
| exp2.sh         | Complete                               | Figure 4a        | 25min         |
| exp3.sh         | Complete                               | Figure 4b and 4c | 40h*          |
| exp4.sh         | Complete                               | Figure 5         | 13h*          |
| all_reduced.sh  | Reduced version                        | -                | 3h            |
| all.sh          | Complete                               | -                | 102h*         |

\* The full experiments were performed on AMD Ryzen Threadripper PRO 5965WX 24-Cores with 48 threads and hyperthreading. We can only estimate the time required to run these files on a modern mid-range laptop. The estimations for the reduced experiments and experiment 2 should be more accurate.



